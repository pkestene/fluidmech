\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://framagit.org/olivier/sensible-styles

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger preamble)
\printversion

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\sectionstarwithsubtitle{Flow separation}{Tchüss}

	%\subsectionstarwithsubtitle{Principle}{Tchüss}
	
		\oldfigureframe{What we’d like to have…}{boundary_layer_separation_0.png}{1}{\wcfile{Boundary layer separation.svg}{Figure} \ccby \olivier}
		\oldfigureframe{…and what we get}{boundary_layer_separation_01.png}{1}{\wcfile{Boundary layer separation.svg}{Figure} \ccby \olivier}
		\oldfigureframe{}{boundary_layer_separation.png}{1}{\wcfile{Boundary layer separation.svg}{Figure} \ccby \olivier}

		\begin{frame}
			Yikes!\pause
			
			Flow separates from the wall — The boundary layer disintegrates…\pause
			
			\emph{Separation is our failure to control the velocity of the fluid.}\pause
			
			Can we predict separation mathematically?
		\end{frame}

		\begin{comment}
		\begin{frame}
				When $U = f_{(x)}$, the \emph{geometry} of the boundary layer changes:\pause
				
				\begin{itemize}
					\item When speed increases ($\diff U/\diff x >0$), the layer is “flattened”;\pause
					\item When speed decreases ($\diff U/\diff x <0$), the boundary layer “straightens up”.\\\pause
							When the speed profile becomes vertical, streamlines separate!
				\end{itemize}
				
		\end{frame}
		\end{comment}

		\figureframe{1}{boundary_layer_separation.png}{}{\wcfile{Boundary layer separation.svg}{Figure} \ccby \olivier}
	
		\begin{frame}

				What equation should we solve?\pause
				
				Key idea: \textbf{at the separation point, the shear on the wall is zero}:\pause
			\begin{IEEEeqnarray*}{rCl}
				\tau_\text{wall} = \mu \left(\frac{\partial u}{\partial y}\right)_{@y=0} &=& 0 \nonumber\\
			\end{IEEEeqnarray*}
			
		\end{frame}

		\begin{frame}
		
			Back to basics: at the wall ($u=0$ and $v=0$), writing N-S:\pause
			\begin{IEEEeqnarray*}{rCl}
					u \partialderivative{u}{x} + v \partialderivative{u}{y}  & = & U \frac{\diff U}{\diff x} + \frac{\mu}{\rho} \secondpartialderivative{u}{y} \nonumber\\\pause
					\mu \left(\secondpartialderivative{u}{y}\right)_{@y=0}  &=& \derivative{p}{x} \nonumber\\
			\end{IEEEeqnarray*}
		
		\end{frame}

		\begin{frame}
			\begin{IEEEeqnarray*}{rCl}
					\mu \left(\secondpartialderivative{u}{y}\right)_{@y=0}  & = & \derivative{p}{x}
			\end{IEEEeqnarray*}
		
			\small
			When $\partialderivative{p}{x} > 0$, then $\secondpartialderivative{u}{y} > 0$ at the wall.
		\end{frame}

		\figureframe{1}{boundary_layer_inflection_1}{}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
		\figureframe{1}{boundary_layer_inflection_2}{}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
		\figureframe{1}{boundary_layer_inflection_3}{}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
		\figureframe{1}{boundary_layer_inflection_4}{}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
			
		\begin{frame}
			\includegraphics[width=5cm]{boundary_layer_inflection_point}
			\begin{itemize}
				\item $\inlinesecondpartialderivative{u}{y}$ always negative at $y=\delta$\pause
				\item $\inlinesecondpartialderivative{u}{y}$ is positive at $y=0$ when $\inlinepartialderivative{p}{x}>0$\pause
			\end{itemize}
			
			So: if $\inlinepartialderivative{p}{x}>0$, then an \vocab{inflection point} exists!
		\end{frame}
			
		\begin{frame}
			\includegraphics[width=8cm]{boundary_layer_inflection_point}
			\begin{itemize}
				\item Above inflection point: net shear is decelerating\pause
				\item Below inflection point: net shear is accelerating
			\end{itemize}
			Uh-oh!\pause
			
			unstable layer: the flow “crumples over itself”, the layer separates!
		\end{frame}
		
		\figureframe{1}{boundary_layer_inflection_point.png}{}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
		
		\begin{frame}
			Two crucial points regarding boundary layer separation\pause
			\begin{enumerate}
				\item \textbf{Separation only occurs if a positive pressure gradient exists}\\(\vocab{adverse gradient}).%\pause
				
					%~\\
					%\small Separation points along a wall are always in areas where pressure increases ($\diff p/\diff x$ positive or $\diff U/\diff x$ negative). If pressure remains constant or decreases, the boundary layer \emph{cannot} separate.
			\end{enumerate}
		\end{frame}

		\begin{comment}
		\pictureframe{1}{1970_Fiat_500_L_(9037425190)}{}{\wcfile{1970_Fiat_500_L_(9037425190).jpg}{Photo} \ccby by \flickrname{39302751@N06}{Spanish Coches}}

		\pictureframe{1}{1970_Fiat_500_L_(9037425190)_bw}{}{\wcfile{1970_Fiat_500_L_(9037425190).jpg}{Photo} \ccby by \flickrname{39302751@N06}{Spanish Coches} (modified)}

		\pictureframe{1}{1970_Fiat_500_L_(9037425190)_bw_mod}{}{\wcfile{1970_Fiat_500_L_(9037425190).jpg}{Photo} \ccby by \flickrname{39302751@N06}{Spanish Coches} (modified)}
		\end{comment}

		%\oldfigureframe{}{reynolds_number_effect_on_airfoil_flow.png}{0.8}{\wcfile{Reynolds Number Effect on Airfoil Flow.svg}{figure} \ccbysa \olivier, based on Barlow \& Pope 1999~\cite{barlowraepope1999}}


			\begin{frame}
				\begin{enumerate}
					\shift{1}
					\item \textbf{Laminar boundary layers separate more easily}.\pause
					
						~\\
						Want to delay separation? Make the boundary layer turbulent!
				\end{enumerate}
			\end{frame}

			%\pictureframe{1}{1280px-NASA_DFRC_Lear_24_in_flight.jpg}{}{\wcfile{NASA DFRC Lear 24 in flight.jpg}{Photo} by Jim Ross (NASA, \pd)}
			%\oldpictureframe{}{1280px-Learjet_24_Wing_Leading_Edge_(5445643377)}{1}{\wcfile{Learjet 24 Wing Leading Edge (5445643377).jpg}{Photo} \ccbysa \flickruser{born1945}}


			\oldpictureframe{}{im_01399.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{im_01397.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{im_01396.jpg}{1}{Photo \ccbysa \olivier}


			\oldpictureframe{}{2_1280px-JR_West_500_024.jpg}{1}{\wcfile{JR West 500 024.jpg}{Photo} \ccbysa \wcu{Tennen-Gas}}
			\oldpictureframe{}{3_1280px-JRwest_500_nozomi_29_kodama_697_shiniwakuni}{1}{\wcfile{aJRwest 500 nozomi 29 kodama 697 shiniwakuni.jpg}{Photo} \ccbysa \wcu{Spaceaero2}}
			\oldpictureframe{}{1_JR_West_500_051.JPG}{1}{\wcfile{JR West 500 051.JPG}{Photo} \ccbysa \wcu{Tennen-Gas}}

			\pictureframe{1}{VT-ANN_(14383883996)}{}{\wcfile{VT-ANN (14383883996).jpg}{Photo} \ccby by \flickruser{76052339@N05}{Alec Wilson}}
			
			
			%\oldpictureframe{}{im_09297.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{im_09700.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{im_09701.jpg}{1}{Photo \ccbysa \olivier}
			
			\begin{comment}
			\oldpictureframe{}{img_20075.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{img_20080.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{11.jpg}{1}{Photo \ccbysa \olivier}
			\oldpictureframe{}{10.jpg}{1}{Photo \ccbysa \olivier}
			
			\pictureframe{1}{Valtteri_Bottas-Mercedes_AMG-2019_(1).jpg}{}{\wcfile{Valtteri_Bottas-Mercedes_AMG-2019_(1).jpg}{Photo} \ccbysa by \wcu{Alberto-g-rovi}}
			
			\pictureframe{1}{Toyota_TF110_front_nose_front-left_2019_Prototyp_Museum.jpg}{}{\wcfile{Toyota_TF110_front_nose_front-left_2019_Prototyp_Museum.jpg}{Photo} \ccbysa by \wcu{Morio}}
			
			\skipinprint{
			\pictureframe{1}{SUT_Monaco_Grand_P_1356164}{}{Photo \copyright\xspace Sutton Images}
			\fullFrameMovie[autostart]{videos/U6H9HtL-AzA.mp4}{videos/U6H9HtL-AzA}{}
			\pictureframe{1}{dcd1604ma387}{}{Photo \copyright\xspace Sutton Images}
			\pictureframe{1}{dcd1604ma391}{}{Photo \copyright\xspace Sutton Images}
			\pictureframe{1}{Im3AAbkTRPA}{}{}
			\fullFrameMovie[autostart]{videos/rzRuZNvkMbc.mp4}{videos/rzRuZNvkMbc}{}
			}
			\end{comment}
	
			% Frisbee
	\pictureframe{1}{2455073433_d190978351_o}{}{\flickrfile{taylorhand/2455073433}{Photo} \ccbysa by \flickrname{taylorhand}{Taylor Hand}}
	
	% Dog with frisbee
	\pictureframe{1}{3923932727_fd1214b464_o}{}{\flickrfile{chrishau/3923932727}{Photo} \ccbysa by \flickrname{chrishau}{Chris Hau}}

\end{document}

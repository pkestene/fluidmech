\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{09}
   \renewcommand{\lasteditday}{19}
\renewcommand{\numberofthischapter}{10}
\renewcommand{\titleofthischapter}{\namechapterten}

\fluidmechchaptertitle
\label{chap_ten}

\mecafluboxen

\section{Motivation}

	In this chapter, we focus on fluid flow close to solid walls. In these regions, viscous effects dominate the dynamics of fluids. This study should allow us to answer two questions:
	\begin{itemize}
		\item How can we quantify shear-induced friction on solid walls?
		\item How can we describe and predict flow separation?
	\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The concept of boundary layer}

%%%%%%%%%%%%%%%%%
	\subsection{Rationale}
		At the very beginning of the 20\up{th} century, \we{Ludwig Prandtl} observed that for most ordinary fluid flows, viscous effects played almost no role outside of a very small layer of fluid along solid surfaces. In this area, shear between the zero-velocity solid wall and the outer flow dominates the flow structure. He named this zone the \vocab{boundary layer}.\wupen{Boundary layer}
	
		We indeed observe that around any solid object within a fluid flow, there exists a thin zone which is significantly slowed down because of the object’s presence. This deceleration can be visualized by measuring the velocity profile (\cref{fig_boundarylayer}).
	
				\begin{figure}
					\begin{center}
						\includegraphics[width=10cm]{boundary_layer_thickness_concept.png}
					\end{center}
					\supercaption{A typical velocity profile in a boundary layer. Only the horizontal component of velocity ($V_x = u$) is represented.}{\wcfile{Boundary layer velocity thickness.svg}{Figure} \ccby \olivier}
					\label{fig_boundarylayer}
				\end{figure}
	
		The boundary layer is a \emph{concept}, a thin invisible layer whose upper limit (termed $\delta$, as we will see in \S\ref{ch_characterization_boundary_layer}) is defined as the distance where the fluid velocity in direction parallel to the wall is \SI{99}{\percent} of the outer (undisturbed) flow velocity.
	
		Upon investigation, we observe that the boundary layer thickness depends strongly on the main flow characteristics. In particular, it \emph{decreases} when speed increases or when viscosity is decreased (\cref{fig_bl_ep}).
	
				\begin{figure}
					\begin{center}
						\includegraphics[width=8cm]{boundary_layer_thickness_speed.png}
					\end{center}
					\supercaption{The thickness of the boundary layer depends strongly on the faraway incoming flow velocity~$U_\infty$.}{\wcfile{Boundary layer thickness and flow speed.svg}{Figure} \ccbysa \olivier~\& \wcu{F l a n k e r}}
					\label{fig_bl_ep}
				\end{figure}

		As we travel downstream along a boundary layer, we observe experimentally that the flow regime is always laminar at first. Then, at some distance downstream which we name \vocab{transition point}, the boundary layer becomes turbulent. The flow-wise position of the transition point depends on the flow properties and is somewhat predictable. Further downstream, the boundary layer becomes fully turbulent. It has larger thickness than in the laminar regime, and grows at a faster rate. Like all turbulent flows, it then features strong energy dissipation and its analytical description becomes much more difficult.
	
		The flow within the boundary layer, and the main external flow (outside of it) affect one another, but may be very different in nature. A laminar boundary layer may exist within a turbulent main flow; while turbulent boundary layers are commonplace in laminar flows.


%%%%%%%%%%%%%%%%%
	\clearpage %handmade
	\subsection{Why do we study the boundary layer?}

		\youtubethumb{cgWIYSnvIEg}{the basics of the boundary layer}{\oc (\ccby)}
		Expending our energy on solving such a minuscule area of the flow may seem counter-productive, yet three great stakes are at play here:
		\begin{itemize}	
			\item First, a good description allows us to \textbf{avoid having to solve the Navier-Stokes equations in the whole flow}.\\
					Indeed, outside of the boundary layer and of the wake areas, viscous effects can be safely neglected. Fluid flow can then be described with $\rho \totaltimederivative{\vec V} = - \gradient{p}$ (the Euler equation, which we will introduce as eq.~\ref{eq_euler_equation} in the next chapter) with acceptably low inaccuracy. As we shall soon see, this allows us to find many interesting solutions, all within reach of human comprehension and easily obtained with computers. Unfortunately, they cannot account for shear on walls.\\
					Thus, solving the flow within the boundary layer, whether analytically or experimentally, allows us to solve the rest of the flow in a simplified manner (\cref{fig_no_ns}).
	
				\begin{figure}[ht!]
					\begin{center}
						\includegraphics[width=0.9\textwidth]{Types_of_flow_analysis_in_fluid_mechanics.png}
					\end{center}
					\supercaption{Fluid flow around a wing profile. When analyzing the flow, whether analytically or within a computational fluid dynamics (\textsc{cfd}) simulation, the flow domain is frequently split into three distinct areas. In the boundary layer (B), fluid flow is dominated by viscosity. Outside of the boundary layer (A), viscous effects are very small, and the flow can be approximately solved using the Euler equation. Lastly, in the turbulent wake (C), characterization of the flow is very difficult and typically necessitates experimental investigations.}{\wcfile{Types of flow analysis in fluid mechanics.svg}{Figure} \ccbysa \olivier}
					\label{fig_no_ns}
				\end{figure}
			
			\item Secondly, the boundary layer is the key to \textbf{quantifying friction}. A good resolution of the boundary layer allows us to precisely quantify the shear forces generated by a fluid on an object.
			\item Finally, a good understanding of the mechanisms at hand within the boundary layer allows us to predict \textbf{flow separation}, which is the divergence of streamlines relative to the object. Control of the boundary layer is key to ensuring that a flow will follow a desired trajectory!
		\end{itemize}


%%%%%%%%%%%%%%%%%
	\subsection{Characterization of the boundary layer}
	\label{ch_characterization_boundary_layer}
	
		Three different parameters are typically used to quantify how thick a boundary layer is at any given position.
		
		The first is the \vocab{thickness} $\delta$,
		\begin{IEEEeqnarray}{rCl}
			\delta &\equiv& y|_{u=\num{0,99}U}
		\end{IEEEeqnarray}
		which is, as we have seen above, equal to the distance away from the wall where the speed~$u$ is~\SI{99}{\percent} of~$U$.
		
		The second is the \vocab{displacement thickness} $\delta^*$, which corresponds to the vertical distance by which streamlines outside of the boundary layer have been displaced. This vertical “shifting” of the flow occurs because the inner fluid is slowed down near the wall, creating some blockage for the outer flow, which then proceeds to avoid it partially by deviating outwards (\cref{fig_bl_deltastar}).
	
			\begin{figure}
				\begin{center}
					\includegraphics[width=\textwidth]{boundary_layer_thicknesses.png}
				\end{center}
				\supercaption{Thickness $\delta$ and displacement thickness $\delta^*$ of a boundary layer. It is important to understand that the boundary layer is not a closed zone: streamlines (drawn blue) penetrate it and the vertical velocity~$v$, although very small compared to~$u$, is not zero.}{\wcfile{Boundar layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa \olivier~\& \wcu{F l a n k e r}}
				\label{fig_bl_deltastar}
			\end{figure}
		
		Integral analysis performed on a control volume enclosing the boundary layer (as for example in exercise~\ref{exo_drag_flat_plate} from chapter~3) allows us to quantify the displacement thickness as:
		\begin{IEEEeqnarray}{rCl}
			\delta^* &\equiv& \int_0^\infty \left( 1 - \frac{u}{U}\right) \diff y
		\end{IEEEeqnarray}
		In practice, this integral can be calculated on a finite interval (instead of using the $\infty$ limit), as long as the upper limit exceeds the boundary layer thickness.
		
		The third and last parameter is the \vocab{momentum thickness} $\delta^{**}$ (sometimes written $\theta$) which is equal to the thickness of a corresponding layer of fluid at velocity~$U$ which would possess the same amount of momentum as the boundary layer. The momentum thickness can be thought of as the thickness of the fluid that would need to be entirely stopped (for example by pumping it outside of the main flow) in order to generate the same drag as the local boundary layer.
		
		A review of the experience we gathered in \chapterthreeshort while solving problems~\ref{exo_drag_cylindrical_profile} to~\ref{exo_drag_wing_profile} p.~\pageref{exo_drag_cylindrical_profile} allows us to to quantify the momentum thickness as:
		\begin{IEEEeqnarray}{rCl}
			\delta^{**} &\equiv& \int_0^\delta \frac{u}{U} \left( 1 - \frac{u}{U}\right) \diff y
		\end{IEEEeqnarray}
		with the same remark regarding the upper limit.	The momentum thickness, in particular when compared to the displacement thickness, is an important parameter used in prediction models for boundary layer separation.
		
		
		Once these three thicknesses have been quantified, we are generally looking for a quantification of the shear term $\tau_\text{wall}$. Since we are working with the hypothesis that the fluid is Newtonian, we merely have to know $u_{(y)}$ to quantify shear, according equation~\ref{eq_shear_velocity_gradient} which we wrote way back p.~\pageref{eq_shear_velocity_gradient}:
		\begin{IEEEeqnarray}{rCl}
			\tau_{\text{wall}\ yx} &=& \mu \partialderivative{u}{y}
		\end{IEEEeqnarray}
		
		In a boundary layer, the shear $\tau_\text{wall}$ will decrease with longitudinal distance~$x$, because the velocity gradient above it also decreases. Consequently, $\tau_\text{wall}$ will become a function of $x$, so that the entire shear force will be obtained by integration (reusing eq.~\ref{eq_shear_force_scalar} p.~\pageref{eq_shear_force_scalar}):
		\begin{IEEEeqnarray}{rCl}
			F_{\text{shear} yx} &=& \int_S \tau_{\text{wall } yx} \diff x \diff z \label{eq_force_tau}
		\end{IEEEeqnarray}
		
		As we have seen in the previous chapter, fluid dynamicists like to quantify phenomena with non-dimensional parameters. The wall shear exerted by the boundary layer is typically non-dimensionalized with the \vocab{shear coefficient} $c_f$,
		\begin{IEEEeqnarray}{rCl}
			c_{f_{(x)}} &\equiv& \frac{\tau_\text{wall}}{\frac{1}{2} \rho U^2} \label{eq_def_shear_coeff}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where $U$ is the outer-layer (free-stream) velocity.
		\end{equationterms}
		The shear coefficient, just like the shear, remains a function of the flow-wise distance $x$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Laminar boundary layers}

%%%%%%%%%%%%%%%%%
	\subsection{Governing equations}

		What is happening inside a laminar, steady boundary layer? We begin by writing out the Navier-Stokes for incompressible isothermal flow in two Cartesian coordinates (eqs. \ref{eq_navierstokes} p.~\pageref{eq_navierstokes}):
		\begin{IEEEeqnarray}{cCc}
			\rho \left[ \partialtimederivative{u} + u \partialderivative{u}{x} + v \partialderivative{u}{y} \right]  & = &	\rho g_x - \partialderivative{p}{x} + \mu \left[ \secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} \right] \label{eq_nsun}\\
			\rho \left[ \partialtimederivative{v} + u \partialderivative{v}{x} + v \partialderivative{v}{y} \right]  & = &	\rho g_y - \partialderivative{p}{y} + \mu \left[ \secondpartialderivative{v}{x} + \secondpartialderivative{v}{y} \right] \label{eq_nsdeux}
		\end{IEEEeqnarray}
		
		Building from these two equations, we are going to add three simplifications, which are hypotheses based on experimental observation of fluid flow in boundary layers:
		\begin{enumerate}
			\item Gravity plays a negligible role;
			\item The component of velocity perpendicular to the wall (in our convention,~$v$) is very small ($v \ll u$). \\
					Thus, its stream-wise spatial variations can also be neglected: $\inlinepartialderivative{v}{x} \approx 0$ and $\inlinesecondpartialderivative{v}{x} \approx 0$. The same goes for the derivatives in the $y$-direction: $\partialderivative{v}{y} \approx 0$ and $\inlinesecondpartialderivative{v}{y} \approx 0$.
			\item The component of velocity parallel to the wall (in our convention, $u$) varies much more strongly in the $y$-direction than in the $x$-direction: $\inlinesecondpartialderivative{u}{x} \ll \inlinesecondpartialderivative{u}{y}$.
		\end{enumerate}
		
		With all of these simplifications, equation~\ref{eq_nsdeux} shrinks down to		
		\begin{IEEEeqnarray}{rCl}
			\partialderivative{p}{y} & \approx & 0
		\end{IEEEeqnarray}
		which tells us that pressure is a function of $x$ only ($\inlinepartialderivative{p}{x} = \inlinederivative{p}{x}$).
		
		We now turn to equation~\ref{eq_nsun}, first to obtain an expression for pressure by applying it outside of the boundary layer where $u = U$:
		\begin{IEEEeqnarray}{rCl}
			\derivative{p}{x} &=& - \rho U \frac{\diff U}{\diff x}
		\end{IEEEeqnarray}
		and secondly to obtain an expression for the velocity profile:
		\begin{IEEEeqnarray}{cCc}
			u \partialderivative{u}{x} + v \partialderivative{u}{y}  & = &	-\frac{1}{\rho} \partialderivative{p}{x} + \frac{\mu}{\rho} \secondpartialderivative{u}{y}\nonumber\\
			&=& U \derivative{U}{x} + \frac{\mu}{\rho} \secondpartialderivative{u}{y}
		\end{IEEEeqnarray}
	
		Thus, the velocity field $\vec V = (u ; v) = f(x,y)$ in a steady laminar boundary layer is driven by the two following equations: a balance of momentum, and a balance of mass:
		\begin{IEEEeqnarray}{rCl}
			u \partialderivative{u}{x} + v \partialderivative{u}{y}  & = & U \frac{\diff U}{\diff x} + \frac{\mu}{\rho} \secondpartialderivative{u}{y} \label{eq_ns_bl_lam_un}\\
			\partialderivative{u}{x} + \partialderivative{v}{y} & = & 0 \label{eq_ns_bl_lam_deux}
		\end{IEEEeqnarray}
		The main unknown in this system is the longitudinal speed profile across the layer, $u_{(x,y)}$. Unfortunately, over a century after it has been written, we still have not found an analytical solution to it.


%%%%%%%%%%%%%%%%%
	\subsection{Blasius’ solution}
	
		\we{Heinrich Blasius} undertook a PhD thesis under the guidance of Prandtl, in which he focused on the characterization of laminar boundary layers. As part of his work, he showed that the geometry of the velocity profile (\ie the velocity distribution) within such a layer is \emph{always the same}, and that regardless of the flow velocity or the position, $u$ can be simply expressed as a function of non-dimensionalized distance away from the wall termed~$\eta$:
		\begin{IEEEeqnarray}{rCl}
			\eta &\equiv& y \ \sqrt{\frac{\rho U}{\mu x}}
		\end{IEEEeqnarray}
		Blasius was able to show that $u$ is a function such that ${u}/{U} = f'_{(\eta)}$, with $f''' + \frac{1}{2} f f'' = 0$. Unfortunately, no known analytical solution to this equation is known. However, it has now long been possible to obtain numerical values for $f’$ at selected positions $\eta$. Those are plotted in \cref{fig_bl_blasius}.
	
			\begin{figure}
				\begin{center}
					\includegraphics[width=8cm]{blasius_velocity_profile.png}
				\end{center}
				\supercaption{The velocity profile obtained by Blasius (an exact solution to the Navier-Stokes equations simplified with laminar boundary-layer hypothesis).}{\wcfile{Blasius boundary layer velocity profile.svg}{Figure} \cczero \oc}
				\label{fig_bl_blasius}
			\end{figure}

		Based on this work, it can be shown that for a laminar boundary layer flowing along a smooth wall, the four parameters of interest for the engineer are simply functions of the distance-based Reynolds number $\rex$:
		\begin{IEEEeqnarray}{rCl}
			\frac{\delta}{x} 	&=&	\frac{\num{4,91}}{\sqrt{\rex}} \label{eq_delta_lam}\\\nonumber\\
			\frac{\delta^*}{x} 	&=& \frac{\num{1,72}}{\sqrt{\rex}} \label{eq_deltastar_lam}\\\nonumber\\
			\frac{\delta^{**}}{x} 	&=& \frac{\num{0,664}}{\sqrt{\rex}} \label{eq_deltastarstar_lam}\\\nonumber\\
			c_{f_{(x)}} 		&=& \frac{\num{0,664}}{\sqrt{\rex}} \label{eq_cf_lam}
		\end{IEEEeqnarray}
	
\begin{comment}
%%%%%%%%%%%%%%%%%
	\subsection{Pohlhausen’s model}
	
		Contrary to Blasius who was searching for an exact solution, Ernst Pohlhausen made an attempt at modeling the velocity profile in the boundary layer with a \emph{simple} equation. He imagined an equation of the form:
		\begin{IEEEeqnarray}{rCl}
			\frac{u}{U} = g_{(Y)} &=& a Y + b Y^2 + c Y^3 + d Y^4 \label{eq_idee_Pohlhausen}
		\end{IEEEeqnarray}
		in which $Y \equiv y/\delta$ represents the distance away from the wall, and he then set to find values for the four coefficients $a$, $b$, $c$ and $d$ that would match the conditions we set in eqs. (\ref{eq_ns_bl_lam_un}) and (\ref{eq_ns_bl_lam_deux}).
		
		In order to obtain these coefficients, Blasius simply calibrated his model using known boundary conditions at the edges of the boundary layer:
		\begin{itemize}
			\item for $y=0$ (meaning $Y = 0$), we have both $u=0$ and $v=0$. Equation~\ref{eq_ns_bl_lam_un} becomes:
				\begin{IEEEeqnarray}{rCl}
					0 &=& U \frac{\diff U}{\diff x} + \frac{\mu}{\rho} g''_{(0)}
				\end{IEEEeqnarray}	
			\item for $y=\delta$ (meaning $Y = 1$) we have $u=U$, $\partial u/\partial y = 0$ and $\partial^2 u/(\partial y)^2 = 0$. These values allow us to evaluate three properties of function $g$ :
				\begin{IEEEeqnarray}{rCCCl}
					u												&=& U &=& U g_{(1)}\\
					\partialderivative{u}{y}				&=& 0 &=& \frac{U}{\delta} g'_{(1)}\\
					\secondpartialderivative{u}{y}		&=& 0 &=& \frac{U}{\delta^2} g''_{(1)}
				\end{IEEEeqnarray}
		\end{itemize}
		
		This system of four equations allowed Pohlhausen to evaluate the four terms $a$, $b$, $c$ and $d$. He introduced the variable $\Lambda$, a non-dimensional measure of the outer velocity flow-wise gradient:
		\begin{IEEEeqnarray}{rCl}
			\Lambda &\equiv& \delta^2 \frac{\rho}{\mu} \derivative{U}{x}
		\end{IEEEeqnarray}
		and he then showed that his velocity model in the smooth-surface laminar boundary layer, equation~\ref{eq_idee_Pohlhausen}, had finally become:
		\begin{IEEEeqnarray}{rCl}
			\frac{u}{U} = 1 - (1+Y)(1-Y)^3 + \Lambda \frac{Y}{6} (1 - Y^3) \label{eq_modele_Pohlhausen}
		\end{IEEEeqnarray}	
		
		Pohlhausen’s model is remarkably close to the exact solution described by Blasius (\cref{fig_Pohlhausen}). We thus have an approximate model for $u_{(x, y)}$ within the boundary layer, as a function of $U_{(x)}$.
	
			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=8cm]{pohlhausen_comparison.png}
				\end{center}
				\supercaption{Velocity profiles in the Pohlhausen and Blasius boundary layer profiles.}{Figure \copyright~from Richecœeur 2012~\cite{richecoeur2012}}
				\label{fig_Pohlhausen}
			\end{figure}

		It can be seen now that neither Blasius nor Pohlhausen’s models are fully satisfactory. The first is drawn from a handful of realistic, known boundary conditions, but we fail to find an analytical solution to match them. The second is drawn from the hypothesis that the analytical solution is simple, but it is not very accurate. And we have not even considered yet the case for turbulent flow!\\
		Here, it is the process which matters to us. Both of those methods are frequently-attempted, sensible approaches to solving problems in modern fluid dynamics research, where, just as here, one often has to contend with approximate solutions.
\end{comment}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Boundary layer transition}
\label{ch_bl_transition}

	After it has traveled a certain length along the wall, the boundary layer becomes very unstable and it transits rapidly from a laminar to a turbulent regime (\cref{fig_bl_transition}). We have already described the characteristics of turbulence in broadly in \chapterseven and more extensively in \chapternine; they apply to turbulence within the boundary layer. It is worth reminding ourselves that the boundary layer may be turbulent in a globally laminar flow (\eg around an aircraft in flight, the boundary layer is turbulent, but the main flow is laminar). Here, we refer to the regime of the boundary layer only, not the outer flow.
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=\textwidth]{boundary_layer_transition.png}
			\end{center}
			\supercaption{Transition of a boundary layer from laminar to turbulent regime.}{\wcfile{Boundary layer transition.svg }{Figure} \ccby \olivier}
			\label{fig_bl_transition}
		\end{figure}
	
	We observe that the distance $x_\text{transition}$ at which the boundary layer changes regime is reduced when the velocity is increased, or when the viscosity is decreased. In practice this distance depends on the distance-based Reynolds number $\rex \equiv {\rho U x}/{\mu}$. The most commonly accepted prediction for the transition position is:
	\begin{IEEEeqnarray}{rCl}
		\re_{x\ \text{transition}} &\approx& \num{5e5}
	\end{IEEEeqnarray}

	Transition can be generated earlier if the surface roughness is increased, or if obstacles (\eg turbulators, vortex generators, trip wires) are positioned within the boundary layer. Conversely, a very smooth surface and a very steady, uniform incoming flow will result in delayed transition.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Turbulent boundary layers}

	\youtubethumb{e1TbkLIDWys}{visualizing the turbulent boundary layer on flat body moved in a stationary tank}{Lee, Kwon, Hutchins, and Monty~\cite{lee2012spatially} (\styl)}
	The extensive description of turbulent flows remains an unsolved problem. As we have seen in \chapternine, by contrast with laminar counterparts, turbulent flows result in
		\begin{itemize}
			\item increased mass, energy and momentum exchange;
			\item increased losses to friction;
			\item apparently chaotic internal movements.
		\end{itemize}
	
	Instead of resolving the entire time-dependent flow in the boundary layer, we satisfy ourselves with describing the average component of the longitudinal speed,~$\overline{u}$. A widely-accepted velocity model is:
		\begin{IEEEeqnarray}{rCl}
			\frac{\overline{u}}{U} &\approx& \left(\frac{y}{\delta}\right)^{\frac{1}{7}}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item for turbulent boundary layer flow over a smooth surface.
		\end{equationterms}
	
	\youtubethumb{4KeaAhVoPIw}{a look inside a turbulent boundary layer, with a completely resolved (\dns) fluid flow simulation}{Linné \textsc{Flow} Centre \\\& \textsc{Serc} KTH \cite{schlatter2010structure,schlatter2010assessment} (\styl)}
	This profile has a much flatter geometry near the wall than its laminar counterpart (\cref{fig_comparaison_profils}).

		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{boundary_layer_laminar_turbulent_comparison.png}
			\end{center}
			\supercaption{Comparison of laminar and turbulent boundary layer profiles, both scaled to the same size. It is important to remember that in practice turbulent boundary layers are much thicker than laminar ones, and that $\overline{u}$ is only a time-average velocity.}{\wcfile{Turbulent laminar boundary layer profiles.svg}{Figure} \cczero \oc}
			\label{fig_comparaison_profils}
		\end{figure}
	
	In the same way that we have worked with the laminar boundary layer profiles, we can derive models for our characteristics of interest from this velocity profile:
	\begin{IEEEeqnarray}{rCl}
		\frac{\delta}{x} 		&\approx& 	\frac{\num{0,16}}{\rex^{\frac{1}{7}}} \label{eq_delta_turb}\\\nonumber\\
		\frac{\delta^*}{x} 		&\approx& 	\frac{\num{0,02}}{\rex^{\frac{1}{7}}} \label{eq_deltastar_turb}\\\nonumber\\
		\frac{\delta^{**}}{x} 		&\approx& 	\frac{\num{0,016}}{\rex^{\frac{1}{7}}} \label{eq_deltastarstar_turb}\\\nonumber\\
		c_{f_{(x)}} 			&\approx& 	\frac{\num{0,027}}{\rex^{\frac{1}{7}}} \label{eq_cf_turb}
	\end{IEEEeqnarray}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Flow separation}

		\youtubethumb{zB5g4aB1Sus}{flow separation: what it is, how to predict and avoid it}{\oc (\ccby)}
		Under certain conditions, fluid flow separates from the wall. The boundary layer then disintegrates and we observe the appearance of a turbulent wake near the wall. Separation is often an undesirable phenomenon in fluid mechanics: it may be thought of as the point where we fail to impart a desired trajectory to the fluid.
		
		When the main flow speed $U$ along the wall is varied, we observe that the geometry of the boundary layer changes. The greater the longitudinal speed gradient ($\inlinederivative{U}{x}>0$), and the flatter the profile becomes. Conversely, when the longitudinal speed gradient is negative, the boundary layer velocity profile straightens up. When it becomes perfectly vertical at the wall, it is such that streamlines separate from the wall: this is called \vocab{separation}\wupen{Flow separation} (\cref{fig_bl_sep}).
	
			\begin{figure}
				\begin{center}
					\includegraphics[width=\textwidth]{boundary_layer_separation.png}
				\end{center}
				\supercaption{Separation of the boundary layer. The main flow is from left to right, and flowing towards a region of increasing pressure. For clarity, the $y$-scale is greatly exaggerated.}{\wcfile{Boundary layer separation.svg}{Figure} \ccby \olivier}
				\label{fig_bl_sep}
			\end{figure}

		The occurrence of separation can be predicted if we have a robust model for the velocity profile inside the boundary layer. For this, we go back to fundamentals, stating that \textbf{at the separation point, the shear effort on the surface must be zero}:
		\begin{IEEEeqnarray}{rCl}
			\tau_\text{wall at separation} = 0 &=& \mu \left(\partialderivative{u}{y}\right)_{@ y=0}
		\end{IEEEeqnarray}

		At the wall surface ($u=0$ and $v=0$), equation~\ref{eq_ns_bl_lam_un} p.~\pageref{eq_ns_bl_lam_un} becomes:
		\begin{IEEEeqnarray}{rCl}
				\mu \left(\secondpartialderivative{u}{y}\right)_{@ y=0}  & = & \derivative{p}{x} = - \rho U \derivative{U}{x}
		\end{IEEEeqnarray}
		
		Thus, as we progressively increase the term $\diff p/\diff x$, the term $\inlinesecondpartialderivative{u}{y}$ reaches higher (positive) values on the wall surface. Nevertheless, we know that it \emph{must} take a negative value at the exterior boundary of the boundary layer. Therefore, it must change sign somewhere in the boundary. This point where $\inlinesecondpartialderivative{u}{y}$ changes sign is called \vocab{inflexion point}.
		
		The existence of the inflection point within the boundary layer tells us that at the wall ($y=0$) the term $\inlinepartialderivative{u}{y}$ tends towards ever smaller values. Given enough distance $x$, it will reach zero value, and the boundary layer will separate (\cref{fig_bl_sep_inflection}). Therefore, the longitudinal pressure gradient, which in practice determines the longitudinal velocity gradient, is the key factor in the analytical prediction of separation.
			\begin{figure}
				\begin{center}
					\includegraphics[width=10cm]{boundary_layer_inflection_point.png}
				\end{center}
				\supercaption{The inflection point within a boundary layer about to separate.}{\wcfile{Boundary layer inflection point.svg}{Figure} \ccby \olivier}
				\label{fig_bl_sep_inflection}
			\end{figure}
		
		\youtubethumb{rzRuZNvkMbc}{a football with a rough edge on one side will see turbulent boundary layer on that side and laminar boundary layer on the other. When the laminar layer separates before the turbulent one, the main flow around the ball becomes asymmetric and deviates the ball sideways, a phenomenon that can be exploited to perform impressive tricks.}{freekikerz (\styl)}
		We shall remember two crucial points regarding the separation of boundary layers:
			\begin{enumerate}
				\item \textbf{Separation occurs in the presence of a positive pressure gradient}, which is sometimes named \vocab{adverse pressure gradient}.\\
						Separation points along a wall (\eg a car bodywork, an aircraft wing, rooftops, mountains) are always situated in regions where pressure increases (positive $\inlinederivative{p}{x}$ or negative $\inlinederivative{U}{x}$). If pressure remains constant, or if it decreases, then the boundary layer cannot separate.
				\item \textbf{Laminar boundary layers are much more sensitive to separation} than turbulent boundary layers (\cref{fig_reynolds_separation_airfoil}).\\
						A widely-used technique to reduce or delay the occurrence of separation is to make boundary layers turbulent, using low-height artificial obstacles positioned in the flow. By doing so, we increase shear-based friction (which increases with turbulence) as a trade-off for better resistance to stall.
			\end{enumerate}
		
			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{reynolds_number_effect_on_airfoil_flow.png}
				\end{center}
				\supercaption{The effect of decreasing Reynolds number on flow attachment over an airfoil at constant angle of attack, with the transition point highlighted. Laminar boundary layers are much more prone to separation than turbulent boundary layers.}{\wcfile{Reynolds Number Effect on Airfoil Flow.svg}{Figure} \ccbysa \olivier, based on Barlow \& Pope 1999~\cite{barlowraepope1999}}
				\label{fig_reynolds_separation_airfoil}
			\end{figure}

		Predicting in practice the position of a separation point is difficult, because an intimate knowledge of the boundary layer profile and of the (external-flow-generated) pressure field are required —\ and as the flow separates, these are no longer independent. Resorting to experimental measurements, in this case, is often a wise idea!

\begin{comment}
%%%%%%%%%%%%%%%%%
	\clearfloats %handmade
	\subsection{Separation according to Pohlhausen}

		In the case of a laminar flow along a smooth surface, Pohlhausen’s model allows for interesting results. Indeed, at the separation point, $\tau_\text{wall} = 0$, thus:		
		\begin{IEEEeqnarray}{rCl}
			\tau_\text{wall} = 0 = \mu \left(\partialderivative{u}{y}\right)_{y=0}	&=& \mu \left(\frac{U}{\delta} g'\right)_{Y=0}\nonumber\\
																				0	&=& g'_{Y=0} = a\nonumber\\
																		\Lambda		&=& \num{-12}\\\nonumber\\
													\delta^2 \frac{\rho}{\mu} \frac{\diff U}{\diff x}	&=& \num{-12}
		\end{IEEEeqnarray}
		
		This equation allows us to predict the thickness of the boundary layer at the point where separation occurs, and therefore its position, if the velocity gradient $\diff U/\diff x$ is known.
		
		We can see here that once we have obtained an extensive description of the flow within the boundary layer, we can generate a large amount of useful information regarding the general flow around the object.
		
		Unfortunately, Pohlhausen’s model’s agreement with experimental measurements is not very good, and it can only usefully predict the position of the separation point in a few simple cases. Nevertheless, the overall thought process,~i.e.
		
		\begin{centering}
			intuition of a simple model\\
			$\downarrow$\\
			non-dimensionalization\\
			$\downarrow$\\
			correlation with known boundary conditions\\
			$\downarrow$\\
			exploitation of the model to predict flow behavior\\
		\end{centering}

		~\\	
		is what should be remembered here, for it is typical of the research process in fluid dynamics.
\end{comment}


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solved problems}

\begin{youtubesolution}

	\begin{center}
		\textbf{Advertising board on a car}
	\end{center}

	A successful fluid dynamics professor advertises for their course using a board above their car. They drive at \SI{10}{\metre\per\second}; the board is \SI{3}{\metre} long and \SI{1.5}{\metre} high.

	\begin{center}
	\vspace{-0.5cm}%handmade
	\includegraphics[width=0.45\textwidth]{car_board}\\
	{{\tiny \wcfile{2d_car.jpg}{Drawing} \ccbysa by Imad Kharkouk}}
	\vspace{-0.5cm}%handmade
	\end{center}

	Will the boundary layer on the board become turbulent? How thick will it become?\vspace{-0.5cm}%handmade

	\seesolution{ML5dqjHeqPE}

\end{youtubesolution}


\begin{youtubesolution}

	\begin{center}
		\textbf{Shear force on a board (laminar part)}
	\end{center}

	In the example above, what is the shear force on the laminar layer part of the board?

	\seesolution{xxoqwzihRMc}
	{\small Note: Unfortunately Olivier made an error in this video: the final expression is correct, but improperly calculated. The correct result is \SI{0,107}{\newton}. Many thanks to the students who double-checked and reported the problem!}

\end{youtubesolution}


\begin{youtubesolution}

	\begin{center}
		\textbf{Shear force on a board (turbulent part)}
	\end{center}

	In the example above, what is the shear force on the turbulent layer part of the board? And what would be the power lost to friction on the entire board?

	~
	
	~
	
	~
	
	~

	\seesolution{6i_yu1BKkVY}

\end{youtubesolution}


\atendofchapternotes

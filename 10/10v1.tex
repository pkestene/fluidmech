\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://framagit.org/olivier/sensible-styles

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger preamble)
\printversion

\begin{document}

\sectionstar{Basics of the boundary layer}


%%%%%%%%%%
%\subsectionstar{Rationale}

	\begin{frame}
	
		Once upon a time, a fluid dynamicist…
		
	\end{frame}
	
	\begin{frame}
		In 1904, Ludwig Prandtl \emph{split the problem in halves}\pause
		
		\begin{itemize}
			\item A tiny area just next to walls is friction-dominated…\pause
			\item but the rest of the flow is nearly inviscid.\pause
		\end{itemize}
		
		And he said,\\\pause
		“let’s call this the \vocab{boundary layer}”.
	\end{frame}
	
	\pictureframe{1}{Ludwig_Prandtl_1904}{Ludwig Prandtl, 1904}{\wcfile{Ludwig Prandtl 1904.jpg}{Photo} from the archives of Universität Göttingen (1904, \pd)}
	
	\begin{frame}
		
		Boundary layer: a \emph{concept}\pause
		
		Zone between wall and point where velocity is \SI{99}{\percent} of external velocity\pause
		
		A blurry and sometimes un-definable boundary!
	\end{frame}

	% Wind tunnel boundary layer measurement
	\oldpictureframe{}{im_02437.jpg}{1}{Photo \ccbysa \olivier}
	\oldpictureframe{}{im_02436_modified.jpg}{1}{Photo \ccbysa \olivier}
	\oldpictureframe{}{im_02434.jpg}{1}{Photo \ccbysa \olivier}

	\oldfigureframe{}{boundary_layer_thickness_concept.png}{1}{figure \ccby \olivier}

	\begin{frame}
	
		Thickness depends strongly of flow conditions:\pause
		
		It \emph{decreases} when speed is increased\\\pause
		or when viscosity is decreased(!)
	\end{frame}
	
	\figureframe{0.8}{boundary_layer_thickness_speed}{}{\wcfile{Boundary layer thickness and flow speed.svg}{Figure} \ccby \olivier}
	
	\begin{frame}
	
		Boundary layer flow always starts laminar…\pause
		
		…up until the \vocab{transition point}. \pause
		
		Turbulent boundary layer: greater thickness, larger growth, more dissipation.
	\end{frame}
	
	\begin{frame}
	
		We may have laminar boundary layer in a turbulent flow;\pause
		
		and turbulent boundary layers are commonplace in laminar flows!
	\end{frame}
	
	\pictureframe{1}{Airbus_A319_Austrian_Airlines}{}{\wcfile{Airbus_A319_Austrian_Airlines.jpg}{Photo} \ccbysa by Austrian Airlines}
	\pictureframe{1}{im_02631}{}{Photo \cczero \oc}


%%%%%%%%%%
\subsectionstar{Why do we study the boundary layer?}

	\begin{frame}
		\begin{centering}
		Should I spend time studying\\
		a puny half-millimeter?\pause
		
		\emph{yes}.
		
		\end{centering}
	\end{frame}
	
	\begin{frame}
		\begin{enumerate}
			\item we \textbf{never solve the full Navier-Stokes equations}!\pause
			
			~
			
			Inside: \emph{model} velocity distribution\\\pause
			Outside: neglect viscous effects					
		\end{enumerate}
	\end{frame}

	\figureframe{1}{Types_of_flow_analysis_in_fluid_mechanics}{Three regions in flow analysis}{\wcfile{Types of flow analysis in fluid mechanics.svg}{Figure} \ccbysa \olivier}

	\skipinprint{
	\oldpictureframe{We never solve the full Navier-Stokes equations!}{rtSNJbX.jpg}{0.65}{}
	}

	\begin{frame}
		\begin{enumerate}
		\shift{1}
		\item We \textbf{quantify shear forces}.\\\pause
		~\\
		A good boundary layer solution\\
		= a quantification of wall friction
		\end{enumerate}
	\end{frame}
	
	\begin{frame}
		\begin{enumerate}
		\shift{2}
		\item We predict \textbf{flow separation}.\\\pause
		~\\ 
		Boundary layer control is key to imparting a given trajectory on a fluid!
		\end{enumerate}
	\end{frame}

	\pictureframe{1}{1915ca_abger_fluegel_(cropped_and_mirrored).jpg}{}{\wcfile{File:1915ca abger fluegel (cropped and mirrored).jpg}{Photo} \ccbyde DLR (1915)}


%%%%%%%%%%
\subsectionstar{How to measure your boundary layer}
	
	\begin{frame}
		Three parameters to quantify boundary layer thickness\pause
			
		\begin{enumerate}
			\item The \vocab{thickness} $\delta$,\\\pause
				\begin{IEEEeqnarray*}{rCl}
					\delta &\equiv& y_{u=\num{0,99}U}
				\end{IEEEeqnarray*}\pause
		
			$\to$ distance at which velocity $u$ is \SI{99}{\percent} of~$U$.
		\end{enumerate}
	\end{frame}
	
	\begin{frame}
		\begin{enumerate}
		\shift{1}
		\item The \vocab{displacement thickness} $\delta^*$ :\pause
		
		What is the distance by which the flow streamlines are shifted away from the wall?\pause
	
		\begin{IEEEeqnarray*}{rCl}
			\delta^* &\equiv& \int_0^\infty \left( 1 - \frac{u}{U}\right) \diff y
		\end{IEEEeqnarray*}

		\end{enumerate}
	\end{frame}
	\figureframe{1}{thicknesses0}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}
	\figureframe{1}{thicknesses1}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}
	\figureframe{1}{thicknesses1b}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}
	\figureframe{1}{thicknesses2}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}
	\figureframe{1}{thicknesses3}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}
	
	\figureframe{0.8}{plate_integral_analysis}{}{\wcfile{Velocity profiles integral analysis.svg}{Figure} \cczero \oc}
	
	\begin{frame}
		\begin{enumerate}
		\shift{1}
		\item The \vocab{displacement thickness} $\delta^*$ :
		
		What is the distance by which the flow streamlines are shifted away from the wall?
	
		\begin{IEEEeqnarray*}{rCl}
			\delta^* &\equiv& \int_0^\infty \left( 1 - \frac{u}{U}\right) \diff y
		\end{IEEEeqnarray*}

		\end{enumerate}
	\end{frame}

	\begin{frame}
		\begin{enumerate}
		\shift{2}
		\item The \vocab{momentum thickness} $\delta^{**}$. \pause
		
		How thick is the fluid layer that we would need to remove in order to generate the same drag as the boundary layer?\pause
	
		\begin{IEEEeqnarray*}{rCl}
			\delta^{**} &\equiv& \int_0^\infty \frac{u}{U} \left( 1 - \frac{u}{U}\right) \diff y
		\end{IEEEeqnarray*}

		\small great tool to predict boundary layer separation
		\end{enumerate}
	\end{frame}
	\figureframe{1}{thicknesses3}{}{\wcfile{Boundary layer velocity thickness and displacement thickness.svg}{Figure} \ccbysa by \olivier}

	\figureframe{1}{thicknesses4}{}{\wcfile{Boundary layer velocity, displacement and momentum thicknesses.svg}{Figure} \ccbysa by \olivier}
	
	
	
	
	\begin{frame}
	
		What about wall shear $\tau_\text{wall}$ ?\pause
		
		easy:
		\begin{IEEEeqnarray*}{rCl}
			\tau_{\text{wall}\ yx} &=& \mu \partialderivative{u}{y}
		\end{IEEEeqnarray*}\pause
		this expression is a function of $x$ \pause
		
		\small (note: $\tau_\text{wall}$ decreases with distance)
	\end{frame}
	
	\begin{frame}
		Useful parameter: the \vocab{shear coefficient},\pause
		\begin{IEEEeqnarray*}{rCl}
			c_{f_{(x)}} &\equiv& \frac{\tau_\text{wall}}{\frac{1}{2} \rho U^2}
		\end{IEEEeqnarray*}\pause
		\small (also a function of $x$)
	\end{frame}




\end{document}

\renewcommand{\lastedityear}{2021}
 \renewcommand{\lasteditmonth}{04}
   \renewcommand{\lasteditday}{03}
\renewcommand{\numberofthischapter}{8}
\renewcommand{\titleofthischapter}{\namechaptereight}

\fluidmechchaptertitle
\label{chap_eight}

\mecafluboxen

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	In this chapter, we develop tools to analyze scale effects in fluid mechanics. This study should allow us to answer two questions:
		\begin{itemize}
			\item How can we adequately reproduce a flow in an enlarged or reduced version?
			\item How do forces and powers change when the flow is scaled?
		\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Comparing influences: the weighted momentum balance]{Comparing influences:\\ the weighted momentum balance}
\label{ch_scaling_flows}

	\subsection{Principle}

	\vimeothumb{69445362}{when their movement is filmed and then viewed sped-up, fog banks can appear to flow like water. What time-lapse rate is required to achieve physical similarity?}{Simon Christen (\stvl)}
	In many practical engineering situations which involve fluid flow, investigating the flow in the real application is impossible or impractical. 
	In those cases it is common practice to build scaled-up or scaled-down versions of the flow in a laboratory. This brings up the question: how should the model flow properties be adapted to represent the original flow? For example, if the model is half as small as the original, should the velocity be halved? Or perhaps doubled?

	The answer to this problem is as follows: two flows are \vocab{dynamically similar} (\ie representative of one another) when their \vocab{flow parameters} are the same.\\
	In order to understand what this means, we will need to look back at the Navier-Stokes equation (the momentum balance equation we derived in \chaptersixshort), re-writing it in a non-dimensional form. Onwards!
	
	\protip{portrait27}{0.325}{0.325}{8}{
	Making models is useful when you have a large machine, say, a jet airliner or a submarine, and you want to study different configurations without building a new full-scale machine each time. But there are other applications: imagine trying to study the flow of molten metal in a furnace, the flow of blood in a beating heart, or the flow of air around a mosquito’s wings. Making models and understanding scale effects will allow you to investigate fluid flow in all kind of inaccessible locations. That makes the math worth the effort!}
	
	
	\subsection{The non-dimensional Navier-Stokes equation}
	\label{ch_non_dim_ns}

		Here, we want to obtain an expression for the Navier-Stokes equation for incompressible flow which allows for an easy comparison of its constituents. We start with the original equation, which we derived in \chaptersixshort as equation~\ref{eq_navierstokes} p.~\pageref{eq_navierstokes}:
			\begin{IEEEeqnarray}{rCl}
				\rho \partialtimederivative{\vec V} + \rho \advective{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}\label{eq_navierstokes_repeat}
			\end{IEEEeqnarray}

		What we would now like to do is \emph{separate the geometry from the scalars} in this equation. The principle is to express each vector $\vec{A}$ as the multiple of its length $A$ and a \vocab{non-dimensional} vector $\vec{A^*}$\ , which has the same direction as $\vec{A}$ but only unit length.
		
		In order to achieve this, we introduce a series of non-dimensional physical terms, starting with non-dimensional time $t^*$, defined as time~$t$ multiplied by the frequency~$f$ (in \si{Hertz}) at which the flow repeats itself:
			\begin{IEEEeqnarray}{rCl}
				 t^* &\equiv& ft
			\end{IEEEeqnarray}
		A flow with a very high frequency is highly unsteady, and the changes in time of the velocity field will be relatively important. On the other hand, the lower the frequency, and the closer the flow is to being steady. In all cases, as we observe the flow, non-dimensional time $t^*$ progresses from 0 to 1, after which the solution is repeated.
		
		We then introduce non-dimensional velocity $\vec V^*$, a unit vector field equal to the velocity vector field divided by its own (scalar field) length $V$:
			\begin{IEEEeqnarray}{rCl}
				 \vec V^*	&\equiv& \frac{\vec V}{V}
			\end{IEEEeqnarray}
		
		Pressure $p$ is non-dimensionalized differently, since in fluid mechanics, it is the pressure \emph{changes} across a field, not their absolute value, that influence the velocity field. For example, in eq.~\ref{eq_navierstokes_repeat} $\gradient{p}$ can be replaced by $\gradient{(p - p_\infty)}$ (in which $p_\infty$ can be any constant faraway pressure). Now, the pressure field $p - p_\infty$ is non-dimensionalized by dividing it by a reference pressure difference $p_0 - p_\infty$, obtaining:
			\begin{IEEEeqnarray}{rCl}
				 p^* &\equiv& \frac{p - p_\infty}{p_0 - p_\infty}
			\end{IEEEeqnarray}
		If $p_0$ is taken to be the maximum pressure in the studied field, then~$p^*$ is a scalar field whose values can only vary between~0 and~1.
		
		Non-dimensional gravity $\vec{g^*}$ is simply a unit vector equal to the gravity vector~$\vec g$ divided by its own length~$g$:
			\begin{IEEEeqnarray}{rCl}
				 \vec g^*	&\equiv& \frac{\vec g}{g}
			\end{IEEEeqnarray}
		
		And finally, we define a new operator, the \vocab{non-dimensional del}~$\gradient^*$,
			\begin{IEEEeqnarray}{rCl}
				 \gradient^* 	&\equiv& L \ \gradient
			\end{IEEEeqnarray}
			which ensures that vector fields obtained with a non-dimensional gradient, and the scalar fields obtained with a non-dimensional divergent, are “scaled” by a reference length $L$.

		These new terms allow us to replace the constituents of equation~\ref{eq_navierstokes_repeat} each by a non-dimensional “unit” term multiplied by a scalar term representing its length or value:
			\begin{IEEEeqnarray*}{rCl}
				 t				&=& \frac{t^*}{f}\\
				 \vec V			&=& V \ \vec V^*\\
				 p - p_\infty	&=& p^* \ (p_0 - p_\infty)\\
				 \vec g			&=& g \ \vec g^*\\
				 \gradient		&=& \frac{1}{L} \gradient^*
			\end{IEEEeqnarray*}

		Now, inserting these in equation~\ref{eq_navierstokes_repeat}, and re-arranging, we obtain:
		\begin{adjustwidth}{-1.9cm}{0cm}%handmade
			\begin{IEEEeqnarray}{rCl}
				\rho  \partialderivative{}{\frac{1}{f} t^*} \left(V \vec V^*\right) + \rho \left(V \vec V^* \cdot \frac{1}{L} \gradient^* \right) \left(V \vec V^*\right)  & = &	\rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty) + p_\infty\right] + \mu \frac{\gradient^{*2}}{L^2} \left(V \vec V^*\right)\nonumber\\
				\rho V f \partialderivative{\vec V^*}{t^*} + \rho V V \frac{1}{L} \left(\vec V^* \cdot \gradient^*\right) \vec V^*  & = &	\rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty)\right] + \mu V \frac{1}{L^2}\gradient^{*2} \vec V^*\nonumber\\
				\left[\rho V f\right]\ \partialderivative{\vec V^*}{t^*} + \left[\frac{\rho V^2}{L}\right]\ \left(\vec V^* \cdot \gradient^*\right) \vec V^*  & = &	\left[\rho g\right]\ \vec g^* - \left[\frac{p_0 - p_\infty}{L}\right]\ \gradient^* p^* + \left[\frac{\mu V}{L^2}\right]\ \gradient^{*2} \vec V^*\nonumber\\\label{eq_nsndtmp}
			\end{IEEEeqnarray}
		\end{adjustwidth}
		
		\youtubethumb{KD3MIqUgN1A}{Figuring out the non-di\-men\-sional Navier-Stokes equation}{\oc (\ccby)}{}
		In equation~\ref{eq_nsndtmp}, the terms in brackets each appear in front of non-di\-men\-sion\-al (unit) vectors. These bracketed terms all have the same dimension, \ie \si{\kilogram\per\metre\squared\per\second\squared}. Multiplying each by $\frac{L}{\rho V^2}$ (of dimension \si{\metre\squared\second\squared\per\kilogram}), we obtain a purely non-dimensional equation:
		\begin{adjustwidth}{-0.1cm}{0cm}%handmade
			\begin{IEEEeqnarray}{rCl}
				\left[\frac{fL}{V}\right]\ \partialderivative{\vec V^*}{t^*} + \left[1\right]\ \left(\vec V^* \cdot \gradient^*\right) \vec V^*  & = &	\left[\frac{gL}{V^2}\right]\ \vec g^* - \left[\frac{p_0 - p_\infty}{\rho V^2}\right]\ \gradient^* p^* + \left[\frac{\mu}{\rho V L}\right]\ \gradient^{*2} \vec V^*\nonumber\\\label{eq_navierstokes_nondim_tmp}
			\end{IEEEeqnarray}
		\end{adjustwidth}
		
		Equation~\ref{eq_navierstokes_nondim_tmp} does not bring any information on top of the original incompressible Navier-Stokes equation (eq.~\ref{eq_navierstokes_repeat}). Instead, it merely separates it into two distinct kinds of components. The first, in square brackets, are a scalar fields (purely numbers), which indicate the magnitude of the acceleration field. The others are unit vector fields (fields of oscillating vectors, all of length 1, and noted with stars), which represent the geometry (direction) of the acceleration field. In this form, we can more easily observe and quantify the weight of the different terms relative to one another. At this point, it is time to introduce the notion of \vocab{flow parameters}.


	\subsection{The flow parameters of Navier-Stokes}

		From here on, in equation~\ref{eq_navierstokes_nondim_tmp}, we convene to call the terms written in brackets \vocab{flow parameters}, and label them with the following definitions:
			\begin{IEEEeqnarray}{rCl}
				 \str		&\equiv& \frac{f\ L}{V} \label{eq_def_str}\\
				 \eu		&\equiv& \frac{p_0 - p_\infty}{\rho\ V^2} \label{eq_def_eu}\\
				 \fr		&\equiv& \frac{V}{\sqrt{g\ L}} \label{eq_def_fr}\\
				 \re		&\equiv& \frac{\rho\ V\ L}{\mu} \label{eq_def_re}
			\end{IEEEeqnarray}

		This allows us to re-write eq.~\ref{eq_navierstokes_nondim_tmp} as:
				\begin{IEEEeqnarray*}{rCl}
					\str\ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^*  & = &	\left[\frac{1}{\text{Fr}^2}\right]\ \vec g^* - \eu\ \gradient^* p^* + \left[\frac{1}{\text{Re}}\right]\ \gradient^{*2} \vec V^*
				\end{IEEEeqnarray*}

		And finally, we arrive at the simple, elegant and formidable \vocab{non-dimensional incompressible Navier-Stokes equation} expressed with flow parameters:
		\begin{mdframed}
			\begin{IEEEeqnarray}{rCl}
				\str\ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^*  & = &	\frac{1}{\fr^2}\ \vec g^* - \eu\ \gradient^* p^* + \frac{1}{\re}\ \gradient^{*2} \vec V^*\nonumber\label{eq_ns_nondim}\\
			\end{IEEEeqnarray}
		\end{mdframed}

		\xkcdthumb{762}{comparing things}{0.4}{}
		Equation~\ref{eq_ns_nondim} is an incredibly useful tool in the study of fluid mechanics, for two reasons:
		\begin{enumerate}
			\item It allows us to quantify the \textbf{relative weight} of the different terms in a given flow.\\
					In this way, it serves as a compass, helping us determine which terms can safely be neglected in our attempts to find a particular flow solution, merely by quantifying the four parameters $\str$, $\eu$, $\fr$, and $\re$.
			\item It allows us to obtain \textbf{dynamic similarity} between two flows at different scales.\\
					In order that a fluid flow be representative of another flow (for example in order to simulate airflow around an aircraft with a model in a wind tunnel), it must generate the same vector field $\vec V^*$. In order to do this, we must generate an incoming flow with the same four parameters $\str$, $\eu$, $\fr$, and $\re$.
		\end{enumerate}
	
		Let us therefore take the time to explore the signification of these four parameters.

		\begin{description}
			\item [The Strouhal number] $\str \equiv {f\ L}/{V}$ (eq.~\ref{eq_def_str}) quantifies the influence of unsteadiness effects over the acceleration field. It does this by evaluating the importance of the representative frequency~$f$ at which the flow pattern repeats itself. Very high frequencies denote highly unsteady flows. When the frequency is very low, $\str$ is very small; the flow is then quasi-steady and it can be solved at a given moment in time as if it was entirely steady.
			
			\item [The Euler number] $\eu \equiv {p_0 - p_\infty}/{\rho\ V^2}$ (eq.~\ref{eq_def_eu}) quantifies the influence of the pressure gradient over the acceleration field. It does this by comparing the largest relative pressure~$p_0 - p_\infty$ to the flow of momentum in the flow field. The greater $\eu$ is, and the more the changes in the velocity field~$\vec V$ are likely to be caused by pressure gradients rather than viscosity, convection or unsteadiness.
			
			\item [The Froude number] $\fr \equiv {V}/{\sqrt{g\ L}}$ (eq.~\ref{eq_def_fr}) quantifies the relative importance of gravity effects. In practice, gravity effects only play an important role in free surface flows, such as waves on the surface of a water reservoir. In most other cases, gravity contributes only to a hydrostatic effect, which has little influence over the velocity field.
			
			\item [The Reynolds number] $\re \equiv {\rho\ V\ L}/{\mu}$ (eq.~\ref{eq_def_re}, also eqs.~\ref{eq_def_reynolds_number} p.~\pageref{eq_def_reynolds_number} \& \ref{eq_def_reynolds_number_two} p.~\pageref{eq_def_reynolds_number_two}) quantifies the influence of viscosity over the acceleration field. It does this by comparing the magnitude of inertial effects ($\rho V L$) to viscous effects ($\mu$). When $\re$ is very large, viscosity plays a negligible role and the velocity field is mostly dictated by the inertia of the fluid. We return to the significance of the Reynolds number in~\S\ref{ch_reynolds_number_practice} below.
			
			\item [The Mach number] $\ma \equiv V/c$ (eq.~\ref{eq_def_ma} p.~\pageref{eq_def_ma}) compares the flow speed $V$ with that of the molecules within the fluid particles (the speed of sound~$c$). $\ma$ does not appear in equation~\ref{eq_ns_nondim}, because we already decided to restrict ourselves to non-compressible flows. If we hadn’t, we would be re-expressing the pressure term in that equation as a function of $\ma$, quantifying the effect of changes in density.
		\end{description}

		\label{ch_scalar_field_or_number}
		These five flow parameters should be thought of \emph{scalar fields} within the studied flow domain: there is one distinct Reynolds number, one Mach number etc. for each point in space and time. Nevertheless, when describing fluid flows, the custom is to choose for each parameter \emph{a single representative value} for the whole flow. For example, when describing pipe flow, it is customary to quantify a representative Reynolds number $\reD$ based on the average flow velocity~$V_\av$ and the pipe diameter~$D$ (as we have seen with eq.~\ref{eq_def_red} p.~\pageref{eq_def_red}), while the representative Reynolds number for flow over an aircraft wing is often based on the free-stream velocity~$V_\infty$ and the wing chord length. Similarly, the flight Mach number~$\ma_\infty$ displayed on an aircraft cockpit instrument is computed using the relative free-stream air speed~$V_\infty$ and the free-stream speed of sound~$c_\infty$, rather than particular values measured closer to the aircraft.

	\subsection{Flow parameters obtained as force ratios}

		Instead of the mathematical approach covered above, the concept of flow parameter can be approached by \emph{comparing forces} in fluid flows. This method is described for reference in Appendix~\ref{appendix_flow_parameters} p.~\pageref{appendix_flow_parameters}.


	\subsection{The Reynolds number in practice}
	\label{ch_reynolds_number_practice}
	\label{ch_inertial_viscous_effects}%legacy
		
		\youtubethumb{51-6QCJTAjU}{half-century-old, but timeless didactic exploration of how the dynamics of fluids change with the Reynolds number, with accompanying notes by G.\ I.\ \mbox{Taylor}\cite{taylor1967}}{the National Committee for Fluid Mechanics Films (\textsc{ncfmf}, 1967\cite{ncfmfmit}) (\styl)}
		Among the five non-dimensional parameters described above, the \vocab{Reynolds number} $\re$ is by far the most relevant in the study of most fluid flows, and it deserves a few additional remarks. As we have seen, the Reynolds number is a measure of how little effect the viscosity has on the time-change of the velocity vector field:
			\begin{itemize}
				\item With low $\re$, the viscosity~$\mu$ plays an overwhelmingly large role, and the velocity of fluid particles is largely determined by that of their own neighbors;
				\item With high $\re$, the momentum~$\rho V$ of the fluid particles plays a more important role than the viscosity~$\mu$, and the inertia of fluid particles affects their trajectory much more than the velocity of their neighbors.
			\end{itemize}
		
		In turn, this gives the Reynolds number a new role in characterization of flows: it can be thought of as \textbf{the likeliness of the flow being turbulent over the length $L$}. Indeed, from a kinematic point of view, viscous effects are highly stabilizing: they tend to harmonize the velocity field and smooth out disturbances. On the contrary, when these effects are overwhelmed by inertial effects, velocity non-uniformities have much less opportunity to dissipate, and at the slightest disturbance the flow will become and remain turbulent (\cref{fig_screencap_reynolds}). This is the reason why the quantification of a representative Reynolds number is often the first step engineers and scientists take when studying a fluid flow.

			\begin{figure}
				\begin{center}
					\includegraphics[width=0.85\textwidth]{images/NCFMF_film_lowre_screencap_reynolds_comparison.png}
				\end{center}
				\supercaption{A viscous opaque fluid is dropped into a clearer receiving static fluid with identical viscosity. The image shows four different experiments photographed after the same amount of time has elapsed. The viscosity is decreased from left to right, yielding Reynolds numbers of \num{0,05}, \num{10}, \num{200} and \num{3000} respectively.\\
				As described in eq.~\ref{eq_re_forces} p.~\pageref{eq_re_forces}, a low Reynolds number indicates that viscous effects dominate the acceleration field. As the Reynolds number increases, the nature of the velocity field changes until it becomes clearly turbulent.}%
				{By the National Committee for Fluid Mechanics Films (\textsc{ncfmf}, 1967\cite{ncfmfmit}), with accompanying notes by \mbox{Taylor}\cite{taylor1967}\\
				Image \copyright 1961-1969 Educational Development Center, Inc., reproduced under Fair Use doctrine\\
				A screen capture from film \textit{Low Reynolds Number Flow} at \href{http://web.mit.edu/hml/ncfmf.html}{http://web.mit.edu/hml/ncfmf.html}}
				\label{fig_screencap_reynolds}
			\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Making models}


		We now know that when we create a scaled-down or scaled-up version of one flow (as shown for example in \cref{fig_dynamic_similarity}), we attempt to keep all flow parameters identical.
		In practice, this is extremely difficult to do, as we will see while going through this chapter’s problem sheet, especially if ordinary fluids (air or water) are to be used. The practice in science and engineering is usually to focus on one or two key parameters, while ignoring others. If the budget allows for it, several models may be built, each focusing on one parameter (\eg one model for compressibility effects, one model for viscous effects).
			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=0.49\textwidth]{images/Lockheed_C-141_Model_in_TDT_-_GPN-2000-001741.jpg}
					\includegraphics[width=0.49\textwidth]{images/starlifter_full_size.jpg}
				\end{center}
				\supercaption{In order for the flow around a wind tunnel model to be representative of the flow around the real-size aircraft (here, a \SI{48}{\metre}-wide \we{Lockheed C-141 Starlifter}), dynamic similarity must be obtained. The value of all flow parameters must be kept identical. This is not always feasible in practice.}%
				{\wcfile{Lockheed C-141 Model in TDT - GPN-2000-001741.jpg}{Wind tunnel photo} by NASA (\pd)\\
				\flickrfile{peterlong/2350813226/}{Full-size aircraft photo} \ccby by \flickrname{peterlong}{Peter Long}}
				\label{fig_dynamic_similarity}
			\end{figure}
		
		In the design and construction of models, practical constraints must be balanced against the need to reproduce the dynamics of fluids accurately. They include:
		\begin{itemize}
			\item Cost of production. The volume of a model typically increases with its length \emph{cubed}, \ie doubling its length multiplies its volume by a factor~8;
			\item Precision of manufacturing. Usually, the smaller the model, and the smaller the geometrical accuracy that can be reached;
			\item Ease of instrumenting. Carrying out measurements over extremely large or extremely small models may be challenging;
			\item Ease of optical access. Optical flow measurement devices are usually preferred because they do not obstruct the flow, but when the flow is internal to a model, they require transparent, flat walls to work correctly;
			\item Performance of large-scale laboratory equipment. Wind tunnels and water channels are expensive installations. Often, the characteristics of models is adapted to the equipment’s availability and performance, rather than the other way around.
		\end{itemize}
		
		Finally, it must be noted that in this chapter, we focus on reproducing flow dynamics only. Other kinds of forces applying on models, and other physical phenomena, are not taken into account. For example, an airplane model in a tunnel may not be able to fly, and a boat model may not be able to float, because they are made of different materials than the original objects, and because weight and lift forces do not scale together (this is also the reason why large birds such as condors or swans do not look like, and cannot fly as slowly as mosquitoes and bugs! — these ideas are beautifully and smartly explored by Hendrik Tennekes \cite{tennekes1992, tennekes2009}). Similarly, heat transfer or chemical reaction rates, may be completely off in the model, unless the physical laws that govern them are also taken into account.
		\dontbreakpage\vspace{-0.1cm}
		\protip{portrait16}{0.2}{0.2}{6}{
		When your small wind tunnel already cost half a million euros to build, is run by fans with hundreds of kilowatts of power, and your new laser-Doppler velocity measurement system cost the lab 200~k€, it is clear that the available laboratory equipment will dictate the model size and the experimental flow conditions, not the other way around.\\
		A well-designed experiment will always take advantage of the best performance of the available equipment. This is why fluid dynamicists like to measure the performance of their wind tunnel not with size or speed, but with a \emph{tunnel Reynolds number} — the maximum Reynolds number you can subject a model to in the test section.}
		
		
		\begin{comment}
		Several levels of similarity may be achieved between two experiments:
		\begin{itemize}
			\item geometrical similarity (usually an obvious requirement), by which we ensure similarity of shape between the solid boundaries, which are then all related by a scale factor $r$;
			\item kinematic similarity, by which we ensure that the motion of all solid and fluid particles is reproduced to scale — thus that for a given time scale ratio $r_t$ the relative positions are scaled by $r$, the relative velocities by $r/r_t$ and the relative accelerations by $(r/r_t)^2$;
			\item dynamic similarity, by which we ensure that not only the movement, but also the \emph{forces} are all scaled by the same factor.
		\end{itemize}
		In more complex cases, we may want to observe other types of similarity, such as thermal or chemical similarity (for which ratios of temperature or concentrations become important).
		\end{comment}
		
		
		

		
		


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Comparing results: coefficients}

	\subsection{Principle}
	
		The general rule that we follow when we compare measurements from experiments at different scale is that
		\begin{mdframed}
		in two physically similar experiments, the force and power coefficients are the same.
		\end{mdframed}
		
		In other words, for scientists and engineers, the hard work is making a model where the flow coefficients are the same (and thus the original flow and the model are physically similar). Once this is done, the comparison of results is easy: the force and power coefficients are the same on the model and in the original flow.
		

	\subsection{Force coefficients}

		\youtubethumb{uGr_dloHWnI}{Comparing fluid-induced forces: using force coefficients}{\oc (\ccby)}{}
		In order to compare forces, we want to have a sense of how flow-induced forces scale when fluid flows are scaled. A look back on \chaptertwo, and in particular eq.~\ref{eq_linearmom_oned} p.~\pageref{eq_linearmom_oned}, shows us that for a steady flow through a given control volume, the net force induced on the fluid is expressed by:
		\begin{IEEEeqnarray}{rCl}
			\vec F_\text{net on fluid} & = & \Sigma_\net \left[\rho V_\perp A \vec V\right]
		\end{IEEEeqnarray}
		
		This equation tells us that the norm of the net force vector, $F_\net$, is directly related to a term with dimensions of $ \rho |V_\perp| A V$. In our selection of a scale by which to measure $F_\net$, it is therefore sensible to include a term proportional to the the density~$\rho$, a term proportional to the area~$A$, and a term proportional to the square of velocity~$V$. This “scale of fluid-induced force” is conventionally measured using the \vocab{force coefficient} $C_F$:
		\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
			C_F &\equiv& \frac{F}{\frac{1}{2} \rho S V^2} \label{eq_def_force_coefficient}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where \tab $F$ \tab is the considered fluid-induced force (\si{\newton});
			\item		\tab $\rho$ \tab is a reference fluid density (\si{\kilogram\per\metre\cubed});
			\item		\tab $S$ \tab is a reference surface area (\si{\metre\squared});
			\item and	\tab $V$ \tab is a reference velocity (\si{\metre\per\second}).
		\end{equationterms}
		\end{mdframed}
		
		In effect, the force coefficient relates the magnitude of the force exerted by the fluid on an object ($F$) to the rate of flow of momentum towards the object ($\rho S V^2 = \dot m V$).
		
		It is worth making a few remarks about this equation. First, it is important to realize that eq.~\ref{eq_def_force_coefficient} is a \emph{definition}: while the choice of terms is guided by physical principles, it is not a physical law in itself, and there are no reasons to expect $C_F$ (which has no dimension, and thus no unit) to reach any particular value in any given case. The choice of terms is also worth commenting:
		\begin{itemize}
			\item $F$ can be any fluid-induced force; generally we are interested in quantifying either the \vocab{drag} $F_\D$ (the force component parallel to the free-stream velocity) or the \vocab{lift} $F_\text{L}$ (force component perpendicular to the free-stream velocity);
			\item the reference area $S$ is chosen arbitrarily. It matters only that this area grow and shrink in proportion to the studied flow case. In the automobile industry, it is customary to choose the vehicle frontal area as a reference, while in the aeronautical industry the top-view wing area is customarily used. The square of a convenient reference length~$L$ can also be chosen instead;
			\item the choice of reference velocity $V$ and density $\rho$ is also arbitrary, since both these properties may vary in time and space within the studied fluid flow case. It matters merely that the chosen reference values are representative of the case; typically the free-stream (faraway) conditions~$V_\infty$ and~$\rho_\infty$ are used;
			\item the term 1/2 in the denominator is a purely arbitrary and conventional value.
		\end{itemize}
		
		Force coefficients are meaningful criteria to compare and relate what is going on in the wind tunnel and on the full-size object: in each case, we scale the measured force according to the relevant local flow conditions.
		
		\protip{portrait22}{0.23}{0.23}{7}{
		Just like when selecting the length $L$ in the Reynolds number, there is no absolute physical law that would indicate which reference $S$ or $V$ should be taken when calculating a force coefficient. The only important thing is that the definition should remain the same across all scaled flows.\\
		However, a large body of professional customs and cultures usually exists in each sub-area of fluid dynamics. Make sure you consult the available literature and field conventions, so your results fit among them. Calculating the drag coefficient of a car using the car side-view area is not wrong, but it would go a long way to annoy colleagues and competitors!}
		
		
		\youtubethumb{FQxmOQmnaGw}{practical application of scale effects: translating measurements made on a \SI{60}{\percent} size formula one car wind tunnel model into their “real size” race car values, as in problem~\ref{exo_formula_one}}{the Sauber F1 team (\styl)}
		For example, a flow case around a car~A may be studied using a model~B. \emph{If dynamic similarity is maintained} (and that is by no means an easy task!), then the flow dynamics will be identical. The drag force $F_\text{D B}$ measured on the model can then be compared to the force~$F_\text{D A}$ on the real car, using coefficients: since $C_{F_\text{D B}} = C_{F_\text{D A}}$ we have:
		\begin{IEEEeqnarray*}{rCl}
			 F_\text{D A} 	&=& C_{F_\text{D A}} \frac{1}{2} \rho_A S_\A V_\A^2 = C_{F_\text{D B}} \frac{1}{2} \rho_A S_\A V_\A^2 =  \frac{F_\text{D B}}{\frac{1}{2} \rho_B S_\B V_\B^2}  \frac{1}{2} \rho_A S_\A V_\A^2 \\
			F_\text{D A}	&=& \left(\frac{\rho_A}{\rho_B} \frac{S_\A}{S_\B} \frac{V_\A^2}{V_\B^2} \right) F_\text{D B}
		\end{IEEEeqnarray*}

		
		
		
	\subsection{Power coefficient, and other coefficients}
		
		\youtubetopthumb{fF07XkAUELE}{Understanding scale effects will save you a lot of money and embarrassment: the cautionary tale of Howard Hughes}{\oc (\ccby)}{}
		During our investigation of the flow using a small-scale model, we may be interested in measuring not only forces, but also other quantities such as power — this would be an important parameter, for example, when studying a pump, an aircraft engine, or a wind turbine.
		
		We again go back to \chaptertwo, and in particular eq.~\ref{eq_sfee} p.~\pageref{eq_sfee}. This helps us recall that power gained or lost by a fluid flowing steadily through a control volume could be expressed as:
			\begin{IEEEeqnarray}{rCcCl}
			\dot Q_{\net} + \dot W_\text{shaft, net} &=& \Sigma_\net \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]
			\end{IEEEeqnarray}
		Thus, the power gained or lost by the fluid is directly related to the magnitude of the scalar $\Sigma_\net \ \rho |V_\perp| A V^2$. A meaningful ”scale” for the power of a machine can therefore be the amount of energy in the fluid that is made available to it every second, a quantity that grows proportionally to $\rho A V^3$.
		
		This “scale of fluid flow-related power” is conventionally measured using the \vocab{power coefficient} $C_\P$:
		\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
			C_\P &\equiv& \frac{\dot W}{\frac{1}{2} \rho S V^3} \label{def_power_coefficient}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where $\dot W$ is the power added to or subtracted from the fluid (\si{\watt}).
		\end{equationterms}
		\end{mdframed}
		
		
	\subsection{Non-dimensionalizing all the problems}

		\xkcdthumb{687}{abusing dimensional analysis}{0.3}{}
		In this chapter, we have focused on scaling forces and powers from one flow to the other. There are of course other parameters of interest, and for each, it is common practice to define a coefficient. We have already used the \vocab{pressure loss coefficient} $K_L \equiv |\Delta p| / \frac{1}{2} \rho V_\av^2$ in \chaptersevenshort (eq.~\ref{eq_def_loss_coeff} p.~\pageref{eq_def_loss_coeff}), and we shall soon use the \vocab{shear coefficient} $c_f$ in the forthcoming chapter (eq.~\ref{eq_def_shear_coeff} p.~\pageref{eq_def_shear_coeff}). 

		There are many other examples; in fact, in fluid mechanics, the non-di\-men\-sion\-al\-iza\-tion of problems is a very important work methodology. This helps us extrapolate from one or two experiments up to entire families of flows.
		
		\protip{portrait37}{0.25}{0.25}{8}{
		Now you should begin to see that there is more to dimensional analysis than just making models. Being good at comparing the influences of various factors on a fluid flow phenomenon means that you can generalize results. Pressure loss in a pipe, for example, is quantified with a single, suitably-designed parameter $f$, irrespective of say the flow speed or fluid density. Take a look back on the Moody diagram page~\pageref{fig_moody}: it has non-dimensional terms on all three sides! This is the reason why we only need one diagram, instead of a catalog of diagrams (\eg one for each flow speed and diameter).\\
		And so, you should not be surprised when, after you show them the results of your first experimental fluid flow measurement campaign, your colleagues in the fluid dynamics laboratory ask you: “can you non-dimensionalize that diagram?”}


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solved problems}

\begin{youtubesolution}

	\begin{center}
		\textbf{Scale model of a car}
	\end{center}

	\begin{center}
	\includegraphics[width=0.8\textwidth]{quizz1}
	\end{center}

	If we want to reproduce turbulent effects in the airflow around a car using a 1/20 model, how fast should the flow around the model be?

	\seesolution{4p99bCkZdho}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Wind tunnel speed}
	\end{center}
	
	We reproduce the airflow around a full-size car moving in air at \SI{25}{\degreeCelsius}. We use a 1/20 model in a wind tunnel. This tunnel is limited to \SI{20}{\metre\per\second}, but the air is cooled ($T=\SI{0}{\degreeCelsius}$).
	
	What is the real-car speed that is being simulated in the wind tunnel?

	\seesolution{gHf8ouKfAAU}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Wind tunnel measurement}
	\end{center}
	
	In the setup from the previous example, how should the force measurements on the car be translated to values corresponding to the real~car?

	\seesolution{Fn-zxYT9-KY}

\end{youtubesolution}

\atendofchapternotes

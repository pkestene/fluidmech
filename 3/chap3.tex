\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{05}
   \renewcommand{\lasteditday}{22}
\renewcommand{\numberofthischapter}{3}
\renewcommand{\titleofthischapter}{\namechapterthreetitle}

\fluidmechchaptertitle
\label{chap_three}

\mecafluboxen

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	In this chapter, we use the same tools that we developed in \chaptertwoshort, but we improve them so we can apply them to more complex cases. Specifically, we would like to answer the following questions:
	\begin{enumerate}
		\item What are the mass flows and forces involved when a flow has non-uniform velocity?
		\item What are the forces and moments involved when a flow changes direction?
	\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Reynolds transport theorem}

	\subsection{Control volume}
	
		Let us begin, this time, by building a control volume in \emph{any arbitrary flow}: we are no longer limited to one-inlet, one-oulet steady-flow situations. Instead, we will write equations that work inside any generic \vocab{velocity field} $\vec V = (u, v, w)$ which is a function of space and time: $\vec V = f(x, y, z, t)$.
		
		Within this flow, we draw an arbitrary volume named \vocab{control volume} (CV) which is free to move and change shape (\cref{fig_cv}). We are going to measure the properties of the fluid at the borders of this volume, which we call the \vocab{control surface} (CS), in order to compute the net effect of the flow through the volume.
		
			\begin{figure}
				\begin{center}
					\includegraphics[width=0.6\textwidth]{concept_control_volume_system.png}
				\end{center}
				\supercaption{A control volume within an arbitrary flow (compare with figure~\ref{fig_cv_simple} p.~\pageref{fig_cv_simple}). The \vocab{system} is the amount of mass included within the control volume at a given time. At a later time (bottom), it may have left the control volume, and its shape and properties may have changed. The control volume may also change shape with time, although this is not represented here.}{\wcfile{System control volume integral analysis.svg}{Figure} \cczero \oc}\vspace{-0.5cm}%handmade
				\label{fig_cv}
			\end{figure}
		
		At a given time, the control volume contains a certain amount of mass which we call the \vocab{system} (sys). Thus the system is a fixed amount of mass transiting through the control volume at the exact time when we write the equation, and its properties (volume, pressure, velocity etc.) may change in the process.
		
		All along the chapter, we are focusing on the question: based on measured fluid properties at some chosen area in space and time (the properties at the control surface), how can we quantify what is happening to the system (the mass inside the control volume)?


	\subsection{Rate of change of an additive property}
		
		In order to proceed with our calculations, we need to make our accounting methodology even more robust. We start with a “dummy” fluid property $B$, which we will later replace with physical variables of interest.\\
		Let us therefore consider an arbitrary additive property $B$ of the fluid. By the term \vocab{additive} property, we mean that the total \emph{amount of property} is divided if the fluid is divided. For instance, this works for properties such as mass, volume, energy, entropy, but not pressure or temperature.\\
		The \vocab{specific} (\ie per unit mass) value of $B$ is designated $b \equiv B/m$.
	
		We now want to compute the variation of a system’s property $B$ based on measurements made at the borders of the control volume. We will achieve this with an equation containing three terms:
		\begin{itemize}
			\item The time variation of the quantity $B$ within the system is measured with the term $\inlinetimederivative{B_\sys}$.\\ 
			We will use this term to represent, for example, the rate of change of the fluid’s energy or momentum as it travels through a jet engine.
			\item Within the control volume, the enclosed quantity $B_\cv$ can vary by accumulation (for example, mass may be increasing in an air tank fed with compressed air): we measure this with the term $\inlinetimederivative{B_\cv}$.
			\item Finally, a mass flux may be flowing through the boundaries of the control volume, carrying with it some amount of $B$ every second: we write that net flow out of the system as $\dot B_\net \equiv \dot B_\out - \dot B_\inn$.
		\end{itemize}
		
		We can now link these three terms with the simple equation:
			\begin{IEEEeqnarray}{CCCCC}
				\timederivative{B_\sys} & = & \timederivative{B_\cv}	& + & \dot B_\net \label{eq_rtt_basic}\\\nonumber\\
				\begin{array}{c} {\scriptstyle \text{the rate of change}} \\ {\scriptstyle \text{of the fluid’s $B$}} \\ {\scriptstyle \text{as it transits}} \end{array}		& = & \begin{array}{c} {\scriptstyle \text{the rate of change}} \\ {\scriptstyle \text{of $B$ inside}} \\ {\scriptstyle \text{considered volume}} \end{array}									& + & \begin{array}{c} {\scriptstyle \text{the net flow of $B$}} \\ {\scriptstyle \text{at the borders}} \\ {\scriptstyle \text{of the considered volume}} \end{array} \nonumber
			\end{IEEEeqnarray}
		
		Since $B$ may not be uniformly distributed within the control volume, we like to express the term $B_\cv$ as the integral of the volume density $B/\vol$ with respect to volume $\vol$:
			\begin{IEEEeqnarray}{rCl}
				B_\cv &=& \iiint_\cv \frac{B}{\vol} \diff \vol = \iiint_\cv \rho b \diff \vol\\
				\timederivative{B_\cv} &=& \timederivative{} \iiint_\cv \rho b \diff \vol \label{eq_secondbit}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item where	\tab CV			\tab is the control volume,
				\item and	\tab $\vol$		\tab\tab\tab is volume (\si{\metre\cubed}).
			\end{equationterms}
		
		The second term of eq.~\ref{eq_rtt_basic}, $\dot B_\net$, can be evaluated by quantifying, for each area element $\tdiff A$ of the control volume’s surface, the surface flow rate $\rho b V_\perp$ of property $B$ that flows through it, as shown in \cref{fig_cv_da}. The integral over the entire control volume surface CS of this term is:
			\begin{IEEEeqnarray}{rCl}
				\dot B_\net	 = \iint_\cs \rho b V_\perp \diff A = \iint_\cs \rho b \ (\vec V_\rel \cdot \vec n) \diff A \label{eq_thirdbit}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item where	\tab CS			\tab\tab\tab is the control surface (enclosing the control volume CV),
				\item 		\tab $\vec n$ 	\tab\tab\tab\tab is a unit vector on each surface element $\tdiff A$ pointing outwards,
				\item 		\tab $\vec V_\rel$ 	\tab is the local velocity of fluid relative to the control surface,
				\item and 	\tab $V_\perp \equiv \vec V_\rel \cdot \vec n$ is the local cross-surface speed (positive outwards, neg. inwards)
			\end{equationterms}
			\begin{figure}[ht]
				\begin{center}\vspace{-0.25cm}%handmade
					\includegraphics[width=0.7\textwidth]{concept_vrel_vecn.png}
				\end{center}
				\supercaption{Part of the system may be flowing through an arbitrary piece of the control surface with area $\diff A$. The $\vec n$ vector defines the orientation of $\tdiff A$ surface, and by convention is always pointed outwards. The amount of $B$ flowing through this small area per unit time is $\tdiff A$ is $\rho V_\perp \diff A b$}{\wcfile{System control volume integral analysis.svg}{Figure} \cczero \oc}
				\label{fig_cv_da}
				\vspace{-1cm}%handmade
			\end{figure}

		\clearpage
		By inserting equations~\ref{eq_secondbit} and~\ref{eq_thirdbit} into equation~\ref{eq_rtt_basic}, we obtain:\dontbreakpage
			\begin{mdframed}\vspace{-0.5cm}\begin{IEEEeqnarray}{rCl}
				\timederivative{B_\sys} & = & \timederivative{} \iiint_\cv \rho b \diff \vol  +  \iint_\cs \rho b \ (\vec V_\rel \cdot \vec n) \diff A \label{eq_rtt}
			\end{IEEEeqnarray}
			\end{mdframed}
		
		Equation~\ref{eq_rtt} is named the \vocab{Reynolds transport theorem}\wupen{Reynolds transport theorem}; it stands now as a general, abstract accounting tool, but as we soon replace $B$ by meaningful variables, it will prove extremely useful, allowing us to quantify the \emph{net} effect of the flow of a system through a volume for which border properties are known.
		
		In the following sections we are going to use this equation to write out four key balance equations (see \S\ref{ch_conservation_equations} p.~\pageref{ch_conservation_equations}):
		\begin{itemize}
			\item balance of mass;
			\item balance of linear momentum;
			\item balance of angular momentum;
			\item balance of energy.
		\end{itemize}

		\protip{portrait07}{0.23}{0.23}{6}{
			Take some time to observe the (sometimes curious!) terms of equation~\ref{eq_rtt}, and learn the associated vocabulary. Just like an accountant in a company would like to have a very clear method for counting how money is spent and earned, fluid dynamicists need very good tools to describe what’s coming in and out of their flows. When you’ll be hurling three-dimensional vector operations at swirling flows down this chapter, you’ll be glad you learned about control surfaces and sign conventions earlier~on.			
		}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of mass}

	What is the balance of mass for fluid flowing through any arbitrary volume? We answer this question by writing out a mass balance equation in the template provided by the Reynolds transport theorem (eq.~\ref{eq_rtt}).
	
	We now state that the placeholder variable $B$ is mass $m$. It follows that $\inlinetimederivative{B}$ becomes $\inlinetimederivative{m_\sys}$, which by definition is~zero (see eq.~\ref{eq_massconservation} p.~\pageref{eq_massconservation}). Also, $b \equiv B/m = m/m = 1$ and now the Reynolds transport theorem becomes:
	\begin{mdframed}
		\begin{IEEEeqnarray}{CCCCCCC}
			\timederivative{m_\sys} & = & 0 & = & \timederivative{} \iiint_\cv \rho \diff \vol  & + & \iint_\cs \rho \ (\vec V_\rel \cdot \vec n) \diff A \nonumber\\\label{eq_rtt_mass}\\
			\begin{array}{c} {\scriptstyle \text{the rate of change}} \\ {\scriptstyle \text{of the fluid’s mass}} \\ {\scriptstyle \text{as it transits}} \end{array} & = & 0 & = & \begin{array}{c} {\scriptstyle \text{the rate of change}} \\ {\scriptstyle \text{of mass inside}} \\ {\scriptstyle \text{considered volume}} \end{array}									& + & \begin{array}{c} {\scriptstyle \text{the net mass flow}} \\ {\scriptstyle \text{at the borders}} \\ {\scriptstyle \text{of the considered volume}} \end{array} \nonumber
		\end{IEEEeqnarray}
	\end{mdframed}

	\commonstopvideo{Niccolò Paganini - Caprice No.5 - David Hernando.ogv}{https://frama.link/vyH-cxCL}{with sufficient skills (and lots of practice!), it is possible for a musician to produce an uninterrupted stream of air into an instrument while still continuing to breathe, a technique called circular breathing.\wupen{Circular breathing} Can you identify the different terms of eq.~\ref{eq_rtt_mass_simple_two} as they apply to the saxophonist’s mouth?}{David Hernando Vitores (\ccbysa)}
	This equation~\ref{eq_rtt_mass} is often called \vocab{continuity equation}. It allows us to compare the incoming and outgoing mass flows through the borders of the control volume.
	
	Sometimes, the control volume has well-defined inlets and outlets through which the volume flow $\rho (\vec V_\rel \cdot \vec n)$ is uniform: this is illustrated in figure~\ref{fig_cv_continuity_simple}. In that case equation~\ref{eq_rtt_mass} reduces to forms that we have already identified in the previous chapter (see \S\ref{ch_balance_mass_int_simple} p.~\pageref{ch_balance_mass_int_simple}):
		\begin{IEEEeqnarray}{rCl}
			0 	& = & \timederivative{} \iiint_\cv \rho \diff \vol  + \sum_\out \left\{ \rho V_\perp A\right\} + \sum_\inn \left\{ \rho V_\perp A\right\}\label{eq_rtt_mass_simple}\\
				& = & \timederivative{} \iiint_\cv \rho \diff \vol  + \sum_\out \left\{ \rho |V_\perp| A\right\} - \sum_\inn \left\{ \rho |V_\perp| A\right\} \nonumber\\
			0	& = & \timederivative{} m_\cv  + \sum_\out \left\{ |\dot m| \right\} - \sum_\inn \left\{ |\dot m| \right\}\label{eq_rtt_mass_simple_two}
		\end{IEEEeqnarray}
	\begin{figure}[ht]
		\begin{center}
			\includegraphics[width=0.8\textwidth]{simple_cv_fnet.png}
		\end{center}
		\supercaption{A control volume for which the system’s properties are uniform at each inlet and outlet. Here eq.~\ref{eq_rtt_mass} translates as $0 = \timederivative{} \iiint_\cv \rho \diff \vol  + \rho_3 |V_{\perp 3}| A_3 + \rho_2 |V_{\perp 2}| A_2 - \rho_1 |V_{\perp 1}| A_1$.}{\wcfile{Integral analysis angular momentum sketch.svg}{Figure} \cczero \oc}
		\label{fig_cv_continuity_simple}
		\vspace{-1cm}%handmade
	\end{figure}
		
	As before, in equation~\ref{eq_rtt_mass_simple}, the term $\rho V_\perp A$ at each inlet or outlet corresponds to the local mass flow~$\pm \dot m$ (negative inwards, positive outwards) through the boundary.
	
	\protip{portrait20}{0.21}{0.21}{7}{
		If you are thinking that equation~\ref{eq_rtt_mass} is just a very fancy way to write the “good” mass balance equation we already wrote in \chaptertwoshort (eq.~\ref{eq_mass_oned} p.~\pageref{eq_mass_oned}), you are not completely wrong. There are two useful improvements, however:\\
		First, having $\iint \rho V_\perp \diff A$ instead of just $\rho V_\perp A$ allows us to handle cases where the incoming/outgoing velocities are not uniform. Outside of fluid mechanics textbooks, very few flows have a nice smooth uniform outlet!\\
		Second, there is an unsteady term $\tdiff m_\cv / \diff t$. It is not used to solve problems in this course, but one day when you are confronted to a case where your inlet and outlet mass flows are not equal, it will save your day!
	}


\dontbreakpage%handmade
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of momentum}

	\agthumb{338}{Newton’s laws of motion almost didn’t happen}{0.7}{}
	What force is applied to the fluid for it to travel through the control volume? We answer this question by writing out a mass balance equation in the template provided by the Reynolds transport theorem (eq.~\ref{eq_rtt}).
	
	We now state that the placeholder variable $B$ is momentum $m \vec V$. It follows that $\inlinetimederivative{B}$ becomes $\inlinetimederivative{m \vec V_\sys)}$, which by definition is the net force $\vec F_\net$ applying to the system (see eq.~\ref{eq_secondlaw} p.~\pageref{eq_secondlaw}). Also, $b \equiv B/m = m \vec V/m = \vec V$ and now the Reynolds transport theorem becomes:\dontbreakpage

	\begin{mdframed}
		\begin{IEEEeqnarray}{CCCCCCC}
			\timederivative{(m \vec V_\text{sys})} & = & \vec F_\net & = & \timederivative{} \iiint_\cv \rho \vec V \diff \vol  & + & \iint_\cs \rho \vec V \ (\vec V_\rel \cdot \vec n) \diff A \nonumber\\\label{eq_rtt_linearmom}\\
			& & \begin{array}{c}
					{\scriptstyle \text{the vector sum}} \\
					{\scriptstyle \text{of forces}} \\
					{\scriptstyle \text{on the fluid}}
				\end{array}	& = & 
				\begin{array}{c}
					{\scriptstyle \text{the rate of change}} \\
					{\scriptstyle \text{of momentum}} \\
					{\scriptstyle \text{within the considered volume}}
				\end{array}	& + &
				\begin{array}{c}
					{\scriptstyle \text{the net flow of momentum}} \\
					{\scriptstyle \text{through the boundaries}} \\
					{\scriptstyle \text{of the considered volume}}
				\end{array} \nonumber
		\end{IEEEeqnarray}
	\end{mdframed}

	\youtubethumb{iDCpqoJJSI4}{Making sense of the 3D linear momentum balance equation}{\oc (\ccby)}{}	
	Sometimes, the control volume has well-defined inlets and outlets through which the flow $\rho \vec V (\vec V_\rel \cdot \vec n)$ is uniform: this is illustrated again in figure~\ref{fig_cv_linearmom_simple}. In that case equation~\ref{eq_rtt_linearmom}  reduces to forms that we have already identified in the previous chapter (see \S\ref{ch_balance_momentum_simple} p.~\pageref{ch_balance_momentum_simple}):
		\begin{IEEEeqnarray}{rCl}
			\vec F_\net & = & \timederivative{} \iiint_\cv \rho \vec V \diff \vol  + \sum_\out \left\{ (\rho |V_\perp| A) \vec V\right\} - \sum_\inn \left\{ (\rho |V_\perp| A) \vec V\right\} \label{eq_rtt_linearmom_simple}\\
				& = & \timederivative{} \left(m \vec V \right)_\cv  + \sum_\out \left\{ |\dot m| \vec V\right\} - \sum_\inn \left\{ |\dot m| \vec V\right\}\label{eq_rtt_linearmom_simple_two}
		\end{IEEEeqnarray}
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.8\textwidth]{simple_cv_fnet.png}
			\end{center}
			\supercaption{The same control volume as in \cref{fig_cv_continuity_simple}. Here, since the system’s properties are uniform at each inlet and outlet, eq.~\ref{eq_rtt_linearmom} translates as $\vec F_\net = \timederivative{} \iiint_\cv \rho \vec V \diff \vol  + \rho_3 |V_{\perp 3}| A_3 \vec V_3 + \rho_2 |V_{\perp 2}| A_2 \vec V_2 - \rho_1 |V_{\perp 1}| A_1 \vec V_1$.}{\wcfile{Integral analysis angular momentum sketch.svg}{Figure} \cczero \oc}
			\label{fig_cv_linearmom_simple}
		\end{figure}

	\youtubethumb{S6JKwzK37_8}{as a person walks, the deflection of the air passing around their body can be used to sustain the flight of a paper airplane (a \textit{walkalong glider}\wupen{Walkalong glider}). Can you figure out the momentum flow entering and leaving a control volume surrounding the glider, and the resulting net force?}{Y:sciencetoymaker (\styl)}{}
	To make clear a few things, let us focus on the simple case where a considered volume has only one inlet (point~1) and one outlet (point~2). From equation~\ref{eq_rtt_linearmom}, the net force $\vec F_\net$ applying on the fluid is:
	\begin{IEEEeqnarray}{rCl}
		\vec F_\net & = & \timederivative{}\iiint_\cv \rho \vec V \diff \vol + \iint \rho_2 |V_{\perp 2}| \vec V_2 \diff A_2 - \iint \rho_1 |V_{\perp 1}| \vec V_1 \diff A_1 \nonumber\\\label{eq_fnet_twovectors_unsteady}
	\end{IEEEeqnarray}
	
	In this equation~\ref{eq_fnet_twovectors_unsteady}, what could cause $\vec F_\net$ to be non-zero?
	\begin{itemize}
		\item The first term, which we could informally write as $\inlinetimederivative{(m \vec V)_\cv}$, could be non-zero. This happens when the momentum inside the control volume changes. This may occur if the distribution of velocities $\vec V$ within the control volume is changing, such as when the fluid in a tank sloshes back and forth against the walls.
		\item The sum of the last two terms, which we could informally write as $|\dot m|_2 \vec V_2 - |\dot m|_1 \vec V_1$, could also be non-zero. This happens when the flux of momentum entering the control volume is different from the one leaving it:
			\begin{itemize}
				\item It may be because the mass flow $\dot m$ is different at inlet and outlet, even if the two velocity distributions are the same;
				\item It may be because the velocities have different length, and the flow is speeding up or slowing down;
				\item It may be because the velocities are aligned differently, and the flow is changing directions;
				\item It may be because the velocities are non-uniform and distributed differently, even if their average (and thus the mass flows) are the same.
			\end{itemize}
	\end{itemize}

	As you can see, a lot of different things may be happening at once! We will study (separately) the most relevant of those effects in the problem section of this chapter.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
\section{Balance of angular momentum}

	\youtubethumb{4cvGGxTsQx0}{rocket landing gone wrong. Can you compute the moment exerted by the top thruster around the base of the rocket as it (unsuccessfully) attempts to compensate for the collapsed landing leg?}{Y:SciNews (\styl)}{}
	What moment (“twisting effort”) is applied to the fluid for it to travel through the control volume? We answer this question by writing an angular momentum balance (see eq.~\ref{eq_secondlawmom} p.~\pageref{eq_secondlawmom}) in the template provided by the Reynolds transport theorem (eq.~\ref{eq_rtt}).
	
	We position ourselves at a point $X$, about which we measure all moments. All positions are measured with a position vector $\vec r_{\X m}$. We now state that the placeholder variable $B$ is angular momentum $\vec r_{\X m} \wedge m \vec V$. It follows that $\inlinetimederivative{B}$ becomes $\inlinetimederivative{\vec r_{\X m} \wedge m \vec V_\sys}$, which by definition is the net moment $\vec M_\net$ applying to the system (see again eq.~\ref{eq_secondlawmom} p.~\pageref{eq_secondlawmom}). Also, $b \equiv B/m = \vec r_{\X m} \wedge \vec V$ and now the Reynolds transport theorem becomes:\dontbreakpage
	\begin{adjustwidth}{-3cm}{0cm}%handmade
	\begin{mdframed}
		\begin{IEEEeqnarray}{cCcCccc}
			\timederivative{(\vec r_{\X m} \wedge m \vec V)_\sys} & = & \vec M_{\net, \X} & = & \timederivative{} \iiint_\cv \vec r_{\X m} \wedge \rho \vec V \diff \vol  & + & \iint_\cs \vec r_{\X m} \wedge \rho \ (\vec V_\rel \cdot \vec n) \vec V \diff A \nonumber\\\label{eq_rtt_angularmom}\\
			& & \begin{array}{c} 
					{\scriptstyle \text{the vector sum}} \\
					{\scriptstyle \text{of moments}} \\
					{\scriptstyle \text{on the fluid}}
				\end{array}	& = & 
				\begin{array}{c} 
					{\scriptstyle \text{the rate of change of}} \\
					{\scriptstyle \text{the angular momentum}} \\
					{\scriptstyle \text{within the considered volume}}
				\end{array}	& + & 
				\begin{array}{c} 
					{\scriptstyle \text{the net flow of angular momentum}} \\ 
					{\scriptstyle \text{through the boundaries}} \\
					{\scriptstyle \text{of the considered volume}}
				\end{array} \nonumber
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item in which $\vec r_{\X m}$ is a vector giving the position of any mass $m$ relative to point $X$.
		\end{equationterms}
	\end{mdframed}
	\end{adjustwidth}

	\youtubethumb{VR8LGr6PuRY}{Making sense of the angular momentum balance equation}{\oc (\ccby)}{}
	Sometimes, the control volume has well-defined inlets and outlets through which the flow $\vec r_{\X m} \wedge \rho \vec V (\vec V_\rel \cdot \vec n)$ is uniform: this is illustrated in figure~\ref{fig_cv_angularmomentum_simple}. In that case equation~\ref{eq_rtt_angularmom}  reduces to a more readable form:
		\begin{IEEEeqnarray}{rCCCCCl}
			\vec M_{\net, \X} & = & \timederivative{} \iiint_\cv \vec r_{\X m} \wedge \rho \vec V \diff \vol  &+& \sum_\out \left\{ \vec r_{\X m} \wedge |\dot m| \vec V\right\} - \sum_\inn \left\{ \vec r_{\X m} \wedge |\dot m| \vec V\right\} \nonumber\\\label{eq_rtt_angularmom_simple}
		\end{IEEEeqnarray}
		
			\begin{figure}[ht]
				\begin{center}
					\includegraphics[width=0.6\textwidth]{simple_cv_mnet.png}
				\end{center}
				\supercaption{A control volume for which the properties of the system are uniform at each inlet or outlet. Here the moment about point X in the bottom right is $\vec M_{\net, \X} = \timederivative{} \iiint_\cv \vec r_{\X m} \wedge \rho \vec V \diff \vol  + \vec r_{2} \wedge |\dot m_2| \vec V_2 - \vec r_{1} \wedge |\dot m_1| \vec V_1$.}{\wcfile{Integral analysis angular momentum sketch.svg}{Figure} \cczero \oc}
				\label{fig_cv_angularmomentum_simple}
			\end{figure}

	The same remarks we made for linear momentum earlier apply here: there are many possible phenomena which may equate to a moment on the fluid. We will explore this equation rather shyly in the problem section of the chapter.
	
	\protip{portrait37}{0.25}{0.25}{8}{
		The angular momentum balance equation is useful in cases where we attempt to balance a machine using fluid flows. Moments can be added and subtracted as vectors, just like forces. It’s an extra layer of abstraction to learn (just about every fluid dynamicist has suffered at first with the clockwise-is-positive convention). You’ll be glad you learned it when you attempt to prevent the helicopter from spinning uncontrollably — or when you ask your \cfd software to calculate a moment, and it spits out a vector!
	}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of energy}

	What power is applied to the fluid for it to travel through the control volume? We can answer this question by writing an energy balance in the template provided by the Reynolds transport theorem (eq.~\ref{eq_rtt}).
	
	The derivation of the equation we look for is the same as the derivation of equation~\ref{eq_sfee} p.~\pageref{eq_sfee} in the last chapter. We may therefore directly jump to the result:
	\begin{mdframed}
		\begin{IEEEeqnarray}{cCcCcCc}
			\timederivative{E_\sys} & = & \dot Q_{\net} + \dot W_\text{shaft, net} & = & \timederivative{} \iiint_\cv \rho\ e_f \diff \vol  & + & \iint_\cs \rho\ e_f \ (\vec V_\rel \cdot \vec n) \diff A \nonumber\\\label{eq_rtt_energy}
		\end{IEEEeqnarray}
	with the term $e_f$ carrying all of the energy terms relevant for us in fluid mechanics:
		\begin{IEEEeqnarray}{rCl}
			e_f &\equiv& i + \frac{p}{\rho} + \frac{1}{2} V^2 + gz
		\end{IEEEeqnarray}
	\end{mdframed}

	It can be seen here that this equation only differs from equation~\ref{eq_sfee} in the last chapter in the two following ways:
	\begin{itemize}
		\item There is an unsteady term (first term on the right-hand side), accounting for the accumulation or depletion of energy within the control volume;
		\item The energy flows through the inlet and outlet are expressed as integrals, allowing us to account for non-uniform distributions.
	\end{itemize}
	
	Save for those two differences, the equation is not any different — and, it must be admitted, not much more useful in practice. In this course, we will not be using this equation to solve problems.
	
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limits of integral analysis}
\label{ch_limits_integral_analysis}

		Integral analysis is an incredibly useful tool in fluid dynamics: in any given problem, it allows us to rapidly describe and calculate the main fluid phenomena at hand. The net force exerted on the fluid as it is deflected downwards by a helicopter, for example, can be calculated using just a loosely-drawn control volume and a single vector equation.
		
		As we progress through the end-of-chapter problems, however, the limits of this method slowly become apparent. There are two of them:
		\begin{itemize}
			\item First, we are confined to calculating the \emph{net} effect of fluid flow. The net force, for example, encompasses the integral effect of all forces —due to pressure, shear, and gravity— applied on the fluid as it transits through the control volume. Integral analysis gives us absolutely no way of distinguishing between those sub-components. In order to do that (for example, to calculate which part of a pump’s mechanical power is lost to internal viscous effects), we would need to look within the control volume.
			\item Second, all four of our equations in this chapter only work in one direction. The value $\diff B_\sys / \diff t$ of any finite integral cannot be used to find which function $\rho b V_\perp \diff A$ was integrated over the control surface to obtain it. For example, there are an \emph{infinite} number of velocity profiles which will result in a net force of \SI{+12}{\newton}. Knowing the net value of an integral, we cannot deduce the conditions which lead to it.\\
			In practice, this is a major limitation on the use of integral analysis, because it confines us to working with large swaths of experimental data gathered at the borders of our control volumes. From the wake below the helicopter, we deduce the net force; but the net force tells us nothing about the shape of the wake.
		\end{itemize}
		Clearly, in order to overcome these limitations, we are going to need to open up the control volume, and look at the details of the flow within — perhaps by dividing it into a myriad of sub-control volumes. This is what we set ourselves to in \chaptersix, with a thundering and formidable methodology we shall call \vocab{derivative analysis}. % ta-daaaa!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solved problems}

\begin{youtubesolution}

	\begin{center}
		\textbf{Flow through pipe bend}
	\end{center}

	A chemical is flowing through a pipe with \SI{90}{\degree} bend, with a mass flow of \SI{200}{\kilogram\per\second}. Its incoming velocity (uniform) is \SI{2}{\metre\per\second}, and its outgoing velocity is \SI{3}{\metre\per\second}.
	
	\begin{center}
	\includegraphics[width=0.5\textwidth]{pipe_bend_2}
	\end{center}
	
	What is the net force exerting on the fluid?

	\seesolution{npEbzOx3qHY}

\end{youtubesolution}

\clearpage
\begin{youtubesolution}

	\begin{center}
		\textbf{Outlet with a non-uniform velocity}
	\end{center}

	Water is flowing through a straight rectangular pipe. At inlet, its velocity is uniform, with $V_1 = \SI{20}{\metre\per\second}$. At the outlet, the velocity is the same on average; however, it is not uniformly distributed. We have $V_2 = 19 + y$ (\si{\metre\per\second}), with $y$ the vertical coordinate (in \si{meters}). The width of the pipe in the $z$-direction is $\Delta z = \SI{1}{\metre}$.
	
	\includegraphics[width=1\textwidth]{example_non-uniform_integral_1}
	
	What is the net force exerting on the fluid?

	\seesolution{hPRvLVolHtY}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Force and power on a moving turbine blade}
	\end{center}

	In a jet engine, a row of blades is moving with great speed ($V_\text{blade} = \SI{442,4}{\metre\per\second}$). Each blade receives $\SI{1,7}{\kilogram\per\second}$ of high-speed, high-temperature, high-pressure gas. Through both the movement and the shape of the blade, this gas is deviated and their properties change. For simplicity, we assume the properties at inlet and outlet are uniform.
	% rough numbers for CF6: 80 blades, 650 kg/s, BPR 4.6 : so 1.7 kg/s per blade
	
	The incoming and outgoing velocities are described below two times: once (left) from the point of view of the stationary engine, and once (right) from the point of view of the moving blades.
	
	\includegraphics[width=1\textwidth]{turbine_blade_triangles}
	
	What is the net force exerting on each blade? What is the power transmitted to the blade?

	\seesolution{BgUjpaBYeDc}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Mass flows in a boundary layer}
	\end{center}

	A flat plate is positioned parallel to a steady, uniform air flow ($u_1 = U = \SI{25}{\metre\per\second}$). At the trailing edge of the plate, the velocity $u_2$ is measured. It is a function of height $y$, with thickness $\delta = \SI{2}{\centi\metre}$, following the distribution:\vspace{-0.8cm}
		\eq{
		u_2 = U \left(\frac{y}{\delta}\right)^{\frac{1}{6}}
		}
	
	The plate has length $L_1 = \SI{50}{\centi\metre}$ (in $x$-direction, along the flow) and width $L_2 = \SI{80}{\centi\metre}$ (in $z$-direction, across the flow). The density of air is \SI{1,225}{\kilogram\per\metre\cubed}.
	
	\begin{center}
	\includegraphics[width=0.85\textwidth]{boundary_layer_velocity_control_volume}
	\end{center}
	
	The layer has thickness $\delta$ at outlet. What is the \textit{inlet} height of a control volume that has the mass flow of this layer?
	\seesolution{2ROV07UcjiI}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Shear force in a boundary layer}
	\end{center}

	In the flat plate problem from above, what is the net force exerted on the air by the plate?\vspace{-0.5cm}

	\seesolution{Lu3GRcv3BSA}

\end{youtubesolution}




\atendofchapternotes

\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://git.framasoft.org/u/olivier/sensible-styles
\renewcommand{\numberofthischapter}{9}
\renewcommand{\titleofthisdocument}{\namechapternine}
\renewcommand{\keywordsofthisdocument}{}
\renewcommand{\lastedityear}{2019}
 \renewcommand{\lasteditmonth}{06}
   \renewcommand{\lasteditday}{06}

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger preamble)
\printversion
\usepackage{pdfpc-commands} % experimental: videos in slides

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

	\mainslidesbegin
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	\begin{frame}
		\begin{centering}
		
			All (interesting) flows are turbulent
		
		\end{centering}
	\end{frame}

	\begin{frame}
	Are turbulent!\pause
		\begin{itemize}
			\item blood flow in large veins \& arteries\pause
			\item fluid flow in pipes \& machines\pause
			\item fluid flow close to walls\pause
			\item air flow in your nostrils\pause
			\item river flows, ocean currents\pause
			\item wind
		\end{itemize}
	\end{frame}
	
	\pictureframe{1}{Sediment_in_the_Gulf_of_Mexico_(2)}{Sediment flow in the Gulf of Mexico}{\wcfile{Sediment_in_the_Gulf_of_Mexico_(2)}{Image} by Jeff Schmaltz / NASA Earth Observatory (NASA/Goddard/Aqua/MODIS) (\pd)}
	
	\pictureframe{1}{Argentina.TMOA2003041_lrg.jpg}{Plankton bloom off the coast of Argentina}{\attlink{https://frama.link/southatlanticphytoplankton}{Image} by Jacques Descloitres, MODIS Rapid Response Team NASA GSFC (\pd)}
	
	\pictureframe{1}{California_in_flames_ESA387826.jpg}{Wildfire smoke off California}{\wcfile{California_in_flames_ESA387826.jpg}{Image} \ccbysathreeigo by ESA (Copernicus Sentinel-2)}

	\begin{frame}
		\begin{centering}
		
			Laminar flows are not the rule\pause
			
			They are the exception
		
		\end{centering}
	\end{frame}


	\begin{frame}{Objectives:}
		\begin{itemize}\pause
			\item How “much” turbulence \emph{is} there?\pause
			\item How “much” turbulence will there be?
		\end{itemize}
	\end{frame}

	\skipinprint{
	\fullFrameMovie[autostart]{84518319.mp4}{84518319}%{Two fluids with different density (DNS simulation)}
	}

	\begin{frame}{Warning}
		Turbulence characterization is not an exact science.\pause
		
		$\to$ OK to be wrong by factor 10 or so today
	\end{frame}

	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Recognizing turbulence}

%%%
\subsection{A brief definition}

	\begin{frame}
		A flow is turbulent if:
		\begin{itemize}\pause
			\item it has many different scales,\pause
			\item it is chaotic,\pause
			\item and it is dissipative.
		\end{itemize}
	\end{frame}

%%%
\subsection{Chaos, not randomness}

	\pictureframe{1}{Kea_0334_(24833991).jpeg}{Transition to turbulence}{\wcfile{Kea_0334_(24833991).jpeg}{Photo} \ccby by \flickrcodename{49503154413@N01}{THOR}}

	\pictureframe{1}{Smoke_Series_(4130758692).jpg}{Transition to turbulence}{\wcfile{Smoke_Series_(4130758692).jpg}{Photo} \ccby by \attlink{https://500px.com/rafaespada}{Rafa Espada}}

	\begin{frame}
		\begin{IEEEeqnarray}{rCl}
			\re &\equiv& \frac{\rho V L}{\mu}
		\end{IEEEeqnarray}\pause
		
		When $\re > \num{e3}$ or so, flow likely to be turbulent
		
		$\re$ is \textit{damping factor} in N-S (ch.\ 9)
	\end{frame}

	\begin{frame}{Chaos, not randomness}
		\begin{itemize}\pause
			\item Navier-Stokes eq.\ still applies!\pause
			\item Turbulent flow is \emph{definitely} deterministic\\\pause
				$\to$ not random\pause
			\item …but the exact details depend on initial conditions\\\pause
				$\to$ chaotic
		\end{itemize}
	\end{frame}

%%%
\subsection{Growth and decay}

	\begin{frame}
		Turbulence occurs because of non-uniform shear (\& pressure)\\\pause
		 e.g. sudden turn, wall shear
	\end{frame}
	

	\begin{frame}
		Turbulence is sustained if shear is continually applied to the flow.\pause
		
		If not, it dies out!
	\end{frame}
	
	\begin{frame}{The dying-out of turbulence}
		Turbulence is \vocab{dissipative}.\pause
		
		Receives energy from large scale, dissipates it down to heat.\pause
		
		Not magic!
	\end{frame}

%%%
\subsection{A cascade of vortices}


	
	\begin{frame}
		Useful concept: \vocab{vortices}/\vocab{eddies}\pause
		
		Flow with two components:\pause
			\begin{itemize}
				\item one “main” flow\pause
				\item one tangle of vortices of all sizes
			\end{itemize}
	\end{frame}
	

	\begin{frame}
		Turbulence begins with large vortices\pause
		
		Energy cascades down to smaller vortices\pause
		
		Very small \& slow vortices dissipate energy to heat
	\end{frame}



%%%
\subsection{Not turbulence}

	\pictureframe{1}{Clouds_over_the_Atlantic_Ocean.jpg}{Not turbulence: sea waves}{\wcfile{Clouds_over_the_Atlantic_Ocean.jpg}{Photo} \ccbysa by \wcun{Tfioreze}{Tiago Fioreze}}

	\pictureframe{1}{Karmansche_Wirbelstr_kleine_Re.JPG}{Not turbulence: von Kármán vortex street}{\wcfile{Karmansche_Wirbelstr_kleine_Re.JPG}{Photo} \ccbysa by Jürgen Wagner}
	
	\skipinprint{
	\fullFrameMovie[loop&autostart]{videos/Vortex-street-animation.mp4}{videos/Vortex-street-animation}{Simulation of a von Kármán vortex street}
	}

	\begin{frame}{Not turbulence}
		\begin{itemize}\pause
			\item Sea waves\pause
			\item von Kármán vortices\pause
			\item large, well-identified patterns\pause
			\item molecular motion
		\end{itemize}
	\end{frame}	
	
	\begin{frame}{(again)}
		A flow is turbulent if:
		\begin{itemize}\pause
			\item it has many different scales,
			\item it is chaotic,
			\item and it is dissipative.
		\end{itemize}
	\end{frame}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The effects of turbulence}

%%%
\subsection{Dissipation (losses)}

	\begin{frame}
		Turbulent subtracts energy from the main flow:\pause
		
		acts as a (\emph{local}) increase in viscosity
	\end{frame}

%%%
\subsection{Main flow patterns}


	\begin{frame}
		Turbulent flow patterns are 
		\begin{itemize}\pause
			\item very different from laminar patterns:\pause
				\begin{itemize}
					\item wider shear zones\pause
					\item unsteady patterns\pause
					\item more uniform velocity profiles\pause
				\end{itemize}
			\item very similar one to another, once turbulent
		\end{itemize}
	\end{frame}


%%%
\subsection{Mixing}

	\begin{frame}
		Increase exchange of mass \& momentum:\pause
		
			bad for pressure losses,\pause
			
			great for heat transfer, mixing, dispersion.
	\end{frame}

	\skipinprint{
	\fullFrameMovie[autostart]{306660827.mp4}{306660827}%{Weather on a humid, heated surface (LES simulation)}
	}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Quantifying turbulence}


%%%
\subsection{Average and fluctuation}

	\begin{frame}
		We split between \vocab{average} and \vocab{fluctuation}:\pause
		\begin{IEEEeqnarray}{rCl}
			u_i 			&\equiv& \overline u_i + u_i'\label{eq_def_average_u}\\\pause
			\overline{u_i'} 	&\equiv& 0
		\end{IEEEeqnarray}
	\end{frame}

	\figureframe{1}{instantaneous_average}{Instantaneous and average temperature}{\wcfile{Average and instantaneous values.svg}{Figure} \cczero \oc}


%%%
\subsection{Turbulence intensity}

	\begin{frame}
		\vocab{Turbulence intensity} $I$:
		
		how large fluctuations $u'$ are relative to average flow velocity $\overline u$
	\end{frame}

	\begin{frame}
		\vocab{Turbulence intensity} $I$:\pause
		
		\begin{IEEEeqnarray}{rCl}
			I &\equiv& \frac{1}{\overline V} \left[\frac{1}{3} \left[ \overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right]\right]^\frac{1}{2} \label{eq_i}
		\end{IEEEeqnarray}\pause
		
		\SI{3}{\percent} small, \SI{80}{\percent} large
	\end{frame}




%%%
\subsection{The size of eddies}


	\begin{frame}
		How “big” is the turbulence?\pause
		
		$\to$ quantify extreme size of eddies
	\end{frame}

	\begin{frame}{Largest eddies}
		Characteristic properties:\\
		$\Lambda$ (lambda = $L_\max$), $v_\Lambda$, $t_\Lambda$\pause
		
		\begin{IEEEeqnarray}{rCcCl}
			v_\Lambda = \frac{\Lambda}{t_\Lambda}\pause
		\end{IEEEeqnarray}
		\begin{IEEEeqnarray}{rCcCl}
			\re_\Lambda &\equiv& \frac{\rho u_\Lambda \Lambda}{\mu}
		\end{IEEEeqnarray}
		
	\end{frame}

	\begin{frame}{Largest eddies}
		In homogeneous turbulent flow, $\Lambda$ is $\approx \SI{50}{\percent}$ of largest scale of the flow
		(pipe diameter, size of obstacle etc.)
	\end{frame}
	
	
	\pictureframe{1}{Eye_of_an_algal_storm_ESA346697.png}{Baltic sea vortex evidenced by plankton bloom}{\wcfile{Eye of an algal storm ESA346697.png}{Image} \ccbysathreeigo ESA (Copernicus Sentinel data 2015)}


	\begin{frame}{Smallest eddies}
		Characteristic properties:\\
		$\eta$ (eta = $L_\min$), $v_\eta$, $t_\eta$\pause
		
		\begin{IEEEeqnarray*}{rCcCl}
			v_\eta = \frac{\eta}{t_\eta}
		\end{IEEEeqnarray*}
		\begin{IEEEeqnarray*}{rCcCl}
			\re_\eta &\equiv& \frac{\rho u_\eta \eta}{\mu}
		\end{IEEEeqnarray*}
		
	\end{frame}


	\begin{frame}{Intuition by Andrey Kolmogorov, 1941}\pause
		In “simple” turbulence,\pause
		\begin{IEEEeqnarray}{rCcCl}
			\re_\eta &\equiv& \frac{\rho u_\eta \eta}{\mu} &=& 1
		\end{IEEEeqnarray}
	\end{frame}


	\begin{frame}{Dimensional analysis by Kolmogorov}
		In “simple” turbulence,\pause
		\begin{IEEEeqnarray}{rCcCl}
			\frac{L_\min}{L_\max} 	&=& \frac{\eta}{\Lambda} 	&=& \re_\Lambda^{-3/4}\label{eq_kolmogorov_size}\\
									&& \frac{u_\eta}{u_\Lambda} &=& \re_\Lambda^{-1/4}\label{eq_kolmogorov_speed}\\
									&& \frac{t_\eta}{t_\Lambda} &=& \re_\Lambda^{-1/2}\label{eq_kolmogorov_time}
		\end{IEEEeqnarray}
	\end{frame}
	
	
	
	\pictureframe{1}{The_Turbulent_Bering_Sea_(detail)_(14923408723)}{Plankton bloom in the Bering Sea)}{\wcfile{The_Turbulent_Bering_Sea_(detail)_(14923408723)}{Image} by NASA/Goddard/Aqua/MODIS (\pd)} 
		% center island is 50 km long
	
	\pictureframe{1}{PIA22256_North Atlantic_Mar1}{Relative vorticity in the North Atlantic, 2012-03-01}{\doilink{10.1038/s41467-018-02983-w} \ccby by Z.\ Su, J.\ Wang, P.\ Klein, A.\ F.\ Thompson \& D.\ Menemenlis}
	
	\pictureframe{1}{PIA22256_North Atlantic_Mar1_zoom}{Relative vorticity in the North Atlantic, 2012-03-01}{\doilink{10.1038/s41467-018-02983-w} \ccby by Z.\ Su, J.\ Wang, P.\ Klein, A.\ F.\ Thompson \& D.\ Menemenlis}
	

%%%
\subsection{Kinetic energy and dissipation rate}

	\begin{frame}
		\vocab{Turbulent kinetic energy} $k$\pause
		\begin{IEEEeqnarray}{rCl}
			k &\equiv& \frac{1}{2} \left(\overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right)\label{eq_k}
		\end{IEEEeqnarray}\pause
		(in \si{\joule\per\kilogram})
	\end{frame}

	\begin{frame}
		\vocab{Turbulent dissipation rate} $\epsilon$ is the rate at which $k$ is dissipated.\\
		In stationary turbulence let to decay,\pause
		\begin{IEEEeqnarray}{rCl}
			\epsilon &=& -\partialtimederivative{k}
		\end{IEEEeqnarray}\pause
		(in $\si{\watt\per\kilogram}$ )
	\end{frame}


	\begin{frame}{Dimensional analysis by Kolmogorov}\pause
		In “simple” turbulence,\pause
		\begin{IEEEeqnarray}{rCl}
				\eta &=& \left(\frac{\mu^3}{\rho^3} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\label{eq_kolmogorov_epsilon_size}\\
				u_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\label{eq_kolmogorov_epsilon_vel}\\
				t_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{2}}\label{eq_kolmogorov_epsilon_time}
		\end{IEEEeqnarray}
	\end{frame}

	\begin{frame}{How are $k$ and $\epsilon$ distributed?}\pause
		Observe distribution of densities:\pause
		
		\begin{IEEEeqnarray}{rCl}
			k &=& \int_\eta^\Lambda E \diff l \\
			\epsilon &=& \int_\eta^\Lambda D \diff l
		\end{IEEEeqnarray}
	
	\end{frame}


	\figureframe{1}{spectra}{Distribution of energy and dissipation}{\wcfile{Distribution turbulent kinetic energy and dissipation rate.svg}{Figure} \ccbysa by \olivier}

	\begin{frame}{How are $k$ and $\epsilon$ distributed?}\pause
		\begin{itemize}	
			\item All the energy in the large scales;\pause
			\item All the dissipation in the small scales.
		\end{itemize}
	\end{frame}


%%%
\subsection{Anisotropy and inhomogeneity}

	\begin{frame}
		Not-so-simple turbulence? Then quantify:\pause
			\begin{itemize}
				\item \vocab{Homogeneity} (uneven distribution in space)\pause
				\item \vocab{Anisotropy} (dependence on direction)\pause
				\item \vocab{Development} (time to build up)
			\end{itemize}
	\end{frame}
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Computing turbulent flow}

%%%
\subsection{Basic premise}

	\begin{frame}
		Navier-Stokes can be solved numerically…\pause
		
		…very slowly
	\end{frame}
	
	\pictureframe{1}{condamine}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	\pictureframe{1}{condamine_p1}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	\pictureframe{1}{condamine_p2}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	\pictureframe{1}{condamine_p3}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	
	\figureframe{1}{property_shrinking_volume}{}{\wcfile{Macroscopic microscopic property_2.svg}{Figure} \ccbysa \oc}
	

%%%
\subsection{Accounting for turbulence}

	
	\begin{frame}
		Researching \emph{everything} is overkill:\pause
	
		what if $u_i = \overline u_i + u’_i$?\pause
		
		$\to$ momentum conservation changes form!
	\end{frame}
	
	\begin{frame}
	
		What is the average of\small
			\begin{multline}
					\rho \left[ \partialtimederivative{(\overline u + u')} + (\overline u + u') \partialderivative{(\overline u + u')}{x} \right.\\
					\left.+ (\overline v + v') \partialderivative{(\overline u + u')}{y} + (\overline w + w') \partialderivative{(\overline u + u')}{z} \right] \\
						= \rho g_x - \partialderivative{(\overline p + p')}{x} \\
						+ \mu \left[ \secondpartialderivative{(\overline u + u')}{x} + \secondpartialderivative{(\overline u + u')}{y} + \secondpartialderivative{(\overline u + u')}{z} \right]
			\end{multline}
	\end{frame}
	
	\begin{frame}
		Answer:
		
		\begin{IEEEeqnarray}{lr}
				\rho \left[ \partialtimederivative{\overline u} + \overline u \partialderivative{\overline u}{x} + \overline v \partialderivative{\overline u}{y} + \overline w \partialderivative{\overline u}{z}\right] \nonumber\\
				\textcolor{red}{+ \rho \left[\overline{u' \partialderivative{u'}{x}} + \overline{v' \partialderivative{u'}{y}} + \overline{w' \partialderivative{u'}{z}} \right]} &\nonumber\\
					~~~~~~ = \rho g_x - \partialderivative{\overline p}{x} + \mu \left[ \secondpartialderivative{\overline u}{x} + \secondpartialderivative{\overline u}{y} + \secondpartialderivative{\overline u}{z} \right] &\nonumber\\\nonumber\\\label{eq_rans}
		\end{IEEEeqnarray}
		
	\end{frame}

	\begin{frame}
		\textit{Catastrophe !}\pause

		\begin{IEEEeqnarray*}{rCl}
			 \overline{u' \partialderivative{u'}{x}} + \overline{v' \partialderivative{u'}{y}} + \overline{w' \partialderivative{u'}{z}} &=&\pause  \partialderivative{\overline{u_i' u_j'}}{j}\\\\\pause
	&\neq& 0
		\end{IEEEeqnarray*}\pause
		
		Correlated fluctuations don’t cancel out…
	\end{frame}
	
	\figureframe{1}{instantaneous_average}{Instantaneous and average temperature}{\wcfile{Average and instantaneous values.svg}{Figure} \cczero \oc}
	
		\begin{frame}
		Average Navier-Stokes $\neq$ Navier-stokes:
		
		\begin{IEEEeqnarray}{lr}
				\rho \left[ \partialtimederivative{\overline u} + \overline u \partialderivative{\overline u}{x} + \overline v \partialderivative{\overline u}{y} + \overline w \partialderivative{\overline u}{z}\right] \nonumber\\
				\textcolor{red}{+ \rho \left[\overline{u' \partialderivative{u'}{x}} + \overline{v' \partialderivative{u'}{y}} + \overline{w' \partialderivative{u'}{z}} \right]} &\nonumber\\
					~~~~~~ = \rho g_x - \partialderivative{\overline p}{x} + \mu \left[ \secondpartialderivative{\overline u}{x} + \secondpartialderivative{\overline u}{y} + \secondpartialderivative{\overline u}{z} \right] &\nonumber\\\nonumber\\\label{eq_rans}
		\end{IEEEeqnarray}
		
	\end{frame}
	
	\begin{frame}
		the time average of a real flow\pause
		
		cannot be obtained by solving for the time-average velocities.
	\end{frame}
	
	
	\pictureframe{1}{condamine_p1}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	\pictureframe{1}{condamine_p2}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}
	\pictureframe{1}{condamine_p4}{How many pixels are enough?}{\wcfile{FR-04-La_Condamine-Châtelard3.JPG}{Photo} \ccbysa by Szeder László (modified)}

	\begin{frame}{bam}
		\begin{center}
			\emph{the average of the solution}\pause
		
			\emph{is not the solution of the average flow}.\pause
			
			$\to$ we need a \vocab{turbulence model}:\pause
			
			a number for $\partialderivative{\overline{u_i' u_j'}}{j}$.
		\end{center}
		
	\end{frame}
	
	\begin{frame}{Conclusion:}
		\begin{center}
			CFD is WRONG\pause
			
			we solve the equations wrong,\pause
			
			and they are the wrong equations
		\end{center}
	\end{frame}


\mainslidesend

\end{document}

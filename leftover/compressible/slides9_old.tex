\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://git.framasoft.org/u/olivier/sensible-styles
\renewcommand{\documentnumber}{9}
\renewcommand{\titleofthisdocument}{Compressible flow}
\renewcommand{\keywordsofthisdocument}{}
\renewcommand{\lastedityear}{2018}
 \renewcommand{\lasteditmonth}{06}
   \renewcommand{\lasteditday}{21}

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger peamble)
\printversion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

	\mainslidesbegin
	
	\begin{frame}{Take-home slide for lecture 9}
		\begin{itemize} \pause
			\item Density changes = momentum storage \pause
			\item Isentropic expansion to accelerate:\\ \pause
					$\ma < 1$: converge\\ \pause
					$\ma > 1$: diverge \pause
			\item Shock waves: pressure increase at constant energy
		\end{itemize}
	\end{frame}



\section{Motivation}

	\begin{frame}
			Compressible flow: density $\rho$ no longer constant
	\end{frame}
	
	\begin{frame}{Objectives}
		\begin{itemize}\pause
			\item Understand the main mechanisms;\pause
			\item Quantify them for the simplest cases.
		\end{itemize}
	\end{frame}
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Compressibility and its consequences}

	\subsection{Problem description}

	\oldpictureframe{}{BmYpSJuCAAEHAax}{0.9}{\copyright unknown photographer}
	
	\begin{frame}{Ouille ouille ouille}\pause
	
		\begin{IEEEeqnarray*}{rCl}
			\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V} +\frac{1}{3}\mu \gradient{\left(\divergent{\vec V}\right)}\\\pause
			0 & = & \partialtimederivative{\rho}  +	\divergent{(\rho \vec V)}
		\end{IEEEeqnarray*}\pause
		
		$\rho$ influences the velocity field!
	\end{frame}
			
	\begin{frame}
		\begin{center}
			Compressible flow is \pause
			
			\textit{squishy fluid mechanics}
		\end{center}
	\end{frame}
	
	\begin{frame}
		\begin{center}
			Compressible flow is \pause
			
			\textit{Thermo-really-dynamics}
		\end{center}
	\end{frame}
	
	\begin{frame}
		\begin{center}
			Compressible flow is \pause
			
			Fluid Mech with \textit{significant} power
		\end{center}
	\end{frame}
	
	\begin{comment}
	\begin{frame}
		Motivation for chapter 9: \pause
		\begin{itemize}
			\item What happens when $\rho$ varies? \pause
			\item What is a shock wave, and how can we “quantify” it?
		\end{itemize}
	\end{frame}
	\end{comment}


	 \begin{frame}
		\begin{center}
			Warning! \pause
			
			Compressible flow involves algebra
		\end{center}
	\end{frame}

	 \begin{frame}
		\begin{center}
			Warning! \pause
			
			Compressible flow is \emph{weird}.
		\end{center}
	\end{frame}


	\subsection{The pressure wave}

	\begin{frame}
		When density $\rho$ is not constant,
		
		pressure changes in the field propagate with \emph{finite} speed.
		
	\end{frame}

	\oldfigureframe{}{pressure_wave_0}{1}{\wcfile{Pressure wave 2.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{pressure_wave}{1}{\wcfile{Pressure wave 2.svg}{Figure} \cczero \oc}

	\begin{frame}
		\includegraphics[width=5cm]{pressure_wave}
	
		In control volume, mass conservation means: \pause
			\begin{IEEEeqnarray}{rCl}
				\rho A c &=& (\rho + \diff \rho) A (c + \diff V)
			\end{IEEEeqnarray} \pause
		
		\footnotesize $\to$ the density change is caused by the velocity change from one side of the wave to the other. 
	\end{frame}

	\begin{frame}		
		
		re-arrange:
			\begin{IEEEeqnarray*}{rCl}
				\rho c &=& \rho c + \rho \diff V + c \diff \rho + \diff \rho \diff V \pause
			\end{IEEEeqnarray*}
			
		from which we drop $\diff \rho \diff V$: \pause
			\begin{IEEEeqnarray}{rCl}
				\rho \diff V &=& - c \diff \rho \label{eq_tmpsoundone} \pause
			\end{IEEEeqnarray}
		\footnotesize let’s eliminate this $\diff V$!

	\end{frame}

	\begin{frame}

		Now turn to Newton: \pause
		\small
			\begin{IEEEeqnarray}{rCl}
				F_\net				&=& \dot (V_\out - V_\inn) \nonumber\\ \pause
p A - (p + \diff p) A 	&=& \rho c A \left[(c + \diff V) - c\right]	\nonumber\\\pause
			-\diff p  				&=& \rho c \diff V	\nonumber\\\\\pause
			\frac{\diff p}{c} 		&=& -\rho \diff V	\label{eq_tmpsoundtwo}
			\end{IEEEeqnarray}

	\end{frame}

	\begin{frame}

			\small
			\begin{IEEEeqnarray*}{rCl}
				\rho \diff V = -c \diff \rho
			\end{IEEEeqnarray*}
			and
			\begin{IEEEeqnarray*}{rCl}
				\frac{\diff p}{c} = -\rho \diff V
			\end{IEEEeqnarray*}
				
			 \normalsize
			combine to obtain: \pause
			\begin{IEEEeqnarray}{rCl}
				c &=& \sqrt{\frac{\diff p }{\diff \rho}}
			\end{IEEEeqnarray}
		
	\end{frame}

	\begin{frame}
			Generalizing for any kind of wave: \pause
			\begin{IEEEeqnarray}{rCl}
				c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} \label{eq_def_speed_sound} \pause
			\end{IEEEeqnarray}
			\small “the speed of sound is the square root of the partial derivative of pressure with respect to density at constant entropy”.\\\pause
			“…which happens when the integral of the infinitesimal heat transfers divided by the temperature at which they occur along a reversible path between the two end states is equal to zero.”
	\end{frame}
	\begin{frame}
			\begin{IEEEeqnarray}{rCl}
				c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} \label{eq_def_speed_sound}
			\end{IEEEeqnarray}
			\small “the speed of sound is the square root of the change of pressure with density {\color{niceblue}when the medium is squished gently}.”
	\end{frame}
	
	\begin{frame}
		\begin{IEEEeqnarray*}{rCl}
			c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} \label{eq_def_speed_sound}
		\end{IEEEeqnarray*}
		\small “the speed of sound is the square root of {\color{niceblue}the hardness of the fluid when you nudge it gently}”.
	\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
\skipinprint{
\section{Thermodynamics of isentropic flow of perfect gases}
\setcounter{section}{2}
}

\sectionwithsubtitle{Thermodynamics of isentropic flow of perfect gases}{Harry Potter and the adiabatic and reversible compressions and expansions of the Perfect Gas}



	\begin{frame}
		Don’t panic! \pause
		
		Thermodynamics \textit{à la} Mach Number Remix
	\end{frame}

	\begin{frame}
		These were the good times:\setcounter{equation}{8}
			\begin{IEEEeqnarray*}{rCl}
				\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma -1}		\label{eq_hor_one}\\
				\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{p_1}{p_2} \right)^{\frac{\gamma -1}{\gamma}} \label{eq_hor_two}\\
				\left( \frac{p_1}{p_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma}		\label{eq_hor_three}
			\end{IEEEeqnarray*}
	\end{frame}

	\figureframe{1}{isentropic_compressions_expansions}{}{\wcfile{Isentropic evolutions of a perfect gas.svg}{Figure} \cczero \olivier}
	\figureframe{0.5}{work_cylinder}{}{\wcfile{Pistons ressort et fluide.svg}{Figure} \ccbysa \olivier}

	\begin{frame}
		A gas behaves like a spring, with three caveats:\pause
		\begin{enumerate}
			\item Heating makes the spring “harder”
		\end{enumerate}
	\end{frame}
	
	\figureframe{1}{pv_diagram_compression}{}{\wcfile{P-v_diagram_reversible_evolutions.svg}{Figure} \cczero \olivier}

	\begin{frame}
		A gas behaves like a spring, with three caveats:\pause
		\begin{enumerate}
			\shift{1}
			\item Temperature increases during compressions
		\end{enumerate}
	\end{frame}

	\begin{frame}
		A gas behaves like a spring, with three caveats:\pause
		\begin{enumerate}
			\shift{2}
			\item Brutal movements “damage” the spring
		\end{enumerate}
	\end{frame}

	\begin{frame}
		These were the good times:
			\begin{IEEEeqnarray}{rCl}
				\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma -1}		\label{eq_hor_one}\\ \pause
				\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{p_1}{p_2} \right)^{\frac{\gamma -1}{\gamma}} \label{eq_hor_two}\\ \pause
				\left( \frac{p_1}{p_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma}		\label{eq_hor_three}
			\end{IEEEeqnarray}
	\end{frame}

	\begin{frame}
			Take \ref{eq_hor_three} ($p v^\gamma = k$), then re-write $c$ as: \pause
			\begin{IEEEeqnarray*}{rCl}
				c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} \pause
				 = \sqrt{k \gamma \rho^{\gamma - 1}} = \sqrt{p \rho^{-\gamma} \gamma \rho^{\gamma - 1}} \nonumber\\ \pause
				&=& \sqrt{\gamma p \rho^{-1}}
			\end{IEEEeqnarray*}
	\end{frame}

	\begin{frame}
			So, in a perfect gas, the speed of sound is: \pause
			\begin{IEEEeqnarray}{rCl}
				c &=& \sqrt{\gamma R T}\\\nonumber\\ \pause
				\ma &=& \frac{V}{\sqrt{\gamma R T}} \label{eq_speed_sound_pg}
			\end{IEEEeqnarray}
	\end{frame}

	\begin{frame}
		New concept: \vocab{stagnation properties}. \pause
			\begin{IEEEeqnarray}{rCl}
				h_0 	&\equiv&	h + \frac{1}{2} V^2 \\ \pause
				T_0 	&\equiv& T + \frac{1}{c_p} \frac{1}{2} V^2 \label{eq_stag_temp}\\ \pause
				h_0 	&=& 		c_p T_0  \pause
			\end{IEEEeqnarray}
		\small the total amount of specific energy possessed by a fluid – the properties that a fluid \emph{would get} if brought to rest isentropically
	\end{frame}

	\begin{frame}
		
		Plug the Mach number into eq. (\ref{eq_stag_temp}):\pause
		\begin{IEEEeqnarray*}{rCl}
			\frac{T_0}{T} 	&=& 1 + \frac{1}{c_p T} \frac{1}{2} V^2 =  1 + \frac{1}{\frac{\gamma R}{\gamma - 1} T} \frac{1}{2} V^2\\ \pause
								&=& 1 + \left(\frac{\gamma - 1}{2}\right) \frac{V^2}{c^2} = 1 + \left(\frac{\gamma - 1}{2}\right) \ma^2\nonumber\\
		\end{IEEEeqnarray*}
	\end{frame}

	\begin{frame}

		The same can be done for pressure and density: \pause
			\begin{IEEEeqnarray}{rCl}
				\left( \frac{T_0}{T} \right)	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma^2		\label{eq_hor_m_one}\\ \pause
				\left( \frac{p_0}{p} \right)	& = & \left[1 + \left(\frac{\gamma - 1}{2}\right) \ma^2\right]^\frac{\gamma}{\gamma - 1} \label{eq_hor_m_two}\\ \pause
				\left( \frac{\rho_0}{\rho} \right)	& = & \left[1 + \left(\frac{\gamma - 1}{2}\right) \ma^2\right]^\frac{1}{\gamma - 1}\label{eq_hor_m_three}
			\end{IEEEeqnarray}\pause
			
		\small from stagnation to flow regime, isentropically
	\end{frame}

	\begin{frame}

		Defining \vocab{critical conditions} as those that occur (if at all) at $\ma = 1$: 
			\begin{IEEEeqnarray}{rCl} \pause
				\left( \frac{T^{*}}{T_0} \right)	& = & \frac{2}{\gamma + 1} \label{eq_hor_crit_one}\\ \pause
				\left( \frac{p^{*}}{p_0} \right)	& = & \left[\frac{2}{\gamma + 1}\right]^\frac{\gamma}{\gamma - 1} \label{eq_hor_crit_two}\\ \pause
				\left( \frac{\rho^{*}}{\rho_0} \right)	& = & \left[\frac{2}{\gamma + 1}\right]^\frac{1}{\gamma - 1}\label{eq_hor_crit_three}
			\end{IEEEeqnarray}
	\end{frame}

	\begin{frame}
	
		This is it!\\
		Back to squishy fluid mechanics…
	\end{frame}

\skipinprint{

	\pictureframe{1}{C8RVgNOXcAATFtp}{}{}
	\pictureframe{0.9}{C8K20vBVYAAJxya}{}{}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\sectionwithsubtitle{Speed and cross-sectional area}{everything you know is wrong}


	\oldpictureframe{}{Heat1xp_test_850}{1}{\wcfile{Heat1xp test 850.jpg}{Photo} \cczero Copenhagen Suborbitals}
	
	\begin{frame}
	
		Problem: \pause
		we want to generate maximum velocity with compressed air, \\ \pause
		exchanging $\rho$ for $V$ at constant energy. \pause
		
		But, $\rho A V$ must stay constant! \pause
		
		What nozzle geometry $A_{(p, T)}$ will enable acceleration?
	\end{frame}

	\begin{frame}
	
		Starting from continuity: \pause
		\begin{IEEEeqnarray*}{rCl}
				\dot m = \rho A V 			&=& \text{cst.}\\ \pause
				\ln \rho + \ln A + \ln V 	&=& \text{cst.} \pause
		\end{IEEEeqnarray*}
		differentiating:
		\begin{IEEEeqnarray}{rCl}
				\frac{\diff \rho}{\rho} + \frac{\diff A}{A} + \frac{\diff V}{V} &=& 0	\label{eq_mass_derivatives}
		\end{IEEEeqnarray} \pause
	
		\footnotesize Let us set this equation aside for a moment.
	
	\end{frame}

	\begin{frame}
		In an isentropic expansion, total energy of the fluid \emph{remains constant}\\
		thus $h_0 = \cst$ \pause
		\begin{IEEEeqnarray*}{rCl}
				h + \frac{V^2}{2} &=& \text{cst.} \pause
		\end{IEEEeqnarray*}
		which we differentiate to obtain:
		\begin{IEEEeqnarray}{rCl}
				\diff h + V \diff V 		&=& 0\\\nonumber \pause
				\frac{1}{\rho} \diff p + V \diff V 	&=& 0\\ \pause
				\frac{1}{\rho} &=& -\frac{1}{\diff p} V \diff V	\label{eq_tmp_rho}
		\end{IEEEeqnarray}
	\end{frame}

	\begin{comment}
	\begin{frame}
	
		Side note: we remember from \ref{eq_def_speed_sound} that the speed of sound $c$ is expressed~as: \pause
		\begin{IEEEeqnarray*}{rCl}
				c^2 = \left(\partialderivative{p}{\rho}\right)_s	\label{eq_a_derivative}
		\end{IEEEeqnarray*}
	\end{frame}
	\end{comment}

	\begin{frame}

		OK, back to the $\rho V A$ thing! \pause
		\begin{IEEEeqnarray*}{rCl}
					\frac{\diff \rho}{\rho} + \frac{\diff A}{A} + \frac{\diff V}{V} &=& 0\\ \pause
			- \derivative{\rho}{p} V \diff V + \frac{\diff A}{A} + \frac{\diff V}{V} &=& 0\\ \pause
				\frac{\diff V}{V} \left[1 - \derivative{\rho}{p} V^2\right] + \frac{\diff A}{A} &=& 0\\ \pause
				\frac{\diff V}{V} \left[1 - \frac{1}{c^2} V^2\right] + \frac{\diff A}{A} &=& 0\\ \pause
				\frac{\diff V}{V} \left[1 - \ma^2\right] + \frac{\diff A}{A} &=& 0
		\end{IEEEeqnarray*}
		\footnotesize cool!
	
	
	\end{frame}

	\begin{frame}
		Area change vs. speed change in an isentropic flow: \pause
		\begin{IEEEeqnarray}{rCl}
			\frac{\diff V}{V} &=& \frac{\diff A}{A} \frac{1}{\ma^2 - 1} \label{eq_v_a_m}
		\end{IEEEeqnarray} \pause
		
		\begin{center}
			\footnotesize TOTALLY AMAZING \pause
		
			WOW\pause

			VERY SCIENCE
		\end{center}
	
	\end{frame}

	\begin{frame}
		%\small		
		\begin{IEEEeqnarray*}{rCl}
			\frac{\diff V}{V} &=& \frac{\diff A}{A} \frac{1}{\ma^2 - 1} \pause
		\end{IEEEeqnarray*}
		
		\normalsize
		When $\ma<1$,\\ \pause
		$1/(\ma^2 - 1) < 0$: $\diff A$ and $\diff V$ are always of opposed sign. \pause
		
		Increase in area = decrease in speed,\\ \pause
		decrease in area = increase in speed.
	\end{frame}

	\begin{frame}
		%\small		
		\begin{IEEEeqnarray*}{rCl}
			\frac{\diff V}{V} &=& \frac{\diff A}{A} \frac{1}{\ma^2 - 1}  \pause
		\end{IEEEeqnarray*}
		
		\normalsize
		When $\ma>1$, \\ \pause
		$\diff A$ and $\diff V$ have the same sign. \pause
		
		Increase in area = \emph{increase} in speed,\\ \pause
		decrease in area = decrease in speed.
	\end{frame}

	\begin{frame}{Ooooooooh}
		From $\ma=1$ onwards, things are backwards! We need two nozzles!
	\end{frame}

	\oldfigureframe{}{subsonic_supersonic}{1}{\wcfile{Subsonic vs. supersonic flow behaviors.svg}{Figure} \cczero \oc}

	\begin{frame}
		If we want to let a static, compressed fluid accelerate as fast as possible, \pause
		
		we need a converging nozzle up to $\ma=1$,  \pause
		
		and a diverging nozzle onwards.
	\end{frame}

	\pictureframe{1}{Goddard_Rocket_With_Turbopumps_-_GPN-2002-000140}{I wonder what happens if I turn this valve?}{\wcfile{Goddard Rocket With Turbopumps - GPN-2002-000140.jpg}{Photo} by National Geographic Society \textsc{nasa} (1940, \pd)}

	\oldfigureframe{}{Full_flow_staged_rocket_cycle}{1}{\wcfile{Full flow staged rocket cycle.svg}{Figure} \ccbysa \wcu{Duk}}
	\oldpictureframe{}{im_06207}{1}{Photo \ccbysa \olivier}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\sectionwithsubtitle{Isentropic flow in converging and diverging nozzles}{the faster-faster bit}

	\begin{frame}
		\begin{center}
			Let’s make an experiment.
		\end{center}
	\end{frame}

	\oldfigureframe{}{converging_diverging_nozzle_0}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_1}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_2}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_3}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_4}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_5}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_6}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_7}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_8}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_9}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
	\oldfigureframe{}{converging_diverging_nozzle_10}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}

	\oldfigureframe{}{isentropic_expansion}{0.9}{\wcfile{Properties isentropic supersonic expansion.svg}{Figure} \cczero \oc}
	
	\oldfigureframe{}{cengel_isentropic_expansion_table}{1}{Figure \copyright\xspace Çengel \& Cimbala 2010 \cite{cengelcimbala2010}}
	
	\begin{frame}
	\begin{IEEEeqnarray*}{rCl}
			\frac{\diff V}{V} &=& \frac{\diff A}{A} \frac{1}{\ma^2 - 1}
		\end{IEEEeqnarray*}
		
		The “tipping point” happens at Mach one.  \pause
		
		If the pressure ratio driving the flow is large enough to reach $\ma=1$,\\ \pause
		this can \emph{only occur at a minimum area}.
	\end{frame}

	\begin{frame}
		Corollary \#1 \pause
	
		No matter what the pressure ratio is, a \emph{converging} nozzle will only allow an expanding fluid to accelerate to Mach one!\pause
		
		When $p_\out \leq p^{*}$, max mass flow: the flow is \vocab{choked}. 
	\end{frame}

	\begin{frame}
		Corollary \#2 \pause
	
		Flow choked?
		
		To accelerate further, you \emph{need} a diverging section\pause
		
		~
		
		(if the pressure ratio is large enough!)
	\end{frame}

	\begin{frame}
		However, even when $\ma_\out > 1$, \pause
		$p_\T = p^{*} = \cst$ \pause
		
		downstream conditions are not propagated upstream anymore.  \pause
		
		The mass flow through the nozzle remains constant!
	\end{frame}


	\oldfigureframe{}{converging_diverging_nozzle_10}{1}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}

	\begin{frame}
		Corollary \# 3 \pause

		\emph{There exists a maximum mass flow for any duct}, which depends only on the stagnation properties of the fluid. 		
	\end{frame}

	\begin{comment}
	\begin{frame}

		Mass flow at any point: \pause
		\begin{IEEEeqnarray*}{rCl}
			\dot m &=& \rho a A \ma
		\end{IEEEeqnarray*} \pause
		or for a perfect gas,
		\begin{IEEEeqnarray}{rCl}
			\dot m &=& \frac{p}{R T} \sqrt{\gamma R T} A \ma \nonumber\\ \pause
			\dot m &=& A \ma p_0 \sqrt{\frac{\gamma}{R T_0}} \left[1 + \frac{(\gamma - 1) \ma^2}{2}\right]^\frac{-\gamma - 1}{2 (\gamma - 1)}
		\end{IEEEeqnarray} \pause
		will now reach a maximum when the flow is choked…
	\end{frame}
	
	\begin{frame}
		Maximum mass flow whenever $\ma=1$ at the throat, \pause
		\begin{IEEEeqnarray}{rCl}
			\dot m_\text{max} &=& \left[\frac{2}{\gamma + 1}\right]^\frac{\gamma + 1}{2 (\gamma - 1)} {A^{*}} p_0 \sqrt{\frac{\gamma}{R T_0}}\nonumber\\
		\end{IEEEeqnarray} \pause
		\small At throat, properties will be critical: $p^{*}$, $T^{*}$, $A^{*}$ etc.
	\end{frame}
	\end{comment}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\sectionwithsubtitle{The perpendicular shock wave}{didn’t hear that one coming}

	\begin{frame}
		\begin{center}
			BAM \pause
		
			~
	
			Not everything in life is reversible!
		\end{center}
	\end{frame}

	\begin{frame}
		At supersonic speeds, nobody hears you coming… \pause
		
		~
		
		…but you can’t hear anything coming, either.
	\end{frame}
	
	\begin{frame}
	
		If a direction change or a pressure increase is required to flow past an obstacle,  \pause
		
		~
		
		fluid particles will be subjected to very sudden changes: \vocab{shock waves}.
	
	\end{frame}
	
	\oldpictureframe{}{US_Navy_051009-N-7559C-001}{1}{\wcfile{US Navy 051009-N-7559C-001 The Navy's Flight Demonstration team, the Blue Angels lead solo, performs the sneak pass.jpg}{Photo} by  Ryan Courtade, US Navy (\pd)}
	\oldpictureframe{}{US_Navy_110606-N-TU221-408}{1}{\wcfile{US Navy 110606-N-TU221-408 An F-A-18C Hornet assigned to Strike Fighter Squadron (VFA) 113 breaks the sound barrier during an air power demonstrati.jpg}{Photo} by Travis K. Mendoza, US Navy (\pd)}
	\oldpictureframe{}{US_Navy_070523-N-0593C-004}{1}{\wcfile{US Navy 070523-N-0593C-004 Blue Angel ^5 performs a sneak pass just below the speed of sound along the Severn River in Annapolis, Md.jpg}{Photo} by Michael Croft, US Navy (\pd)}

	\oldpictureframe{}{im_09753}{1}{\wcfile{Shock wave above airliner wing (3).jpg}{Photo} \ccbysa \olivier}
	\oldpictureframe{}{im_09722}{1}{\wcfile{Shock wave above airliner wing (7).jpg}{Photo} \ccbysa \olivier}
	\oldpictureframe{}{im_09738}{1}{\wcfile{Shock wave above airliner wing (5).jpg}{Photo} \ccbysa \olivier}
	\oldpictureframe{}{im_09747}{1}{\wcfile{Shock wave above airliner wing (4).jpg}{Photo} \ccbysa \olivier}

	\begin{frame}
		Want to be supersonic?\\
		You need gradual pressure and direction changes\\ \pause
		$\to$ weak shock waves.
	\end{frame}

	
	\oldpictureframe{}{West_German_F-104_Starfighter}{1}{\wcfile{West German F-104 Starfighter.jpg}{Photo} by Marshall, S.L.A., US Army (\pd, 1960)}
	\oldpictureframe{}{Lockheed_F-104D_Starfighter_(8590367618)_(2)}{1}{\wcfile{Lockheed F-104D Starfighter (8590367618) (2).jpg}{Photo} \ccby \flickrname{7489441@N06}{Clemens Vasters}}

	\begin{frame}
		By contrast, the most loss-inducing shock wave is the \vocab{normal shock wave}.\\ \pause
		Typically: front of a blunt body.
	\end{frame}
	
	\pictureframe{0.9}{bow_shock}{}{\wcfile{Bowshock example - blunt body.jpg}{Photo} by \textsc{nasa} (1960, \pd)}



	\begin{frame}

		A perpendicular shock wave is studied with two hypothesis: \pause
			\begin{itemize}
				\item It is a \emph{constant energy} process; \pause
				\item It it a \emph{one-dimensional} process \\ \pause
				\small($\to$ parallel streamlines)
			\end{itemize}
	\end{frame}


	\oldfigureframe{}{shock_wave}{1}{\wcfile{Shock wave control volume.svg}{Figure} \cczero \oc}


	\begin{frame}
			\includegraphics[width=5cm]{shock_wave}
			
			Starting with continuity:	 \pause
			\begin{IEEEeqnarray}{rCl}
				\rho_1 A_1 V_1 &=& \rho_2 A_2 V_2 \pause
			\end{IEEEeqnarray}
			and energy conservation:
			\begin{IEEEeqnarray}{rCl} \pause
				T_{0 1} &=& T_{0 2}
			\end{IEEEeqnarray}
	\end{frame}

	\begin{frame}

			Since
			\begin{IEEEeqnarray*}{rCl}
				 \frac{T_{01}}{T_1} 	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma_1^2\\ \pause
				 \frac{T_{02}}{T_2} 	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma_2^2
			\end{IEEEeqnarray*}
			and given that $T_{0 1} = T_{0 2}$: \pause
			\begin{IEEEeqnarray}{rCl}
				\frac{T_2}{T_1}	& = & \frac{1 + \frac{\gamma - 1}{2} \ma_1^2}{1 + \frac{\gamma - 1}{2} \ma_2^2} \label{eq_tmpi}
			\end{IEEEeqnarray} \pause
			\footnotesize Store this equation aside
	\end{frame}

	\begin{frame}

			Similarly:
			\begin{IEEEeqnarray}{rCl}
				\frac{p_2}{p_1}	& = & \frac{1 + \gamma \ma_1^2}{1 + \gamma \ma_2^2}	\label{eq_tmpii} \pause
			\end{IEEEeqnarray}
			\footnotesize Store this equation aside
			
	\end{frame}

	\begin{frame}

			Now, we compare conditions upstream and downstream:
			\begin{IEEEeqnarray}{rCl}
				\rho_1 V_1 					&=& \rho_2 V_2 \nonumber\\ \pause
				\frac{\rho_1}{\rho_2}	&=& \frac{V_2}{V_1} = \frac{\ma_2 c_2}{\ma_1 c_1}\nonumber\\ \pause
												&=& \frac{\ma_2 \sqrt{\gamma R T_2}}{\ma_1 \sqrt{\gamma R T_1}}\nonumber\\ \pause
						\frac{\ma_1}{\ma_2}	&=& \frac{p_2}{p_1} \left(\frac{T_1}{T_2}\right)^\frac{1}{2} \pause
			\end{IEEEeqnarray}
						\footnotesize Nothing special here…
	\end{frame}

	\begin{frame}
		
		
			But now, we join our three equations: \pause
			\begin{IEEEeqnarray*}{rCl}
						\frac{\ma_1}{\ma_2}	&=& \frac{p_2}{p_1} \left(\frac{T_1}{T_2}\right)^\frac{1}{2}\\
						\frac{p_2}{p_1}	& = & \frac{1 + \gamma \ma_1^2}{1 + \gamma \ma_2^2}	\\
						\frac{T_2}{T_1}	& = & \frac{1 + \frac{\gamma - 1}{2} \ma_1^2}{1 + \frac{\gamma - 1}{2} \ma_2^2}
			\end{IEEEeqnarray*} \pause
			
			\footnotesize huhr!
	\end{frame}

	\begin{frame}
			
			Hey! look at this! \pause
			\begin{IEEEeqnarray}{rCl}
						\frac{\ma_1}{\ma_2}	&=& \frac{1 + \gamma \ma_1^2}{1 + \gamma \ma_2^2} \left(\frac{1 + \frac{\gamma - 1}{2} \ma_1^2}{1 + \frac{\gamma - 1}{2} \ma_2^2}\right)^\frac{1}{2} \pause
			\end{IEEEeqnarray}
			
			The ratio $\ma_1/\ma_2$ depends only on $\ma_1$ and $\ma_2$!  \pause
			
			If the shock wave is perpendicular, \textbf{there is only one possible post-shock Mach number for any given upstream Mach number}.
	\end{frame}

	\begin{frame}
		
			After some algebra, isolate $\ma_2$:\pause
			\begin{IEEEeqnarray}{rCl}
						\ma_2^2	&=& \frac{\ma_1^2 + \frac{2}{\gamma - 1}}{2 \ma_1^2 \frac{\gamma}{\gamma - 1} - 1}\label{eq_m_normal_shock} \pause
			\end{IEEEeqnarray}
			
			\footnotesize (but tables and a graph are fine, too)
			
	\end{frame}
	
	\oldfigureframe{}{cengel_shock_table}{1}{Figure \copyright\xspace Çengel \& Cimbala 2010 \cite{cengelcimbala2010}}

\section{Compressible flow beyond frictionless pipe flow}

	\begin{frame}{Compressible flows are flows, too:}\pause
		\begin{itemize}
			\item they have boundary layers\pause
			\item they can be laminar or turbulent\pause
			\item oblique shock waves reflect and interact\pause
			\item heat transfers can also affect density\pause
			\item phase changes are fun, too
		\end{itemize}	
	\end{frame}
	
	
	\pictureframe{1}{Expedition_47_Soyuz_TMA-19M_Landing_(NHQ201606180019)}{Soyuz TMA-19M, 2016-06-18}{\wcfile{Expedition_47_Soyuz_TMA-19M_Landing_(NHQ201606180019)}{Photo} by Bill Ingalls, NASA (\pd)}

	\pictureframe{1}{Expedition_47_Soyuz_TMA-19M_Landing_(NHQ201606180030).jpg}{}{\wcfile{Expedition 47 Soyuz TMA-19M Landing (NHQ201606180030).jpg}{Photo} by Bill Ingalls, NASA (\pd)}

	\pictureframe{1}{Expedition_47_Soyuz_TMA-19M_Landing_(NHQ201606180031).jpg}{}{\wcfile{Expedition_47_Soyuz_TMA-19M_Landing_(NHQ201606180031).jpg}{Photo} by Bill Ingalls, NASA (\pd)}

	\begin{frame}
		\begin{centering}
			A Picture to Change the World
			
		\end{centering}
	\end{frame}
	
	\figureframe{0.5}{rect4826}{}{}

	\begin{frame}
		Low Earth orbit: \SI[per-mode=symbol]{28 000}{\kilo\metre\per\hour}
	\end{frame}

	\pictureframe{1}{Apollo-11-Capsule}{}{\wcfile{Apollo-11-Capsule.jpg}{Photo} by \textsc{nasa} (1969, \pd)}
	
	\pictureframe{1}{Apollo_cm}{}{\wcfile{Apollo cm.jpg}{Drawing} by \textsc{nasa} (1968, \pd)}

	\begin{frame}
		Low Earth orbit: \SI[per-mode=symbol]{28 000}{\kilo\metre\per\hour}
	\end{frame}

	\figureframe{1}{path41261}{}{\wcfile{RP1357 p174 Saturn V.svg}{Drawing} by David S. F. Portree, NASA (1995, \pd)}
	\figureframe{1}{path41262}{}{\wcfile{RP1357 p174 Saturn V.svg}{Drawing} by David S. F. Portree, NASA (1995, \pd)}
	\figureframe{1}{path41263}{}{\wcfile{RP1357 p174 Saturn V.svg}{Drawing} by David S. F. Portree, NASA (1995, \pd)}
	\figureframe{1}{path41264}{}{\wcfile{RP1357 p174 Saturn V.svg}{Drawing} by David S. F. Portree, NASA (1995, \pd)}
	\figureframe{1}{path4126}{}{\wcfile{RP1357 p174 Saturn V.svg}{Drawing} by David S. F. Portree, NASA (1995, \pd)}
	
	\oldpictureframe{}{S-IC_engines_and_Von_Braun}{1}{\wcfile{S-IC engines and Von Braun.jpg}{Photo} NASA (1969, \pd)}
	\skipinprint{\oldpictureframe{}{img_48137}{1}{}}

	\pictureframe{1}{Apollo_8_Liftoff_View_(24246227076)_(2).jpg}{}{\wcfile{Apollo 8 Liftoff View (24246227076) (2).jpg}{Photo} by \textsc{nasa} (1968, \pd)}
	
	\pictureframe{1}{As08-16-2593}{}{\wcfile{As08-16-2593.jpg}{Photo} by Bill Anders, \textsc{nasa} (1968, \pd)}
	
	\pictureframe{0.3}{As08-16-2593}{}{\wcfile{As08-16-2593.jpg}{Photo} by Bill Anders, \textsc{nasa} (1968, \pd)}

	\begin{frame}{Take-home slide for lecture 9}\pause
		\begin{centering}\textit{(violins play in background)}
		
		\end{centering}
		\begin{itemize} \pause
			\item Density changes = momentum storage \pause
			\item Isentropic expansion to accelerate:\\ \pause
					$\ma < 1$: converge\\ \pause
					$\ma > 1$: diverge \pause
			\item Shock waves: pressure increase at constant energy \pause
		\end{itemize}
	\end{frame}

\mainslidesend

\end{document}

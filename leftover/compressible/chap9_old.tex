\renewcommand{\lastedityear}{2018}
 \renewcommand{\lasteditmonth}{07}
   \renewcommand{\lasteditday}{05}
\renewcommand{\numberofthischapter}{9}
\renewcommand{\titleofthischapter}{Introduction to compressible flow}

\fluidmechchaptertitle

\mecafluboxen

\section{Motivation}

	\youtubethumb{Lk8BMRC_Cyw}{pre-lecture briefing for this chapter}{\oc (\ccby)}
	In this chapter we investigate the fundamentals of compressible flows: those in which density varies significantly. This study should allow us to answer two questions:
	\begin{itemize}
		\item What happens in ideal (reversible) compressible flows?
		\item What is a shock wave, and how does it influence a flow?
	\end{itemize}


\section{Compressibility and its consequences}

	\subsection{Problem description}

		Ever since we have begun describing fluid flow in an extensive manner in chapter~4, we have restricted ourselves to incompressible flows — those in which the fluid density $\rho$ is uniform and constant. Now,~$\rho$ becomes one additional unknown property field.
	
		In some cases, solving for an additional unknown is not particularly challenging. For example, if we wish to calculate water temperature in a heat transfer case, or species concentration in a reacting flow, it is possible to first solve for the flow, and then solve for those particular property fields within the solution; thus the additional unknowns are solved separately with additional equations.
		
		The mathematical treatment of density, however, is much worse, because $\rho$ appears in the momentum and mass conservation equations. Density directly affects the velocity fields, and so we have solve for $\rho$, $p$ and $\vec V$ \emph{simultaneously}.
	
		From a physical point of view, compressibility brings two new effects:
		\begin{itemize}
			\item Density changes in fluids translate into large changes in fluid mechanical energy; these can be come very large in comparison to other forms of fluid energy, in particular kinetic energy. When $\rho$ varies, fluids “perform work upon themselves”, accumulating and expending this energy, which translates into kinetic energy changes, and irreversible losses as heat transfer.
			\item When density is allowed to vary, pressure travels at \emph{finite speed} within the fluid — one could say that the fluid becomes appreciably “squishy”. The delay resulting from this finite pressure propagation speed becomes very significant if the fluid velocity is high, and it may even prevent the propagation of pressure waves in the upstream direction altogether.
		\end{itemize}
		
		In this chapter, we wish to quantify both of those effects. By now, after eight chapters already where~$\rho$ was just an input constant, the reader should readily agree we have no hope of coming up with a general solution to compressible flow. Nevertheless, it is possible to reproduce and quantify the most important aspects of compressibility effects with relatively simple, one-dimensional air flow cases — and this will suffice for us in this last exploratory chapter.
		

	\subsection{The speed of pressure}

		We start by considering the \emph{speed of pressure}. In compressible fluids, pressure changes propagate with a speed that increases with the magnitude of the pressure change. When this magnitude tends to zero (and the pressure wave reduces to that of a weak sound wave, with fully-reversible fluid compression and expansion) then the speed tends to a given speed which we name \vocab{speed of sound} and noted~$c$. At ambient conditions, this speed is approximately \SI[per-mode=symbol]{1000}{\kilo\metre\per\hour} in air and roughly \SI[per-mode=symbol]{5000}{\kilo\metre\per\hour} in water.
		
		Let us, as a thought experiment, create a small pressure change inside a static fluid, and then travel along with the pressure wave. As we follow a wave which travels from right to left, we perceive fluid movement from left to right (\cref{fig_oned_wave}). We construct a small control volume encompassing and moving with the wave — at the speed of sound~$c$.
	
	\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{images/pressure_wave.png}
			\end{center}
			\supercaption{An infinitesimal pressure-difference wave is represented traveling from right to left inside a stationary fluid, in a one-dimensional tube. From the point of view of a control volume attached to (and traveling with) the wave, the flow is from left to right.}{\wcfile{Pressure wave 2.svg}{Figure} \cczero \oc}
			\label{fig_oned_wave}
	\end{figure}
	
	Control volume fluid flow analysis does not frighten us anymore. From mass conservation (eq.~\ref{eq_rtt_mass} p.\pageref{eq_rtt_mass}) we write:
		\begin{IEEEeqnarray}{rCl}
			\rho c A &=& (\rho + \diff \rho) (c + \diff V) A\label{eq_continuity_pressurewave}
		\end{IEEEeqnarray}
	
	This equation~\ref{eq_continuity_pressurewave} relates the density change across the wave to the velocity change between inlet and outlet. Re-arranging, and focusing only on the case where the pressure wave is extremely weak (as are sound waves in practice), we see the product $\diff \rho \diff V$ vanish and obtain:
		\begin{IEEEeqnarray}{rCl}
			\rho c &=& \rho c + \rho \diff V + c \diff \rho + \diff \rho \diff V \nonumber\\
			\rho \diff V &=& - c \diff \rho \label{eq_tmpsoundone}
		\end{IEEEeqnarray}

	In equation~\ref{eq_tmpsoundone}, which relates the speed of sound~$c$ to the density~$\rho$, we would now like to eliminate the~$\diff V$ term. For this, we turn to the control volume momentum equation (eq:~\ref{eq_rtt_linearmom} p.\pageref{eq_rtt_linearmom}):
		\begin{IEEEeqnarray}{rCl}
			F_\net					&=& -\dot m_\inn V_\inn + \dot m_\out V_\out = \dot m (V_\out - V_\inn)	\nonumber\\
			p A - (p + \diff p) A 	&=& \rho c A \left[(c + \diff V) - c\right]	\nonumber\\
			-\diff p  				&=& \rho c \diff V	\nonumber\\
			\frac{\diff p}{c} 		&=& -\rho \diff V	\label{eq_tmpsoundtwo}
		\end{IEEEeqnarray}

		We can now combine eqs. (\ref{eq_tmpsoundone}) and (\ref{eq_tmpsoundtwo}) to obtain:
		\begin{IEEEeqnarray}{rCl}
			c &=& \sqrt{\frac{\diff p }{\diff \rho}}
		\end{IEEEeqnarray}
		
		This can be generalized for any pressure wave, and we state that
		\begin{IEEEeqnarray}{rCl}
			c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} \label{eq_def_speed_sound}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where the $s = \cst$ subscript indicates that entropy $s$ is constant.
		\end{equationterms}
		
		So now, we see that in a fluid the speed of sound —the speed of pressure waves when they travel in a reversible (constant-entropy) manner— is the square root of the derivative of pressure with respect to density. This allows us to compare it to another term, the \vocab{bulk modulus of elasticity} $K$, which represents the differential amount of pressure that one has to apply on a body to see its density increase by a certain percentage:
		\begin{IEEEeqnarray}{rCl}
			K &\equiv& \frac{\diff p}{\frac{\diff \rho}{\rho}}  \label{eq_def_bulk_modulus}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where $K$ is expressed in \si{\pascal}.
		\end{equationterms}
		
		Inserting eq.~\ref{eq_def_bulk_modulus} into eq.~\ref{eq_def_speed_sound}, we obtain a relationship which we had introduced already in chapter~6 (as eq.~\ref{eq_speed_sound_elasticity_one} p.\pageref{eq_speed_sound_elasticity_one}):
		\begin{IEEEeqnarray}{rCl}
			c &=& \sqrt{\frac{K_{s=\cst}}{\rho}} \label{eq_speed_sound_elasticity_two}
		\end{IEEEeqnarray}
		With this equation \ref{eq_speed_sound_elasticity_two} we see that in any fluid, the square of the speed of sound increases inversely proportionally to the density and proportionally to the modulus of elasticity (the “hardness”) of the fluid.


	\subsection{The measure of compressibility}
		
		The most useful measure of compressibility in fluid mechanics is the Mach number, which we already defined (with eq.~\ref{eq_def_ma} p.\pageref{eq_def_ma}) as:
		\begin{IEEEeqnarray}{rCl}
			\ma &\equiv& \frac{V}{c}
		\end{IEEEeqnarray}
		As we saw in chapter~6 (\S\ref{ch_mach_number_force_ratios} p.\pageref{ch_mach_number_force_ratios}), $\ma^2$ is proportional to the ratio of net force to elastic force on fluid particles: at high Mach numbers the forces related to the compression and expansion of the fluid dominate its dynamics.
		
		Not all compressible flows feature high Mach numbers. In large-scale atmospheric weather for example, air flows mush slower than the speed of sound yet large density changes can occur because of the vertical (static) pressure gradient. The same is true in low-speed centrifugal machines because of centrifugal pressure gradients. In this chapter however, we choose to focus on the simplest of compressible flows: one-directional gas flows with Mach numbers approaching or exceeding~1.
		
		

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Thermodynamics of isentropic flow for a perfect gas]{Thermodynamics of isentropic flow\\ for a perfect gas}
\label{thermodynamics_isentropic_flow_perfect_gas}


	\subsection{Principle}
		
		When a gas is compressed or expanded, its pressure and temperature vary together with its density. Before we consider the \emph{dynamics} of such phenomena, we need a robust model to relate those properties. This is the realm of \vocab{thermodynamics}.
		
		In short, from a macroscopic point of view, a gas behaves as a mechanical spring (\cref{fig_gas_spring}): one needs to provide it with energy as work in order to compress it, and one recovers energy as work when it expands. Three important points must be made regarding these compressions and expansions:
			\begin{figure}
				\begin{center}
					\includegraphics[width=0.4\textwidth]{work_cylinder}
				\end{center}
				\supercaption{When they are compressed or expanded, the behavior of fluids can be modeled as if they were mechanical springs: they gain energy during compressions, and lose energy during expansions. These spring can be thought of as being “fragile”: when the movements are sudden, compressions involve more mechanical energy, and expansions involve less.}{\wcfile{Pistons ressort et fluide.svg}{Figure} \ccbysa \olivier}
				\label{fig_gas_spring}
			\end{figure}
				\begin{figure}
					\begin{center}
						\includegraphics[width=0.6\textwidth]{pv_diagram_compression}
					\end{center}
					\supercaption{Pressure-volume diagram of a perfect gas during a compression. When the fluid is heated up, more work is required; when it is cooled, less work is required. Opposite trends are observed during expansions.}{\wcfile{P-v diagram reversible evolutions.svg}{Figure} \cczero \olivier}
					\label{fig_pv_gas}
				\end{figure}
		\begin{enumerate}
			\item When gases are heated during the movement, the work transfers are increased; conversely they are decreased when the gas is cooled (\cref{fig_pv_gas}). Cases where no heat transfer occurs are called \vocab{adiabatic};
			\begin{figure}
				\begin{center}
					\includegraphics[width=0.5\textwidth]{isentropic_compressions_expansions}
				\end{center}
				\supercaption{When no heat transfer occurs, the a compression or expansion is said to be \vocab{adiabatic}. The temperature of fluids can still vary significantly because of the compression or expansion. When adiabatic evolutions are infinitely smooth, they are termed \vocab{isentropic}.}{\wcfile{Isentropic evolutions of a perfect gas.svg}{Figure} \cczero \olivier}
				\label{fig_isentropics}
			\end{figure}
			\item The temperature of gases tends to increase when they are compressed, and tends to decrease when they expand, \emph{even when no heat transfer occurs} (\cref{fig_isentropics});
			\item A gas behaves as a “fragile” spring: the faster a compression occurs, and the more work is required to perform it. Conversely, the faster an expansion occurs, and the less work is recovered from it. Cases where evolutions occur infinitely slowly are used as a reference for comparison: such cases are termed \vocab{reversible}. Cases where the evolution is both adiabatic and reversible are named \vocab{isentropic} (\emph{iso-entropic}, because they occur at constant entropy).
		\end{enumerate}
	
	\subsection{Pressure, temperature, density and the speed of sound}
		
		The quantitative relations between pressure, temperature and density of perfect gas during isentropic evolutions are classical results in the study of thermodynamics. In this section, we wish to re-write them with particular focus placed on the speed of sound and the Mach number. The goal is to arrive to eqs.~\ref{eq_hor_crit_one} to~\ref{eq_hor_crit_three} p.\pageref{eq_hor_crit_one}, which we need in the upcoming sections to describe supersonic flow of gases.
		
		Let us then focus on the isentropic evolutions of a perfect gas. It is known from the study of thermodynamics that in isentropic flow from a point~1 to a point~2, the properties of a perfect gas are related one to another by the relations:
		\begin{IEEEeqnarray}{rCl}
			\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma -1}		\label{eq_hor_one}\\
			\left( \frac{T_1}{T_2} \right)	& = & \left( \frac{p_1}{p_2} \right)^{\frac{\gamma -1}{\gamma}} \label{eq_hor_two}\\
			\left( \frac{p_1}{p_2} \right)	& = & \left( \frac{v_2}{v_1} \right)^{\gamma}		\label{eq_hor_three}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item for a perfect gas, during an isentropic (constant-entropy) evolution.
		\end{equationterms}
		
		It follows from eq.~\ref{eq_hor_three} that in an isentropic evolution, a perfect gas behaves such that $p \rho^{-\gamma} = k = \text{cst.}$, which enables us to re-write equation~\ref{eq_def_speed_sound} for a perfect gas as:
		\begin{IEEEeqnarray*}{rCl}
			c &=& \sqrt{\left.\partialderivative{p}{\rho}\right|_{s=\cst}} = \sqrt{k \gamma \rho^{\gamma - 1}} = \sqrt{p \rho^{-\gamma} \gamma \rho^{\gamma - 1}} = \sqrt{\gamma p \rho^{-1}}
		\end{IEEEeqnarray*}
			The speed of sound in a perfect gas can therefore be expressed as:
		\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
			c 		&=& \sqrt{\gamma R T} \label{eq_speed_sound_gamma_rt}\\
			\ma 	&=& \frac{V}{\sqrt{\gamma R T}} \label{eq_speed_sound_pg}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item for any evolution in a perfect gas.
		\end{equationterms}
		\end{mdframed}
		Thus, in a perfect gas, the speed of sound depends only on the local temperature.

		We now introduce the concept of \vocab{stagnation} or \vocab{total properties}, a measure of the total amount of specific energy possessed by a fluid particle at a given instant. They are the properties that the fluid would have if it was brought to rest in an isentropic manner:
		\begin{IEEEeqnarray}{rCl}
			h_0 	&\equiv&	h + \frac{1}{2} V^2 \\
			T_0 	&\equiv& T + \frac{1}{c_p} \frac{1}{2} V^2 \label{eq_stag_temp}\\
			h_0 	&=& 		c_p T_0
		\end{IEEEeqnarray}
		
		Using equation~\ref{eq_speed_sound_pg}, we can re-write eq. (\ref{eq_stag_temp}) as:
		\begin{IEEEeqnarray*}{rCl}
			\frac{T_0}{T} 	&=& 1 + \frac{1}{c_p T} \frac{1}{2} V^2 =  1 + \frac{1}{\frac{\gamma R}{\gamma - 1} T} \frac{1}{2} V^2\\
							&=& 1 + \left(\frac{\gamma - 1}{2}\right) \frac{V^2}{c^2} = 1 + \left(\frac{\gamma - 1}{2}\right) \ma^2
		\end{IEEEeqnarray*}

		Eqs. (\ref{eq_hor_one}) to (\ref{eq_hor_three}) can thus be re-written to express the transition from flow condition to stagnation condition:
			\begin{IEEEeqnarray}{rCl}
				\left( \frac{T_0}{T} \right)	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma^2		\label{eq_hor_m_one}\\
				\left( \frac{p_0}{p} \right)	& = & \left[1 + \left(\frac{\gamma - 1}{2}\right) \ma^2\right]^\frac{\gamma}{\gamma - 1} \label{eq_hor_m_two}\\
				\left( \frac{\rho_0}{\rho} \right)	& = & \left[1 + \left(\frac{\gamma - 1}{2}\right) \ma^2\right]^\frac{1}{\gamma - 1}\label{eq_hor_m_three}
			\end{IEEEeqnarray}
		These three equations (\ref{eq_hor_m_one}) to (\ref{eq_hor_m_three}) express, for any point inside the flow of a perfect gas, the local Mach number $\ma$ as a function of the ratio of the local (current) property ($T$, $p$ or $\rho$) to the local \vocab{total} property ($T_0$, $p_0$, $\rho_0$). These ratios are always inferior or equal to one, by definition.

		Now, if now we define \vocab{critical conditions} as those that would occur if the fluid was exactly at $\ma = 1$, noting them with an asterisk, we can re-write eqs.~(\ref{eq_hor_m_one}) to~(\ref{eq_hor_m_three}) comparing stagnation properties to critical properties:
			\begin{IEEEeqnarray}{rCl}
				\left( \frac{T^{*}}{T_0} \right)	& = & \frac{2}{\gamma + 1} \label{eq_hor_crit_one}\\
				\left( \frac{p^{*}}{p_0} \right)	& = & \left[\frac{2}{\gamma + 1}\right]^\frac{\gamma}{\gamma - 1} \label{eq_hor_crit_two}\\
				\left( \frac{\rho^{*}}{\rho_0} \right)	& = & \left[\frac{2}{\gamma + 1}\right]^\frac{1}{\gamma - 1}\label{eq_hor_crit_three}
			\end{IEEEeqnarray}
		Here, we compare the local total property (e.g. the total temperature $T_0$) to the value the property needs to have for the flow to be at sonic speed (e.g. the critical temperature $T^*$). This ratio does not depend at all on the fluid flow: it is merely a function of the (invariant) characteristics of the gas.
	
		This is it! It is important to understand that nothing in this section describes a particular flow movement. We have merely derived a convenient tool expressing flow properties as a function of their stagnation properties and the local Mach number, which we shall put to good use further down.



\section{Speed and cross-sectional area}

	Let us now go back to fluid dynamics. The last time we considered frictionless pipe flow, in chapter~5, we situation was rather simple. From the one-dimensional mass conservation equation, eq.~\ref{eq_mass_conservation_pipe_flow}:
		\begin{IEEEeqnarray}{rCl}
			\rho_1 V_1 A_1 &=& \rho_2 V_2 A_2 \label{eq_mass_conservation_compressible_pipe_flow}
		\end{IEEEeqnarray}
	it was possible to relate cross-sectional area $A$ and velocity $V$ easily. Now that $\rho$ is also allowed to vary, the situation is decidedly more interesting.
	
	We wish to let static, pressurized air accelerate as fast as possible: letting $V$ increase at a cost of a decease in $\rho$ according to the rules of an isentropic, steady expansion. We know however, that according to mass conservation, $\rho V A$ has to remain constant all along the acceleration, so that only one given nozzle geometry ($A = f(p, T)$) will permit the desired acceleration. How can we find this geometry?
	
	We start from the conservation of mass inside our isentropic acceleration nozzle, and differentiating the terms:
	\begin{IEEEeqnarray*}{rCl}
			\rho A V					&=& \text{cst.}\\
			\ln \rho + \ln A + \ln V 	&=& \text{cst.}\\
			\frac{\diff \rho}{\rho} + \frac{\diff A}{A} + \frac{\diff V}{V} &=& 0	\label{eq_mass_derivatives}
	\end{IEEEeqnarray*}
	\begin{equationterms}
		\item for any steady flow.
	\end{equationterms}
	
	Let us set this equation~\ref{eq_mass_derivatives} aside for a moment. During our isentropic expansion, the total energy of the fluid remains constant, thus the total specific enthalpy $h_0$ is constant,
	\begin{IEEEeqnarray*}{rCl}
			h + \frac{V^2}{2} &=& \text{cst.}
	\end{IEEEeqnarray*}
	and here also we differentiate, obtaining:
	\begin{IEEEeqnarray}{rCl}
			\diff h + V \diff V &=& 0 \label{eq_tmp_comp}
	\end{IEEEeqnarray}
	In last relation, since the process is reversible and without external energy input, we can write the first term as $\diff h = \diff p / \rho$, yielding:
	\begin{IEEEeqnarray}{rCl}
			\frac{1}{\rho} \diff p + V \diff V &=& 0\nonumber\\
			\frac{1}{\rho} &=& -\frac{1}{\diff p} V \diff V	\label{eq_tmp_rho}
	\end{IEEEeqnarray}
	\begin{equationterms}
		\item for isentropic gas flow.
	\end{equationterms}

	This expression of $1/\rho$ is the relation we were after. Now, coming back to equation~\ref{eq_mass_derivatives} and inserting eq.~(\ref{eq_tmp_rho}), we get:
	\begin{IEEEeqnarray*}{rCl}
			- \derivative{\rho}{p} V \diff V + \frac{\diff A}{A} + \frac{\diff V}{V} &=& 0\\
			\frac{\diff V}{V} \left[1 - \derivative{\rho}{p} V^2\right] + \frac{\diff A}{A} &=& 0
	\end{IEEEeqnarray*}
	In this last relation, since we are in an isentropic flow, we can insert an expression for the speed of sound, $c^2 = \left.\partialderivative{p}{\rho}\right|_s$ derived from equation~\ref{eq_def_speed_sound}, obtaining:
	\begin{IEEEeqnarray*}{rCl}
			\frac{\diff V}{V} \left[1 - \frac{1}{c^2} V^2\right] + \frac{\diff A}{A} &=& 0\\
			\frac{\diff V}{V} \left[1 - \ma^2\right] + \frac{\diff A}{A} &=& 0
	\end{IEEEeqnarray*}
	
	This leads us to the following equation relating area change and speed change in compressible gas flow:
	\begin{mdframed}
	\begin{IEEEeqnarray}{rCl}
		\frac{\diff V}{V} &=& \frac{\diff A}{A} \frac{1}{\ma^2 - 1} \label{eq_v_a_m}
	\end{IEEEeqnarray}
	\begin{equationterms}
		\item for isentropic, one-dimensional gas flow.
	\end{equationterms}
	\end{mdframed}

	This equation~\ref{eq_v_a_m} is one of the most stunning equations of fluid dynamics. It relates the change in flow cross-section area, $\diff A$, to the change in speed $\diff V$, for lossless flow. Let us examine how these two terms relate one to another, as displayed in figure~\ref{fig_converging_vs_diverging}:
	\begin{itemize}
		\item When $\ma<1$, the term $1/(\ma^2 - 1)$ is negative, and $\diff A$ and $\diff V$ are always of opposite sign: an increase in area amounts to a decrease in speed, and a decrease in area amounts to an increase in speed.
		\item When $\ma>1$, $\diff A$ and $\diff V$ have the same sign, which —perhaps disturbingly— means that an increase in area amounts to an \emph{increase} in speed, and a decrease in area amounts to a decrease in speed.
		%\item The tipping point will occur at $\ma=1$, i.e. if the flow is to transit from subsonic to supersonic speed, a \emph{change in nozzle geometry} has to occur.
	\end{itemize}
	\begin{figure}[ht!]%handmade
		\begin{center}
			\includegraphics[width=0.8\textwidth]{subsonic_supersonic.png}
		\end{center}
		\supercaption{Changes in nozzle cross-section area have opposite effects when the Mach number~$\ma$ is lower or greater than~1.\\
		Note that because temperature changes as the gas compresses and expands, the local Mach number varies as the flow changes speed. It is thus possible for the flow to go from one regime to another during an expansion or compression. These effects are studied further down and described in figure~\ref{fig_converging_diverging_nozzle} p.\pageref{fig_converging_diverging_nozzle}.}{\wcfile{Subsonic vs. supersonic flow behaviors.svg}{Figure} \cczero \oc}
		\label{fig_converging_vs_diverging}
	\end{figure}

	Thus, if we want to let a static, compressed fluid accelerate as fast as possible, we need a converging nozzle up to $\ma=1$, and a diverging nozzle onwards.


%%%%%%%%%%%%%%%%%%%%%%
\section{Isentropic flow in converging and diverging nozzles}

	The change of between the subsonic and supersonic flow regimes, by definition, occurs at Mach one. When the flow is isentropic, equation~\ref{eq_v_a_m} above tells us that the $\ma=1$ case \emph{can only occur at a minimum area}.
	
	Of course, there is no guarantee that a pipe flow will be able to become supersonic (\cref{fig_isentropic_expansion}). For this, very large pressure ratios between inlet and outlet are required. Nevertheless, whichever the pressure ratio, according to eq.~\ref{eq_v_a_m}, a converging nozzle will only let an expanding fluid accelerate to Mach one (critical conditions). Changing the nozzle outlet area itself will not permit further acceleration unless an increase in nozzle area is generated beyond the critical point.

	\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{images/isentropic_expansion.png}
			\end{center}
			\supercaption{Non-dimensionalized properties of a gas undergoing an isentropic (i.e. shock-less) expansion through a converging-diverging nozzle.}{\wcfile{Properties isentropic supersonic expansion.svg}{Figure} \cczero \oc}
			\label{fig_isentropic_expansion}
	\end{figure}
	
	This is a surprising behavior. In a converging-only nozzle, increasing the inlet pressure increases the outlet velocity only up until $\ma=1$ is reached at the outlet. From there on, further increases in inlet pressure have \emph{no effect} on the outlet pressure, which remains fixed at $p_\text{outlet} = p^{*}$. The flow then reaches a maximum mass flow rate $\dot m_\max$. This condition is called \vocab{choked flow}.
	
	Once the flow is choked, adding a diverging section further down the nozzle may enable further acceleration, if the pressure ratio allows for it. Nevertheless, the pressure $p_\T \equiv p_\text{throat}$ at the throat remains constant at $p_\T = p^{*}$: from this point onwards, downstream conditions (in particular the pressure) are not propagated upstream anymore: the gas travels faster than the pressure waves inside it. With $p_\T$ still equal to the critical pressure $p^{*}$, the mass flow through the nozzle cannot exceed $\dot m_\max$.

	We can see, therefore, that it follows from equation~\ref{eq_v_a_m} that \emph{there exists a maximum mass flow for any duct}, which depends only on the stagnation properties of the fluid and the throat cross-sectional area.

	The mass flow, at any point in the duct, is quantified as:
	\begin{IEEEeqnarray}{rCl}
		\dot m 	&=& \rho c A \ma \nonumber\\
				&=& \frac{p}{R T} \sqrt{\gamma R T} A \ma \nonumber\\
		\dot m 	&=& A \ma p_0 \sqrt{\frac{\gamma}{R T_0}} \left[1 + \frac{(\gamma - 1) \ma^2}{2}\right]^\frac{-\gamma - 1}{2 (\gamma - 1)} \label{eq_compressible_massflow_general}
	\end{IEEEeqnarray}
	and $\dot m$ will now reach a maximum when the flow is choked, i.e. reaches $\ma=1$ at the throat, where its properties will be $p^{*} = p_\T$, $T^{*} = T_\T$, and the area $A^{*} = A_\T$:
	\begin{IEEEeqnarray}{rCl}
		\dot m_\max &=& \left[\frac{2}{\gamma + 1}\right]^\frac{\gamma + 1}{2 (\gamma - 1)} {A^{*}} p_0 \sqrt{\frac{\gamma}{R T_0}} \label{eq_compressible_massflow_maximum}
	\end{IEEEeqnarray}

	A general description of the properties of a perfect gas flowing through a converging-diverging duct is given in \cref{fig_converging_diverging_nozzle,fig_isentropic_expansion}.
	\begin{figure}[ht!]%handmade
			\begin{center}
				\includegraphics[width=\textwidth]{images/converging_diverging_nozzle.png}
			\end{center}
			\supercaption{The pressure of a gas attempting an isentropic expansion through a converging-diverging duct, represented as a function of distance for different values of outlet pressure~$p_\text{b}$. The green curves show the reversible evolutions.\\
			As long as the back pressure~$p_\text{b}$ is high enough, the flow is subsonic everywhere, and it remains reversible everywhere. When $p_\T$ reaches $p^{*}$, the flow downstream of the throat becomes supersonic. In that case, if the back pressure is low enough, a fully-reversible expansion will occur. If not, then a shock wave will occur and the flow will return to subsonic regime downstream of the shock.}{\wcfile{Converging-diverging nozzle, pressure distribution during acceleration of flow.svg}{Figure} \cczero \oc}
			\label{fig_converging_diverging_nozzle}
	\end{figure}


%%%%%%%%%%%%%%%%%%%%%%
\section{The perpendicular shock wave}

	The movement of an object in a fluid at supersonic speeds (or a supersonic fluid flow over a stationary object, which produces the same effects) provokes pressure field changes that cannot propagate upstream. Thus, if a direction change or a pressure increase is required to flow past an obstacle, fluid particles will be subjected to very sudden changes. These localized changes are called \vocab{shock waves}.
	
	The design of bodies moving at supersonic speeds usually focuses on ensuring pressure and direction changes that are very gradual, and at the limit (e.g. far away from a supersonic aircraft) these are very small. \\
	The limiting case at the other side of the spectrum is that of the \vocab{normal shock wave}, the most loss-inducing (and simple!) type of shock wave. This will typically occur in front of a blunt supersonic body.

	A perpendicular shock wave (\cref{fig_shock_wave}) is studied with the following hypothesis:
		\begin{itemize}
			\item It is a \emph{constant energy} process;
			\item It it a \emph{one-dimensional} process (i.e. the streamlines are all parallel to one another and perpendicular to the shock wave).
		\end{itemize}
		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{images/shock_wave.png}
			\end{center}
			\supercaption{A normal shock wave occurring in a constant-section duct. All properties change except for the total temperature~$T_0$, which, since the transformation is at constant energy, remain constant.}{\wcfile{Shock wave control volume.svg}{Figure} \cczero \oc}
			\label{fig_shock_wave}
		\end{figure}
		
		With these two hypothesis, we can compare the flow conditions upstream and downstream of a shock wave occurring in a tube using the continuity equation, which we wrote above as eq.~\ref{eq_mass_conservation_compressible_pipe_flow}:
		\begin{IEEEeqnarray}{rCl}
			\rho_1 A_1 V_1 &=& \rho_2 A_2 V_2
		\end{IEEEeqnarray}
		The energy equation in this case shrinks down to:
		\begin{IEEEeqnarray}{rCl}
			T_{0 1} &=& T_{0 2}
		\end{IEEEeqnarray}

		Since, from equation~\ref{eq_hor_m_one} p.\pageref{eq_hor_m_one}, we have:
		\begin{IEEEeqnarray*}{rCl}
			\frac{T_{01}}{T_1} 	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma_1^2\\
			\frac{T_{02}}{T_2} 	& = & 1 + \left(\frac{\gamma - 1}{2}\right) \ma_2^2
		\end{IEEEeqnarray*}
		and given that $T_{0 1} = T_{0 2}$, dividing one by the other yields:
		\begin{IEEEeqnarray}{rCl}
			\frac{T_2}{T_1}	& = & \frac{1 + \frac{\gamma - 1}{2} \ma_1^2}{1 + \frac{\gamma - 1}{2} \ma_2^2} \label{eq_tmpi}
		\end{IEEEeqnarray}

		In a similar fashion, we can obtain:
		\begin{IEEEeqnarray}{rCl}
			\frac{p_2}{p_1}	& = & \frac{1 + \gamma \ma_1^2}{1 + \gamma \ma_2^2}	\label{eq_tmpii}
		\end{IEEEeqnarray}

		Let us set aside these two equations~\ref{eq_tmpi} and \ref{eq_tmpii} and compare conditions upstream and downstream starting with mass conservation, and re-writing it as a function of the Mach number:
		\begin{IEEEeqnarray}{rCl}
			\rho_1 V_1				&=& \rho_2 V_2 \nonumber\\
			\frac{\rho_1}{\rho_2}	&=& \frac{V_2}{V_1} = \frac{\ma_2 a_2}{\ma_1 a_1}\nonumber\\
									&=& \frac{\ma_2 \sqrt{\gamma R T_2}}{\ma_1 \sqrt{\gamma R T_1}}\nonumber\\
					\frac{\ma_1}{\ma_2}	&=& \frac{p_2}{p_1} \left(\frac{T_1}{T_2}\right)^\frac{1}{2} \label{eq_tmp_comp2}
		\end{IEEEeqnarray}
		
		So far, we have not obtained anything spectacular, but now, inserting both equations~\ref{eq_tmpi} and~\ref{eq_tmpii} into this last equation~\ref{eq_tmp_comp2}, we obtain the staggering result:
		\begin{IEEEeqnarray}{rCl}
					\frac{\ma_1}{\ma_2}	&=& \frac{1 + \gamma \ma_1^2}{1 + \gamma \ma_2^2} \left(\frac{1 + \frac{\gamma - 1}{2} \ma_2^2}{1 + \frac{\gamma - 1}{2} \ma_1^2}\right)^\frac{1}{2}
		\end{IEEEeqnarray}
		This equation is not interesting because of its mathematical properties, but because of its contents: the ratio $\ma_1/\ma_2$ depends only on $\ma_1$ and $\ma_2$! In other words, if the shock wave is perpendicular, \textbf{there is only one possible post-shock Mach number for any given upstream Mach number}.
		
		After some algebra, $\ma_2$ can be expressed as
		\begin{IEEEeqnarray}{rCl}
					\ma_2^2	&=& \frac{\ma_1^2 + \frac{2}{\gamma - 1}}{2 \ma_1^2 \frac{\gamma}{\gamma - 1} - 1}\label{eq_m_normal_shock}
		\end{IEEEeqnarray}
		
		Values for $\ma_2$ as a function of $\ma_1$ according to equation~\ref{eq_m_normal_shock} for particular values of $\gamma$ can be tabulated or recorded graphically, allowing us to predict properties of the flow across a shock wave. We can therefore quantify the pressure and temperature increases, and velocity decrease, that occur though a perpendicular shock wave.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Compressible flow beyond frictionless pipe flow}

	In this chapter, we have restricted ourselves to one-directional steady flow, i.e. flows inside a pipe; within these flows, we have neglected all losses but those which occur in perpendicular shock waves. This is enough to describe and understand the most important phenomena in compressible flow.
	
	Of course, in reality most compressible flows are much more complex than that. Like other flows, compressible flows have boundary layers, feature laminarity and turbulence, heat transfer, unsteadiness, and more. Shock waves occur not just perpendicularly to flow direction, but also in oblique patterns, reflecting against walls and interacting with boundary layers. Temperature variations in compressible flow are often high enough to provoke phase changes, which complicates the calculation of thermodynamic properties as well as flow dynamics and may result in spectacular visual patterns.
	
	\youtubethumb{eZDaM3MHNqg}{watch a 140-thousand-ton rocket lift away humans on their first ever trip to the moon. Compressible flow of expanding exhaust gases is what allows the maximum amount of thrust to be obtained from a given amount of combustion energy in rocket engines.}{NASA (\pd)}
	All those phenomena are beyond the scope of this study, but the reader can hopefully climb up to more advanced topics such as high-speed aerodynamics, or rocket engine and turbomachine design, where they are prominently featured and can surely make for some exciting learning opportunities.

\atendofchapternotes

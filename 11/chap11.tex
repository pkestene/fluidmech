\renewcommand{\lastedityear}{2019}
 \renewcommand{\lasteditmonth}{06}
   \renewcommand{\lasteditday}{26}
\renewcommand{\numberofthischapter}{11}
\renewcommand{\titleofthischapter}{\namechaptereleven}

\fluidmechchaptertitle
\label{chap_eleven}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	%\youtubethumb{qEE-tyXgWLI}{pre-lecture briefing for this chapter (back when it had a different chapter number)}{\oc (\ccby)}
	This exploratory chapter is not a critical component of fluid dynamics; instead, it is meant as a brief overview of two extreme cases: flows for which viscous effects are negligible, and flows for which they are dominant.
	This exploration should allow us to answer two questions:
	\begin{itemize}
		\item How can we model large-scale flows?
		\item How can we model small-scale flows?
	\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Flow at large scales}

	\subsection{Problem statement}
	
		In this section, we are interested in flow at very large scales: those for which a representative length $L$ is very large. In particular, when $L$ is large enough, the influence of viscosity is reduced. Formally, this corresponds to the case where the Reynolds number $\re \equiv \rho V L/\mu$ is very large.
		
		To examine the mechanics of such a flow, we turn to our beloved non-dimensional Navier-Stokes equation for incompressible flow derived as eq.~\ref{eq_ns_nondim} p.~\pageref{eq_ns_nondim},
			\begin{IEEEeqnarray}{rCl}
				\str\ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^*  & = &	\frac{1}{\fr^2}\ \vec g^* - \eu\ \gradient^* p^* + \frac{1}{\re}\ \gradient^{*2} \vec V^*\nonumber\label{eq_ns_nondim_chapeight}\\
			\end{IEEEeqnarray}
			
		We saw in \chaptereight that we could compare the relative weight of terms: when the Reynolds number $\re$ is very large, the last term becomes negligible relative to the other four. Thus, our governing equation can be reduced as follows:
			\begin{IEEEeqnarray}{rCl}
				\str\ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^*  & \approx &	\frac{1}{\fr^2}\ \vec g^* - \eu\ \gradient^* p^* \label{eq_euler_nondim}
			\end{IEEEeqnarray}
		
		Now, converting eq.~\ref{eq_euler_nondim} back to dimensional terms, the governing momentum equation for large-scale flow becomes:
			\begin{IEEEeqnarray}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} \label{eq_euler_equation}
			\end{IEEEeqnarray}
		
		We see that with the starting proposition that $\re$ was large, we have removed altogether the viscous (last) term from the Navier-Stokes equation. Flows governed by this equation are called \vocab{inviscid} flows. Equation~\ref{eq_euler_equation} is named the \vocab{Euler equation}; it stipulates that the acceleration field is driven only by gravity and by the pressure field.
		

	\subsection{Investigation of inviscid flows}
	
		From what we studied in \chaptereight, we recognize immediately that flows governed by eq.~\ref{eq_euler_equation} are troublesome: the absence of viscous effects facilitates the occurrence of turbulence and makes for much more chaotic behaviors. Although the removal of shear from the Navier-Stokes equation simplifies the governing equation, the \emph{solutions} to this new equation become even harder to find and describe.

		What can be done in the other two branches of fluid mechanics?
		\begin{itemize}
			\item Large-scale flows are difficult to investigate experimentally. As we have seen in \chaptereight, scaling down a flow (\eg so it may fit inside a laboratory) while maintaining constant $\re$ requires increasing velocity by a corresponding factor.
			\item Large-scale flows are also difficult to investigate numerically. At high $\re$, the occurrence of turbulence makes for either an exponential increase in computing power (we saw in \chapternineshort that direct numerical simulation computing power increases with $\re^{\num{3,5}}$), or for increased reliance on hard-to-calibrate turbulence models (in Reynolds-averaged simulations).
		\end{itemize}
		
		All three branches of fluid mechanics, therefore, struggle with large-scale flows, because of turbulence.
		
		In spite of this, large-scale flows are undeniably important. In \chaptertenshort we were able to understand and describe fluid flow very close to walls. Now we wish to be able to to the same for large structures, for example, in order to describe the broad patterns of fluid flow (in particular, pressure distribution) in the wake of an aircraft, around a wind turbine, or within a hurricane. It is clear that we have no hope of accounting easily for turbulence, but we can at least describe the main features of such flows by restricting ourselves to laminar cases. In the following sections, we will \emph{model} such laminar solutions directly, based on intuition and observation, and make sure that they match the condition described by eq.~\ref{eq_euler_equation} above.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Plotting velocity with functions}

	\subsection{Kinematics that fit conservation laws}
	
		For simple flow structures, the velocity field can be simply described based on observation and intuition. This is done for example in exercise~\ref{exo_tornado}, where we reconstruct the flow field within and around a tornado using very simple, almost primitive, kinematics.
		
		Without so much as a small increase in complexity, this approach becomes untenable. In the last exercises of \chaptersixshort (ex.~\ref{exo_vortex_continuity} \& \ref{exo_pressure_fields} p.~\pageref{exo_pressure_fields}) we have seen that it is easy to propose a velocity field that does not satisfy (is not a solution of) either the mass balance equation or the momentum balance equation. For example, if one considers \emph{two} of the tornado flows mentioned above together, the velocity field cannot be described easily anymore.
		
		One approach has been developed in the 17\up{th} century to overcome this problem. It consists in finding a family of flows, all steady, that \emph{always} satisfy the conservation equations. Those flows can then be added to one another to produce new flows which satisfy the balance equations. Such flows are called \vocab{potential flows}.
		
		Two conditions need to be fulfilled for this approach to work:
		\begin{enumerate}
			\item The velocity field must always be describable with a function; that is, there must correspond a single value of $u$, of $v$ and of $w$ at each of the coordinates $x_i$, $y_i$, or $z_i$ in space. This means in practice that we cannot account for flows which “curl up” on themselves, occasionally recirculating back on their path.
			\item The velocity field must conserve mass. In incompressible flow, this is achieved if the continuity equation $\divergent{\vec V} = 0$ (eq.~\ref{eq_continuity_der_inc} p.~\pageref{eq_continuity_der_inc}) is respected.
		\end{enumerate}
		
		With potential flow, these two conditions are addressed as follows:
		\begin{enumerate}
			\item We restrict ourselves to \vocab{irrotational} flows, those in which the curl of velocity (see Appendix~\ref{appendix_field_operators} p.~\pageref{appendix_field_operators}) is always null:
				\begin{IEEEeqnarray}{rCl}
					\curl{\vec V} &=& \vec 0
				\end{IEEEeqnarray}
				\begin{equationterms}
					\item by definition, for an irrotational flow.
				\end{equationterms}
				It can be shown that flows are irrotational when there exists a scalar function $\phi$ (pronounced “phi” and named \vocab{potential function}) of which the gradient is the velocity vector field:
				\begin{IEEEeqnarray}{rCl}
					\gradient{\phi} &\equiv& \vec V
				\end{IEEEeqnarray}
				In the case of two-dimensional flow, this translates as:
				\begin{IEEEeqnarray}{rCl}
					\partialderivative{\phi}{x} &\equiv& u\\
					\partialderivative{\phi}{y} &\equiv& v
				\end{IEEEeqnarray}
				
				When plotted out, lines of constant $\phi$ (named \vocab{equipotential lines}) are always perpendicular to the streamlines of the flow.
				
			%%%%%%%
			\item The continuity equation is satisfied by referring to stream functions. It can be shown that the divergent of velocity is null when there exists a vector field function $\vec \psi$ (pronounced “psi” and named \vocab{stream function}) of which the curl is the velocity vector field:
				\begin{IEEEeqnarray}{rCl}
					\curl{\vec \psi} = \vec V \label{eq_curl_psi}
				\end{IEEEeqnarray}
				In the case of two-dimensional flow, $\psi$ is a scalar field and eq.~\ref{eq_curl_psi} translates as:
				\begin{IEEEeqnarray}{rCl}
					\partialderivative{\psi}{y}  &\equiv& u\\
					-\partialderivative{\psi}{x} &\equiv& v
				\end{IEEEeqnarray}
				
				When plotted out, lines of of constant $\psi$ value are \vocab{streamlines} – in other words, as they travel along, fluid particles follow paths of constant $\psi$ value.
			
		\end{enumerate}
		

		In summary, we have shifted the problem from looking for $u$ and $v$, to looking for $\psi$ and $\phi$. The existence of such functions ensures that flows can be added and subtracted from one another yet will always result in mass-conserving, mathematically-describable flows. If such two functions are known, then the velocity components can be obtained (recovered) easily either in Cartesian coordinates,
			\begin{IEEEeqnarray}{rCl}
				u &=& \partialderivative{\phi}{x} = \partialderivative{\psi}{y}\\
				v &=& \partialderivative{\phi}{y} = - \partialderivative{\psi}{x}
			\end{IEEEeqnarray}
			or angular coordinates:
			\begin{IEEEeqnarray}{rCl}
				v_r 			&=& \partialderivative{\phi}{r} = \frac{1}{r} \partialderivative{\psi}{\theta}\\
				v_\theta 	&=& \frac{1}{r} \partialderivative{\phi}{\theta} = - \partialderivative{\psi}{r}
			\end{IEEEeqnarray}


	\subsection{Strengths and weaknesses of potential flow}

		The potential flow methodology allows us to find solutions to the Euler equation: flows in which the Reynolds number is high enough that viscosity has no significant role anymore. Potential flows are the simplest solutions that we are able to come up with. They are:
		\begin{itemize}
			\item strictly steady;
			\item inviscid;
			\item incompressible;
			\item devoid of energy transfers;
			\item two-dimensional (in the scope of this course at least).
		\end{itemize}

		This is quite convenient for the academician, who recognizes immediately four of the five criteria which we set forth in \chaptertwo for using the Bernoulli equation. Along a streamline, the fifth condition is met, and Euler’s equation reduces to eq.~\ref{eq_bernoulli} (p.~\pageref{eq_bernoulli}), reproduced here:
			\begin{IEEEeqnarray}{rCl}
				\frac{p_1}{\rho} + \frac{1}{2} V_1^2 + g z_1 & = & \frac{p_2}{\rho} + \frac{1}{2} V_2^2 + g z_2  = \text{cst.} \label{eq_bernoulli_cst}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item along a streamline in a steady incompressible inviscid flow.
			\end{equationterms}
		and so it follows that if the solution to a potential flow is known, the pressure is known everywhere, and the forces due to pressure can be calculated with relative ease.

		Nevertheless, from a science and engineering point of view, potential flows have only limited value, because they are entirely unable to account for turbulence, which we have seen is an integral feature of high-$\re$ flows. We should therefore use them only with great caution. Potential flows help us model large-scale structures with very little computational cost, but this comes with strong limitations.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Superposition: the lifting cylinder}

	It is possible to describe a handful of basic potential flows called \vocab{elementary flows} as fundamental ingredients that can be added to one another to create more complex and interesting flows. Without going into much detail, the most relevant elementary flows are:
	\begin{itemize}
		\item Uniform longitudinal flow,
			\begin{IEEEeqnarray}{rCl}
				\phi &=& V \ r \cos \theta \\
				\psi &=& V \ r \sin \theta
			\end{IEEEeqnarray}
		\item \vocab{Sources} and \vocab{sinks} (\cref{fig_potential_flow_source}) which are associated with the appearance of a (positive or negative) volume flow rate~$\dot \vol$ from a single point in the flow:
			\begin{IEEEeqnarray}{rCl}
				\phi &=& \frac{\dot \vol}{2 \pi} \ln r\\
				\psi &=& \frac{\dot \vol}{2 \pi} \theta
			\end{IEEEeqnarray}
			\begin{figure}%[ht!]
				\begin{center}
					\includegraphics[width=0.35\textwidth]{images/potential_flow_source}
				\end{center}
				\supercaption{Concept of a source inside a two-dimensional potential flow. A sink would display exactly opposed velocities.}{\wcfile{NSConvection vectorial.svg}{Figure} \ccbysa \wcu{Nicoguaro}}
				\label{fig_potential_flow_source}
			\end{figure}
		\item \vocab{Irrotational vortices}, rotational patterns which impart a rotational velocity $v_\theta = f(r, \theta)$ on the flow, in addition to which there may exist a radial component $v_r$:
			\begin{IEEEeqnarray}{rCl}
				\phi &=& \frac{\Gamma}{2 \pi} \theta \label{eq_phi_irrot_vortex}\\
				\psi &=& -\frac{\Gamma}{2 \pi} \ln r \label{eq_psi_irrot_vortex}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item in which $\Gamma$ (termed \vocab{circulation}) is a constant proportional to the strength of the vortex.
			\end{equationterms}
		\item \vocab{Doublets}, which consist in a source and a sink of equal volume flow rate positioned extremely close one to another (\cref{fig_potential_flow_doublet}):
			\begin{IEEEeqnarray}{rCl}
				\phi &=&  K \frac{\cos \theta}{r}\\
				\psi &=& - K \frac{\sin \theta}{r}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item in which $K$ is a constant proportional to the source/sink volume flow rate $\dot \vol$.
			\end{equationterms}
			\begin{figure}%[ht!]
					\begin{center}
						\includegraphics[height=3cm]{images/potential_flow_uniform_flow} \hspace{1cm}
						\includegraphics[height=3cm]{potential_flow_doublet}
					\end{center}
					\supercaption{Left: a simple uniform steady flow; Right: a \vocab{doublet}, the result of a source and a sink brought very close one to another}{\wcfile{Construction of a potential flow.svg}{Figure} \cczero \oc}
					\label{fig_potential_flow_doublet}
			\end{figure}
		\end{itemize}
		
		It was found in the 17\up{th} Century that combining a doublet with uniform flow resulted in flow patterns that imiated “perfect” flow around a cylinder: a flow where the fluid flows smoothly and steadily everywhere (\cref{fig_potential_flow_cylinder}). The stream function of that flow is:
		\begin{IEEEeqnarray}{rCl}
			\psi &=& U_\infty \sin \theta \left(r - \frac{R^2}{r}\right)
		\end{IEEEeqnarray}
		\begin{figure}
				\begin{center}
					\includegraphics[width=0.7\textwidth]{potential_flow_cylinder}
				\end{center}
				\supercaption{The addition of a \vocab{doublet} and a \vocab{uniform flow} produces streamlines for an (idealized) flow around a cylinder.}{\wcfile{Potential cylinder (colorless).svg}{Figure} \ccbysa by \wcu{Kraaiennest}}
				\label{fig_potential_flow_cylinder}
		\end{figure}


		This stream function allows us to describe the velocity everywhere:
		\begin{IEEEeqnarray}{rCl}
			v_r &=& \frac{1}{r} \partialderivative{\psi}{\theta} = U_\infty \cos \theta \left(1 - \frac{R^2}{r^2}\right)\label{eq_ur_cylinder_nolift}\\
			v_\theta &=& - \partialderivative{\psi}{r} = - U_\infty \sin \theta \left(1 + \frac{R^2}{r^2}\right)\label{eq_utheta_cylinder_nolift}
		\end{IEEEeqnarray}
		
		
		We can even calculate the lift and drag applying on the cylinder surface. Indeed, along the cylinder wall, $r=R$ and
		\begin{IEEEeqnarray}{rCl}
			\left.v_r\right|_{r=R} &=& 0\\
			\left.v_\theta\right|_{r=R} &=& - 2 U_\infty \sin \theta
		\end{IEEEeqnarray}
		Since the Bernoulli equation can be applied along any streamline in this (steady, constant-energy, inviscid, incompressible) flow, we can express the pressure~$p_\text{s}$ on the cylinder surface as a function of $\theta$:
		\begin{IEEEeqnarray}{rCl}
			p_\infty + \frac{1}{2} \rho U_\infty^2 &=& p_\text{s} + \frac{1}{2} \rho v_\theta^2 \nonumber\\
			p_{\text{s}\ (\theta)} &=& p_\infty + \frac{1}{2} \rho \left(U_\infty^2 - v_\theta^2 \right) \label{eq_pressure_surface_cylinder}
		\end{IEEEeqnarray}
		
		Now, a relatively simple integration gives us the net forces exerted by the fluid on the cylinder per unit width $L$, in each of the two directions $x$ and $y$:
		\begin{IEEEeqnarray}{rCCCl}
			\frac{F_{\net, x}}{L} &=& -\int_0^{2\pi} p_\text{s} \ \cos \theta \ R \diff \theta &=& 0\\
			\frac{F_{\net, y}}{L} &=& -\int_0^{2\pi} p_\text{s} \ \sin \theta \ R \diff \theta &=& 0
		\end{IEEEeqnarray}
		
		The results are interesting, and at the time they were obtained by their author, \wed{Jean le Rond d'Alembert}{Jean le Rond D’Alembert}, were devastating: both lift and drag are zero. This inability to reproduce the well-known phenomena of drag is often called the \vocab{d’Alembert paradox}.
		
		To find out why the solution is not realistic, we can plot the resulting surce pressure distribution graphically, and compared to experimental measurements: this is done in \cref{fig_pressure_distribution_cylinder}. Good agreement is obtained on the leading edge of the cylinder; but as the pressure gradient becomes unfavorable, in practice the boundary layer separates –a phenomenon that \emph{cannot} be described with inviscid flow— and a low-pressure area forms on the downstream side of the cylinder.
		\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=0.6\textwidth]{pressure_distribution_cylinders}
				\end{center}
				\supercaption{Pressure distribution (relative to the far-flow pressure) on the surface of a cylinder, with flow from left to right. On the left is the potential flow case, purely symmetrical. On the right (in blue) is a measurement made at a high Reynolds number. Boundary layer separation occurs on the second half of the cylinder, which prevents the recovery of leading-edge pressure values, and increases drag.}{\wcfile{Pressure distribution around a cylinder, with and without friction.svg}{Figure} \ccbysa \wcu{BoH} \& \olivier}
				\label{fig_pressure_distribution_cylinder}
		\end{figure}


	\subsection{Circulating cylinder}
	\label{ch_circulating_cylinder}
	
		
		\youtubethumb{oGeMZ3t8jn4}{watch the Brazilian football team show their French counterparts how circulation (induced through friction by ball rotation) is associated to dynamic lift on a circular body}{TF1, 1997 (\styl)}
		An extremely interesting “hack” can be implemented with the potential cylinder flow above if an irrotational vortex of stream function $\psi = -\frac{\Gamma}{2 \pi} \ln r$ (eq.~\ref{eq_psi_irrot_vortex}) is added to it. The overall flow field becomes:
		\begin{IEEEeqnarray}{rCl}
			\psi &=& U_\infty \sin \theta \left(r - \frac{R^2}{r}\right) - \frac{\Gamma}{2 \pi} \ln r
			%\phi &=& U_\infty \cos \theta \left(r + \frac{R^2}{r}\right) + \frac{\Gamma}{2 \pi} \theta\\
		\end{IEEEeqnarray}
		
		With this function, several key characteristics of the flow field can be obtained. The first is the velocity field at the cylinder surface:
		\begin{IEEEeqnarray}{rCl}
			\left.v_r\right|_{r=R} &=& 0\\
			\left.v_\theta\right|_{r=R} &=& - 2 U_\infty \sin \theta + \frac{\Gamma}{2 \pi R}\label{eq_utheta_rorating_cylinder}
		\end{IEEEeqnarray}
		and we immediately notice that the velocity distribution is no longer symmetrical with respect to the horizontal axis (\cref{fig_lifting_cylinder}): the fluid is deflected, and so there will be a net force on the cylinder.
		\begin{figure}
				\begin{center}
					\includegraphics[width=0.7\textwidth]{rotating_cylinders_white}
				\end{center}
				\supercaption{The addition of an irrotational vortex on top of the cylinder flow described in \cref{fig_potential_flow_cylinder} distorts the flow field and it becomes asymmetrical: a lift force is developed, which depends directly on the circulation $\Gamma$.}{Figure \copyright\xspace White 2008 \cite{white2008}}
				\label{fig_lifting_cylinder}
		\end{figure}
		
		\begin{comment}
		The position of the stagnation points can be determined by setting $v_\theta = 0$ in eq.~\ref{eq_utheta_rorating_cylinder}:
		\begin{IEEEeqnarray}{rCl}
			\theta_{\text{stagnation}} &=& \sin^{-1} \left(\frac{\Gamma}{4 \pi R \ U}\right)
		\end{IEEEeqnarray}
		\end{comment}
		
		This time, the net pressure forces on the cylinder have changed:
		\begin{IEEEeqnarray}{rCCCl}
			\frac{F_{\net, x}}{L} &=& -\int_0^{2\pi} p_\text{s} \ \cos \theta \ R \diff \theta &=& 0\\
			\frac{F_{\net, y}}{L} &=& -\int_0^{2\pi} p_\text{s} \ \sin \theta \ R \diff \theta &=& - \rho \ U_\infty \ \Gamma
		\end{IEEEeqnarray}
		
		We thus find out that \textbf{the drag is once again zero} —as for any potential flow— but that \textbf{lift occurs} which is proportional to the free-stream velocity~$U$ and to the circulation~$\Gamma$.
		
		In practice, such a flow can be generated by spinning a cylindrical object in a uniform flow. A lateral force is then obtained, which can be used as a propulsive or sustaining force. Several boats and even an aircraft have been used in practice to demonstrate this principle. Naturally, flow separation from the cylinder profile and the high shear efforts generated on the surface cause real flows to differ from the ideal case described here, and it turns out that rotating cylinders are a horribly uneconomical and unpractical way of generating lift.



	\subsection{Modeling lift with circulation}

		\youtubethumb{pnbJEg9r1o8}{acting on swimming pool water with a round plate sheds a half-circular vortex that is extremely stable and can be interacted with quite easily. Such stable laminar structures are excellent candidates for analysis using potential flow.}{Physics Girl (Dianna Cowern) (\styl)}
		Fluid flow around cylinders may have little appeal for the modern student of fluid mechanics, but the methodology above has been taken much further. With further mathematical manipulation called \vocab{conformal mapping}, potential flow can be used to described flow around geometrical shapes such as airfoils (\cref{fig_conformal_mapping}). Because the flow around such streamlined bodies usually does not feature boundary layer separation, the predicted flow fields everywhere except in the close vicinity of the solid surface are accurately predicted.
		\begin{figure}
				\begin{center}
					\includegraphics[width=0.8\textwidth]{conformal_mapping_1}
					\vspace{1cm}\\
					\includegraphics[width=0.8\textwidth]{conformal_mapping_2}
				\end{center}
				\supercaption{Potential flow around an airfoil without (top) and with (bottom) circulation. Much like potential flow around a cylinder, potential flow around an airfoil can only result in a net vertical force if an irrotational vortex (with circulation~$\Gamma$) is added on top of the flow. Only one value for~$\Gamma$ will generate a realistic flow, with the rear stagnation point coinciding with the trailing edge, a occurrence named \vocab{Kutta condition}.}{\wcfile{Examples of potential flow modelling.svg}{Figure} \cczero \oc}
				\label{fig_conformal_mapping}
		\end{figure}
		
		There again, it is observed that regardless of the constructed geometry, no lift can be modeled unless circulation is also added within the flow. The amount of circulation needed so that results may correspond to experimental observations is found by increasing it progressively until the the rear stagnation point reaches the rear trailing edge of the airfoil, a condition known as the \vocab{Kutta condition}. Regardless of the amount of circulation added, potential flow remains entirely reversible, both in a kinematic and a thermodynamic sense, thus, care must be taken in the problem setup to make sure the model is realistic (\cref{fig_conformal_mapping_reversibility}).
		\begin{figure}
				\begin{center}
					\includegraphics[width=0.4\textwidth]{conformal_mapping_3}\hspace{1cm}
					\includegraphics[width=0.4\textwidth]{conformal_mapping_4}
				\end{center}
				\supercaption{Potential flow allows all velocities to be inverted without any change in the flow geometry. Here the flow around an airfoil is reversed, displaying unphysical behavior.}{\wcfile{Examples of potential flow modelling.svg}{Figure} \cczero \oc}
				\label{fig_conformal_mapping_reversibility}
		\end{figure}
		
				It is then observed in general that \emph{any} dynamic lift generation can be modeled as the superposition of a free-stream flow and a circulation effect (\cref{fig_airfoil_velocities}).
		With such a tool, potential flow becomes an extremely useful tool, mathematically and computationally inexpensive, in order to model and understand the cause and effect of dynamic lift in fluid mechanics. In particular, it has been paramount in the description of aerodynamic lift distribution over aircraft wing surfaces (\cref{fig_lifting_line_theory}), with a concept called the \we{Lifting-line theory}.
		\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=0.4\textwidth]{Velocity_relative_to_airfoil}\hspace{0.5cm}
					\includegraphics[width=0.4\textwidth]{Velocity_relative_to_ground}
				\end{center}
				\supercaption{A numerical model of flow around an airfoil. In the left figure, the velocity vectors are represented relative to a stationary background. In the right figure, the velocity of the free-stream flow has been subtracted from each vector, bringing the circulation phenomenon into evidence.}{\wcfile{Velocity relative to ground.png}{Figures 1} \& \wcfile{Velocity relative to airfoil.png}{2} \ccbysa by \weu{Crowsnest}}
				\label{fig_airfoil_velocities}
		\end{figure}
		
		\begin{figure}
				\begin{center}
					\includegraphics[width=\textwidth]{lift_distribution_2}\vspace{0.5cm}\\
					\includegraphics[width=\textwidth]{lift_distribution_3}\vspace{0.5cm}\\
					\includegraphics[width=\textwidth]{lift_distribution_4}
				\end{center}
				\supercaption{From top to bottom, the lift distribution over the wings of a glider is modeled with increasingly complex (and accurate) lift and circulation distributions along the span. The \we{Lifting-line theory} is a method associating each element of lift with a certain amount of circulation. The effect of each span-wise change of circulation is then mapped onto the flow field as a trailing vortex.}{\wcfile{Aircraft wing lift distribution showing trailing vortices (1).svg}{Figures 1}, \wcfile{Aircraft wing lift distribution showing trailing vortices (2).svg}{2} \& \wcfile{Aircraft wing lift distribution showing trailing vortices (3).svg}{3} \ccbysa \olivier}
				\label{fig_lifting_line_theory}
		\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Flow at very small scales}
\label{ch_creeping_flow}
	
	At the complete opposite of the spectrum, we find flow at very small scales: flows around bacteria, dust particles, and inside very small ducts. In those flows the representative length $L$ is extremely small, which makes for small values of the Reynolds number. Such flows are termed \vocab{creeping} or \vocab{Stokes flows}. What are their main characteristics?
	
	Looking back once again at the non-dimensional Navier-Stokes equation for incompressible flow derived as eq.~\ref{eq_ns_nondim} p.~\pageref{eq_ns_nondim},
	\begin{IEEEeqnarray}{rCl}
				\str \ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^* 	& = &	\frac{1}{\fr^2}\ \vec g^* - \eu\ \gradient^* p^* + \frac{1}{\re}\ \gradient^{*2} \vec V^*\label{eq_ns_nondim_two}
	\end{IEEEeqnarray}
	we see that creeping flow will occur when the Reynolds number is much smaller than 1. The relative weight of the term $(1/{\re})\ \gradient^{*2} \vec V^*$ then becomes overwhelming.

	In addition to cases where $\re \ll 1$, we focus our interest on flows for which:
		\begin{itemize}
			\item gravitational effects have negligible influence over the velocity field;
			\item the characteristic frequency is extremely low (quasi-steady flow).
		\end{itemize}

	With these characteristics, the terms associated with the $\str$ (Strouhal) and $\fr$ (Froude) numbers become very small with respect to the other terms, and our non-dimensionalized Navier-Stokes equation (eq.~\ref{eq_ns_nondim_two}) is approximately reduced~to:
		\begin{IEEEeqnarray}{rCl}
				\vec 0	& \approx & - \eu \ \gradient^* p^* + \frac{1}{\re}\ \gradient^{*2} \vec V^*
		\end{IEEEeqnarray}
	
	We can now come back to dimensionalized equations, concluding that for a fluid flow dominated by viscosity, the pressure and velocity fields are linked together by the approximate relation:
		\begin{IEEEeqnarray}{rCl}
				\gradient{p}  &=&  \mu \laplacian{\vec V} \label{eq_stokes}
		\end{IEEEeqnarray}

	In this type of flow, the pressure field is entirely dictated by the Laplacian of velocity, and the fluid density has no importance. Micro-organisms, for which the representative length $L$ is very small, spend their lives in such flows (\cref{fig_microorganisms}). At the human scale, we can visualize the effects of these flows by moving an object slowly in highly-viscous fluids (\eg a spoon in honey), or by swimming in a pool filled with plastic balls. The inertial effects are almost inexistent, drag is extremely important, and the object geometry has comparatively small influence.

	\begin{figure}
		\begin{center}
			\includegraphics[width=0.35\textwidth]{EMpylori}
		\end{center}
		\supercaption{Micro-organisms carry themselves through fluids at extremely low Reynolds numbers, since their scale~$L$ is very small. For them, viscosity effects dominate inertial effects.}{\wcfile{EMpylori.jpg}{Photo} by Yutaka Tsutsumi, M.D., Fujita Health University School of Medicine}
		\label{fig_microorganisms}
	\end{figure}
	

	In 1851, \we{George Gabriel Stokes} worked through equation~\ref{eq_stokes} for flow around a sphere, and obtained an analytical solution for the flow field. This allowed him to show that the drag $F_\text{D sphere}$ applying on a sphere of diameter $D$ in creeping flow (\cref{fig_sphere_creeping_flow}) is:
		\begin{IEEEeqnarray}{rCl}
				F_\text{D sphere} &=& 3 \pi \mu U_\infty D \label{eq_drag_creeping_sphere}
		\end{IEEEeqnarray}
	
	Inserting this equation~\ref{eq_drag_creeping_sphere} into the definition of the drag coefficient $C_{F \D} \equiv F_\D/{\frac{1}{2} \rho S_\text{frontal} U_\infty^2}$ (from eq.~\ref{eq_def_force_coefficient} p.~\pageref{eq_def_force_coefficient}) then yields:
	\begin{IEEEeqnarray}{rCl}
			C_{F \D} = \frac{F_\text{D sphere}}{\frac{1}{2} \rho U_\infty^2 \frac{\pi}{4} D^2} & = & \frac{24 \mu}{\rho \ U_\infty D} = \frac{\num{24}}{\reD}
	\end{IEEEeqnarray}

	\begin{figure}
		\begin{center}
			\includegraphics[width=0.6\textwidth]{stokes_flow_sphere_streamlines}
		\end{center}
		\supercaption{Flow at very low Reynolds numbers around a sphere. In this regime, the drag force is proportional to the velocity.}{\wcfile{Flow patterns around a sphere at very low Reynolds numbers.svg}{Figure} \ccbysa by \olivier\ \& \wcu{Kraaiennest}}
		\label{fig_sphere_creeping_flow}
	\end{figure}

	These equations are specific to flow around spheres, but the trends they describe apply well to most bodies evolving in highly-viscous flows, such as dust or liquid particles traveling through the atmosphere. Drag is only proportional to the speed (as opposed to low-viscosity flows in which it grows with velocity \emph{squared}), and it does not depend on fluid density.

\atendofchapternotes

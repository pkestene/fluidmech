\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\contentsline {chapter}{Contents}{2}{section*.1}%
\contentsline {chapter}{About this course (syllabus)}{6}{section*.2}%
\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Basic flow quantities}{9}{chapter.1}%
\contentsline {section}{\numberline {1.1}Concept of a fluid}{9}{section.1.1}%
\contentsline {section}{\numberline {1.2}Fluid dynamics}{9}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Solution of a flow}{9}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Modeling of fluids}{10}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Theory, numerics, and experiment}{11}{subsection.1.2.3}%
\contentsline {section}{\numberline {1.3}Important concepts in mechanics}{12}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Position, velocity, acceleration}{12}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Forces and moments}{12}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Energy}{13}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Properties of fluids}{14}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Density}{14}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {1.4.2}Phase}{14}{subsection.1.4.2}%
\contentsline {subsection}{\numberline {1.4.3}Temperature}{15}{subsection.1.4.3}%
\contentsline {subsection}{\numberline {1.4.4}Perfect gas model}{15}{subsection.1.4.4}%
\contentsline {subsection}{\numberline {1.4.5}Speed of sound}{16}{subsection.1.4.5}%
\contentsline {subsection}{\numberline {1.4.6}Viscosity}{17}{subsection.1.4.6}%
\contentsline {section}{\numberline {1.5}Forces on fluids}{18}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Gravity}{18}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Pressure}{18}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {1.5.3}Shear}{18}{subsection.1.5.3}%
\contentsline {section}{\numberline {1.6}Basic flow quantities}{19}{section.1.6}%
\contentsline {section}{\numberline {1.7}Four balance equations}{20}{section.1.7}%
\contentsline {section}{\numberline {1.8}Classification of fluid flows}{21}{section.1.8}%
\contentsline {section}{\numberline {1.9}Limits of fluid dynamics}{23}{section.1.9}%
\contentsline {section}{\numberline {1.10}Solved problems}{24}{section.1.10}%
\contentsline {section}{\numberline {1.11}Problems}{27}{section.1.11}%
\contentsline {subsubsection}{\numberline {1.1}Reading quiz}{27}{subsubsection.1.11.0.1}%
\contentsline {subsubsection}{\numberline {1.2}Compressibility effects}{27}{subsubsection.1.11.0.2}%
\contentsline {subsubsection}{\numberline {1.3}Pressure-induced force}{27}{subsubsection.1.11.0.3}%
\contentsline {subsubsection}{\numberline {1.4}Shear-induced force}{28}{subsubsection.1.11.0.4}%
\contentsline {subsubsection}{\numberline {1.5}Speed of sound}{28}{subsubsection.1.11.0.5}%
\contentsline {subsubsection}{\numberline {1.6}Wind on a truck}{28}{subsubsection.1.11.0.6}%
\contentsline {subsubsection}{\numberline {1.7}Go-faster exhaust pipe}{29}{subsubsection.1.11.0.7}%
\contentsline {subsubsection}{\numberline {1.8}Acceleration of a particle}{30}{subsubsection.1.11.0.8}%
\contentsline {subsubsection}{\numberline {1.9}Flow classifications}{30}{subsubsection.1.11.0.9}%
\contentsline {chapter}{\numberline {2}Analysis of existing flows with\nobreakspace {}one\nobreakspace {}dimension}{33}{chapter.2}%
\contentsline {section}{\numberline {2.1}Motivation}{33}{section.2.1}%
\contentsline {section}{\numberline {2.2}One-dimensional flow problems}{33}{section.2.2}%
\contentsline {section}{\numberline {2.3}Balance of mass}{35}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Mass balance equation}{35}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Problems with the mass balance equation}{36}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Balance of momentum}{37}{section.2.4}%
\contentsline {section}{\numberline {2.5}Balance of energy}{39}{section.2.5}%
\contentsline {section}{\numberline {2.6}The Bernoulli equation}{41}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Theory}{41}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}Reality}{42}{subsection.2.6.2}%
\contentsline {section}{\numberline {2.7}Solved problems}{44}{section.2.7}%
\contentsline {section}{\numberline {2.8}Problems}{45}{section.2.8}%
\contentsline {subsubsection}{\numberline {2.1}Reading quiz}{45}{subsubsection.2.8.0.1}%
\contentsline {subsubsection}{\numberline {2.2}Pipe expansion without losses}{45}{subsubsection.2.8.0.2}%
\contentsline {subsubsection}{\numberline {2.3}Pipe flow with losses}{46}{subsubsection.2.8.0.3}%
\contentsline {subsubsection}{\numberline {2.4}Combustor from a jet engine}{46}{subsubsection.2.8.0.4}%
\contentsline {subsubsection}{\numberline {2.5}Water jet on a truck}{47}{subsubsection.2.8.0.5}%
\contentsline {subsubsection}{\numberline {2.6}High-speed gas flow}{48}{subsubsection.2.8.0.6}%
\contentsline {chapter}{\numberline {3}Analysis of existing flows with\nobreakspace {}three\nobreakspace {}dimensions}{51}{chapter.3}%
\contentsline {section}{\numberline {3.1}Motivation}{51}{section.3.1}%
\contentsline {section}{\numberline {3.2}The Reynolds transport theorem}{51}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Control volume}{51}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Rate of change of an additive property}{52}{subsection.3.2.2}%
\contentsline {section}{\numberline {3.3}Balance of mass}{54}{section.3.3}%
\contentsline {section}{\numberline {3.4}Balance of momentum}{56}{section.3.4}%
\contentsline {section}{\numberline {3.5}Balance of angular momentum}{57}{section.3.5}%
\contentsline {section}{\numberline {3.6}Balance of energy}{59}{section.3.6}%
\contentsline {section}{\numberline {3.7}Limits of integral analysis}{59}{section.3.7}%
\contentsline {section}{\numberline {3.8}Solved problems}{60}{section.3.8}%
\contentsline {section}{\numberline {3.9}Problems}{63}{section.3.9}%
\contentsline {subsubsection}{\numberline {3.1}Reading quiz}{63}{subsubsection.3.9.0.1}%
\contentsline {subsubsection}{\numberline {3.2}Pipe bend}{63}{subsubsection.3.9.0.2}%
\contentsline {subsubsection}{\numberline {3.3}Exhaust gas deflector}{63}{subsubsection.3.9.0.3}%
\contentsline {subsubsection}{\numberline {3.4}Pelton water turbine}{64}{subsubsection.3.9.0.4}%
\contentsline {subsubsection}{\numberline {3.5}Snow plow}{65}{subsubsection.3.9.0.5}%
\contentsline {subsubsection}{\numberline {3.6}Inlet of a pipe}{65}{subsubsection.3.9.0.6}%
\contentsline {subsubsection}{\numberline {3.7}Drag on a cylindrical profile}{66}{subsubsection.3.9.0.7}%
\contentsline {subsubsection}{\numberline {3.8}Drag on a flat plate}{68}{subsubsection.3.9.0.8}%
\contentsline {subsubsection}{\numberline {3.9}Drag measurements in a wind tunnel}{69}{subsubsection.3.9.0.9}%
\contentsline {subsubsection}{\numberline {3.10}Moment on gas deflector}{70}{subsubsection.3.9.0.10}%
\contentsline {subsubsection}{\numberline {3.11}Helicopter tail moment}{70}{subsubsection.3.9.0.11}%
\contentsline {chapter}{\numberline {4}Effects of pressure}{73}{chapter.4}%
\contentsline {section}{\numberline {4.1}Motivation}{73}{section.4.1}%
\contentsline {section}{\numberline {4.2}Pressure forces on walls}{73}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Magnitude of the pressure force}{73}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Position of the pressure force}{74}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}Pressure fields in fluids}{75}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}The direction of pressure}{75}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Pressure on an infinitesimal volume}{76}{subsection.4.3.2}%
\contentsline {section}{\numberline {4.4}Special case: pressure in static fluids}{79}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Forces in static fluids}{79}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Pressure and depth}{80}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}Buoyancy}{82}{subsection.4.4.3}%
\contentsline {section}{\numberline {4.5}Solved problems}{83}{section.4.5}%
\contentsline {section}{\numberline {4.6}Problems}{85}{section.4.6}%
\contentsline {subsubsection}{\numberline {4.1}Reading quiz}{85}{subsubsection.4.6.0.1}%
\contentsline {subsubsection}{\numberline {4.2}Pressure in a static fluid}{85}{subsubsection.4.6.0.2}%
\contentsline {subsubsection}{\numberline {4.3}Pressure measurement with a U-tube}{85}{subsubsection.4.6.0.3}%
\contentsline {subsubsection}{\numberline {4.4}Straight water tank door}{86}{subsubsection.4.6.0.4}%
\contentsline {subsubsection}{\numberline {4.5}Access door on a water channel wall}{86}{subsubsection.4.6.0.5}%
\contentsline {subsubsection}{\numberline {4.6}Pressure force on a cylinder}{87}{subsubsection.4.6.0.6}%
\contentsline {subsubsection}{\numberline {4.7}Buoyancy of a barge}{88}{subsubsection.4.6.0.7}%
\contentsline {subsubsection}{\numberline {4.8}Atmospheric pressure distribution}{88}{subsubsection.4.6.0.8}%
\contentsline {chapter}{\numberline {5}Effects of shear}{91}{chapter.5}%
\contentsline {section}{\numberline {5.1}Motivation}{91}{section.5.1}%
\contentsline {section}{\numberline {5.2}Shear forces on walls}{91}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Magnitude of the shear force}{91}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Direction and position of the shear force}{92}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Shear fields in fluids}{92}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}The direction of shear}{93}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Shear on an infinitesimal volume}{93}{subsection.5.3.2}%
\contentsline {section}{\numberline {5.4}Resistance to shear: viscosity}{97}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Viscosity}{97}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}Kinematic viscosity}{98}{subsection.5.4.2}%
\contentsline {subsection}{\numberline {5.4.3}Turbulent viscosity}{99}{subsection.5.4.3}%
\contentsline {subsection}{\numberline {5.4.4}Non-Newtonian fluids}{100}{subsection.5.4.4}%
\contentsline {subsection}{\numberline {5.4.5}The no-slip condition}{100}{subsection.5.4.5}%
\contentsline {section}{\numberline {5.5}Special case: shear in simple laminar flows}{101}{section.5.5}%
\contentsline {section}{\numberline {5.6}Solved problems}{103}{section.5.6}%
\contentsline {section}{\numberline {5.7}Problems}{105}{section.5.7}%
\contentsline {subsubsection}{\numberline {5.1}Quiz}{106}{subsubsection.5.7.0.1}%
\contentsline {subsubsection}{\numberline {5.2}Flow in between two plates}{106}{subsubsection.5.7.0.2}%
\contentsline {subsubsection}{\numberline {5.3}Friction on a plate}{107}{subsubsection.5.7.0.3}%
\contentsline {subsubsection}{\numberline {5.4}Viscometer}{107}{subsubsection.5.7.0.4}%
\contentsline {subsubsection}{\numberline {5.5}Boundary layer}{107}{subsubsection.5.7.0.5}%
\contentsline {subsubsection}{\numberline {5.6}Clutch}{109}{subsubsection.5.7.0.6}%
\contentsline {chapter}{\numberline {6}Prediction of fluid flows}{111}{chapter.6}%
\contentsline {section}{\numberline {6.1}Motivation}{111}{section.6.1}%
\contentsline {section}{\numberline {6.2}Organizing calculations}{112}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Problem description}{112}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}The total time derivative}{112}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}Equations for all flows}{116}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Balance of mass}{116}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}Balance of linear momentum}{118}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Balance of energy}{120}{subsection.6.3.3}%
\contentsline {subsection}{\numberline {6.3.4}Other terms and equations}{121}{subsection.6.3.4}%
\contentsline {subsection}{\numberline {6.3.5}Interlude: where does this leave us?}{122}{subsection.6.3.5}%
\contentsline {section}{\numberline {6.4}Equations for incompressible flow}{123}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Balance of mass}{123}{subsection.6.4.1}%
\contentsline {subsection}{\numberline {6.4.2}Balance of linear momentum}{124}{subsection.6.4.2}%
\contentsline {subsection}{\numberline {6.4.3}The Bernoulli equation (again)}{127}{subsection.6.4.3}%
\contentsline {section}{\numberline {6.5}CFD: the Navier-Stokes equations in practice}{127}{section.6.5}%
\contentsline {section}{\numberline {6.6}Solved problems}{128}{section.6.6}%
\contentsline {section}{\numberline {6.7}Problems}{131}{section.6.7}%
\contentsline {subsubsection}{\numberline {6.1}Quiz}{131}{subsubsection.6.7.0.1}%
\contentsline {subsubsection}{\numberline {6.2}Revision questions}{131}{subsubsection.6.7.0.2}%
\contentsline {subsubsection}{\numberline {6.3}Acceleration field}{131}{subsubsection.6.7.0.3}%
\contentsline {subsubsection}{\numberline {6.4}Volumetric dilatation rate}{132}{subsubsection.6.7.0.4}%
\contentsline {subsubsection}{\numberline {6.5}Incompressibility}{132}{subsubsection.6.7.0.5}%
\contentsline {subsubsection}{\numberline {6.6}Missing components}{132}{subsubsection.6.7.0.6}%
\contentsline {subsubsection}{\numberline {6.7}Another acceleration field}{132}{subsubsection.6.7.0.7}%
\contentsline {subsubsection}{\numberline {6.8}Vortex}{132}{subsubsection.6.7.0.8}%
\contentsline {subsubsection}{\numberline {6.9}Pressure fields}{133}{subsubsection.6.7.0.9}%
\contentsline {chapter}{\numberline {7}Pipe flows}{135}{chapter.7}%
\contentsline {section}{\numberline {7.1}Motivation}{135}{section.7.1}%
\contentsline {section}{\numberline {7.2}Frictionless flow in pipes}{135}{section.7.2}%
\contentsline {section}{\numberline {7.3}Parameters to quantify losses in pipes}{137}{section.7.3}%
\contentsline {section}{\numberline {7.4}Laminar flow in pipes}{137}{section.7.4}%
\contentsline {subsection}{\numberline {7.4.1}Laminar flow between plates}{137}{subsection.7.4.1}%
\contentsline {subsection}{\numberline {7.4.2}Laminar flow in pipes}{139}{subsection.7.4.2}%
\contentsline {section}{\numberline {7.5}Turbulent flow in pipes}{142}{section.7.5}%
\contentsline {subsection}{\numberline {7.5.1}When is a pipe flow turbulent?}{142}{subsection.7.5.1}%
\contentsline {subsection}{\numberline {7.5.2}Characteristics of turbulent flow}{143}{subsection.7.5.2}%
\contentsline {subsection}{\numberline {7.5.3}Velocity profile in turbulent pipe flow}{144}{subsection.7.5.3}%
\contentsline {subsection}{\numberline {7.5.4}Pressure losses in turbulent pipe flow}{144}{subsection.7.5.4}%
\contentsline {section}{\numberline {7.6}Engineer’s guide to pipe flows}{146}{section.7.6}%
\contentsline {subsection}{\numberline {7.6.1}Summary so far}{146}{subsection.7.6.1}%
\contentsline {subsection}{\numberline {7.6.2}Choosing laminar or turbulent flow}{146}{subsection.7.6.2}%
\contentsline {subsection}{\numberline {7.6.3}Pressure losses in laminar flow}{147}{subsection.7.6.3}%
\contentsline {subsection}{\numberline {7.6.4}Pressure losses in turbulent flow}{147}{subsection.7.6.4}%
\contentsline {subsection}{\numberline {7.6.5}Calculating pumping and turbining power}{148}{subsection.7.6.5}%
\contentsline {section}{\numberline {7.7}Solved problems}{148}{section.7.7}%
\contentsline {section}{\numberline {7.8}Problems}{151}{section.7.8}%
\contentsline {subsubsection}{\numberline {7.1}Reading quiz}{151}{subsubsection.7.8.0.1}%
\contentsline {subsubsection}{\numberline {7.2}Revision questions}{153}{subsubsection.7.8.0.2}%
\contentsline {subsubsection}{\numberline {7.3}Air flow in a small pipe}{153}{subsubsection.7.8.0.3}%
\contentsline {subsubsection}{\numberline {7.4}Water piping}{153}{subsubsection.7.8.0.4}%
\contentsline {subsubsection}{\numberline {7.5}Design of a water piping system}{153}{subsubsection.7.8.0.5}%
\contentsline {subsubsection}{\numberline {7.6}Major oil pipeline}{155}{subsubsection.7.8.0.6}%
\contentsline {subsubsection}{\numberline {7.7}Pump with pipe expansion}{156}{subsubsection.7.8.0.7}%
\contentsline {subsubsection}{\numberline {7.8}Piping and power of a water turbine}{156}{subsubsection.7.8.0.8}%
\contentsline {subsubsection}{\numberline {7.9}Politically incorrect fluid mechanics}{157}{subsubsection.7.8.0.9}%
\contentsline {chapter}{\numberline {8}Engineering models}{161}{chapter.8}%
\contentsline {section}{\numberline {8.1}Motivation}{161}{section.8.1}%
\contentsline {section}{\numberline {8.2}Comparing influences: the weighted momentum balance}{161}{section.8.2}%
\contentsline {subsection}{\numberline {8.2.1}Principle}{161}{subsection.8.2.1}%
\contentsline {subsection}{\numberline {8.2.2}The non-dimensional Navier-Stokes equation}{162}{subsection.8.2.2}%
\contentsline {subsection}{\numberline {8.2.3}The flow parameters of Navier-Stokes}{164}{subsection.8.2.3}%
\contentsline {subsection}{\numberline {8.2.4}Flow parameters obtained as force ratios}{166}{subsection.8.2.4}%
\contentsline {subsection}{\numberline {8.2.5}The Reynolds number in practice}{166}{subsection.8.2.5}%
\contentsline {section}{\numberline {8.3}Making models}{167}{section.8.3}%
\contentsline {section}{\numberline {8.4}Comparing results: coefficients}{169}{section.8.4}%
\contentsline {subsection}{\numberline {8.4.1}Principle}{169}{subsection.8.4.1}%
\contentsline {subsection}{\numberline {8.4.2}Force coefficients}{169}{subsection.8.4.2}%
\contentsline {subsection}{\numberline {8.4.3}Power coefficient, and other coefficients}{170}{subsection.8.4.3}%
\contentsline {subsection}{\numberline {8.4.4}Non-dimensionalizing all the problems}{171}{subsection.8.4.4}%
\contentsline {section}{\numberline {8.5}Solved problems}{172}{section.8.5}%
\contentsline {section}{\numberline {8.6}Problems}{173}{section.8.6}%
\contentsline {subsubsection}{\numberline {8.1}Reading quiz}{173}{subsubsection.8.6.0.1}%
\contentsline {subsubsection}{\numberline {8.2}Scaling a golf ball}{174}{subsubsection.8.6.0.2}%
\contentsline {subsubsection}{\numberline {8.3}Fluid mechanics of a giant airliner}{175}{subsubsection.8.6.0.3}%
\contentsline {subsubsection}{\numberline {8.4}Scale effects on a dragonfly}{176}{subsubsection.8.6.0.4}%
\contentsline {subsubsection}{\numberline {8.5}Formula One testing}{176}{subsubsection.8.6.0.5}%
\contentsline {chapter}{\numberline {9}Dealing with\nobreakspace {}turbulence}{179}{chapter.9}%
\contentsline {section}{\numberline {9.1}Motivation}{179}{section.9.1}%
\contentsline {section}{\numberline {9.2}Recognizing turbulence}{180}{section.9.2}%
\contentsline {subsection}{\numberline {9.2.1}A brief definition}{180}{subsection.9.2.1}%
\contentsline {subsection}{\numberline {9.2.2}Chaos, not randomness}{180}{subsection.9.2.2}%
\contentsline {subsection}{\numberline {9.2.3}Growth and decay}{181}{subsection.9.2.3}%
\contentsline {subsection}{\numberline {9.2.4}A cascade of vortices}{182}{subsection.9.2.4}%
\contentsline {subsection}{\numberline {9.2.5}Not turbulence}{182}{subsection.9.2.5}%
\contentsline {section}{\numberline {9.3}The effects of turbulence}{183}{section.9.3}%
\contentsline {subsection}{\numberline {9.3.1}Dissipation (losses)}{183}{subsection.9.3.1}%
\contentsline {subsection}{\numberline {9.3.2}Main flow patterns}{183}{subsection.9.3.2}%
\contentsline {subsection}{\numberline {9.3.3}Mixing}{184}{subsection.9.3.3}%
\contentsline {section}{\numberline {9.4}Quantifying turbulence}{184}{section.9.4}%
\contentsline {subsection}{\numberline {9.4.1}Average and fluctuation}{184}{subsection.9.4.1}%
\contentsline {subsection}{\numberline {9.4.2}Turbulence intensity}{185}{subsection.9.4.2}%
\contentsline {subsection}{\numberline {9.4.3}The size of eddies}{185}{subsection.9.4.3}%
\contentsline {subsection}{\numberline {9.4.4}Turbulent kinetic energy and dissipation rate}{186}{subsection.9.4.4}%
\contentsline {subsection}{\numberline {9.4.5}Turbulence anisotropy and inhomogeneity}{188}{subsection.9.4.5}%
\contentsline {section}{\numberline {9.5}Computing turbulent flow}{189}{section.9.5}%
\contentsline {subsection}{\numberline {9.5.1}Basic premise}{189}{subsection.9.5.1}%
\contentsline {subsection}{\numberline {9.5.2}Accounting for turbulence}{189}{subsection.9.5.2}%
\contentsline {section}{\numberline {9.6}Commented bibliography}{190}{section.9.6}%
\contentsline {section}{\numberline {9.7}Problems}{193}{section.9.7}%
\contentsline {subsubsection}{\numberline {9.1}Hypothetical flow}{194}{subsubsection.9.7.0.1}%
\contentsline {subsubsection}{\numberline {9.2}Turbulent channel flow}{194}{subsubsection.9.7.0.2}%
\contentsline {subsubsection}{\numberline {9.3}Cumulus cloud}{195}{subsubsection.9.7.0.3}%
\contentsline {subsubsection}{\numberline {9.4}Reactor tank}{195}{subsubsection.9.7.0.4}%
\contentsline {chapter}{\numberline {10}Flow near walls}{199}{chapter.10}%
\contentsline {section}{\numberline {10.1}Motivation}{199}{section.10.1}%
\contentsline {section}{\numberline {10.2}The concept of boundary layer}{199}{section.10.2}%
\contentsline {subsection}{\numberline {10.2.1}Rationale}{199}{subsection.10.2.1}%
\contentsline {subsection}{\numberline {10.2.2}Why do we study the boundary layer?}{201}{subsection.10.2.2}%
\contentsline {subsection}{\numberline {10.2.3}Characterization of the boundary layer}{201}{subsection.10.2.3}%
\contentsline {section}{\numberline {10.3}Laminar boundary layers}{203}{section.10.3}%
\contentsline {subsection}{\numberline {10.3.1}Governing equations}{203}{subsection.10.3.1}%
\contentsline {subsection}{\numberline {10.3.2}Blasius’ solution}{204}{subsection.10.3.2}%
\contentsline {section}{\numberline {10.4}Boundary layer transition}{205}{section.10.4}%
\contentsline {section}{\numberline {10.5}Turbulent boundary layers}{206}{section.10.5}%
\contentsline {section}{\numberline {10.6}Flow separation}{207}{section.10.6}%
\contentsline {section}{\numberline {10.7}Solved problems}{210}{section.10.7}%
\contentsline {section}{\numberline {10.8}Problems}{213}{section.10.8}%
\contentsline {subsubsection}{\numberline {10.1}Water and air flow}{213}{subsubsection.10.8.0.1}%
\contentsline {subsubsection}{\numberline {10.2}Boundary layer sketches}{214}{subsubsection.10.8.0.2}%
\contentsline {subsubsection}{\numberline {10.3}Shear force due to boundary layer}{215}{subsubsection.10.8.0.3}%
\contentsline {subsubsection}{\numberline {10.4}Wright Flyer I}{215}{subsubsection.10.8.0.4}%
\contentsline {subsubsection}{\numberline {10.5}Power lost to shear on an airliner fuselage}{215}{subsubsection.10.8.0.5}%
\contentsline {subsubsection}{\numberline {10.6}Laminar wing profile}{216}{subsubsection.10.8.0.6}%
\contentsline {subsubsection}{\numberline {10.7}Separation mechanism}{216}{subsubsection.10.8.0.7}%
\contentsline {chapter}{\numberline {11}Large- and small-scale\nobreakspace {}flows}{221}{chapter.11}%
\contentsline {section}{\numberline {11.1}Motivation}{221}{section.11.1}%
\contentsline {section}{\numberline {11.2}Flow at large scales}{221}{section.11.2}%
\contentsline {subsection}{\numberline {11.2.1}Problem statement}{221}{subsection.11.2.1}%
\contentsline {subsection}{\numberline {11.2.2}Investigation of inviscid flows}{222}{subsection.11.2.2}%
\contentsline {section}{\numberline {11.3}Plotting velocity with functions}{223}{section.11.3}%
\contentsline {subsection}{\numberline {11.3.1}Kinematics that fit conservation laws}{223}{subsection.11.3.1}%
\contentsline {subsection}{\numberline {11.3.2}Strengths and weaknesses of potential flow}{224}{subsection.11.3.2}%
\contentsline {subsection}{\numberline {11.3.3}Superposition: the lifting cylinder}{225}{subsection.11.3.3}%
\contentsline {subsection}{\numberline {11.3.4}Circulating cylinder}{227}{subsection.11.3.4}%
\contentsline {subsection}{\numberline {11.3.5}Modeling lift with circulation}{229}{subsection.11.3.5}%
\contentsline {section}{\numberline {11.4}Flow at very small scales}{232}{section.11.4}%
\contentsline {section}{\numberline {11.5}Problems}{235}{section.11.5}%
\contentsline {subsubsection}{\numberline {11.1}Volcanic ash from the Eyjafjallajökull}{235}{subsubsection.11.5.0.1}%
\contentsline {subsubsection}{\numberline {11.2}Water drop}{235}{subsubsection.11.5.0.2}%
\contentsline {subsubsection}{\numberline {11.3}Idealized flow over a hangar roof}{235}{subsubsection.11.5.0.3}%
\contentsline {subsubsection}{\numberline {11.4}Cabling of the Wright Flyer}{237}{subsubsection.11.5.0.4}%
\contentsline {subsubsection}{\numberline {11.5}Ping pong ball}{237}{subsubsection.11.5.0.5}%
\contentsline {subsubsection}{\numberline {11.6}Flow field of a tornado}{240}{subsubsection.11.5.0.6}%
\contentsline {subsubsection}{\numberline {11.7}Lift on a symmetrical object}{242}{subsubsection.11.5.0.7}%
\contentsline {subsubsection}{\numberline {11.8}Air flow over a wing profile}{242}{subsubsection.11.5.0.8}%
\contentsline {chapter}{Appendix}{245}{section*.143}%
\contentsline {section}{\numberline {A1}Notation}{246}{section.Alph0.1}%
\contentsline {section}{\numberline {A2}Vector operations}{247}{section.Alph0.2}%
\contentsline {subsection}{\numberline {A2.1}Vector dot product}{247}{subsection.Alph0.2.1}%
\contentsline {subsection}{\numberline {A2.2}Vector cross product}{247}{subsection.Alph0.2.2}%
\contentsline {section}{\numberline {A3}Field operators}{250}{section.Alph0.3}%
\contentsline {subsection}{\numberline {A3.1}Gradient}{250}{subsection.Alph0.3.1}%
\contentsline {subsection}{\numberline {A3.2}Divergent}{250}{subsection.Alph0.3.2}%
\contentsline {subsection}{\numberline {A3.3}Advective}{251}{subsection.Alph0.3.3}%
\contentsline {subsection}{\numberline {A3.4}Laplacian}{251}{subsection.Alph0.3.4}%
\contentsline {subsection}{\numberline {A3.5}Curl}{252}{subsection.Alph0.3.5}%
\contentsline {section}{\numberline {A4}Derivations of the Bernoulli equation}{253}{section.Alph0.4}%
\contentsline {subsection}{\numberline {A4.1}The Bernoulli equation from the energy equation}{253}{subsection.Alph0.4.1}%
\contentsline {subsection}{\numberline {A4.2}The Bernoulli equation from the integral momentum equation}{253}{subsection.Alph0.4.2}%
\contentsline {subsection}{\numberline {A4.3}The Bernoulli equation from the Navier-Stokes equation}{253}{subsection.Alph0.4.3}%
\contentsline {section}{\numberline {A5}Flow parameters as force ratios}{256}{section.Alph0.5}%
\contentsline {subsection}{\numberline {A5.1}Acceleration vs. viscous forces: the Reynolds number}{256}{subsection.Alph0.5.1}%
\contentsline {subsection}{\numberline {A5.2}Acceleration vs. gravity force: the Froude number}{257}{subsection.Alph0.5.2}%
\contentsline {subsection}{\numberline {A5.3}Acceleration vs. elastic forces: the Mach number}{257}{subsection.Alph0.5.3}%
\contentsline {subsection}{\numberline {A5.4}Other force ratios}{258}{subsection.Alph0.5.4}%
\contentsline {section}{\numberline {A6}Details of the winter 2020-2021 final examination (updated February 2021)}{259}{section.Alph0.6}%
\contentsline {section}{\numberline {A7}Example of previous examinations}{261}{section.Alph0.7}%
\contentsline {section}{\numberline {A8}References}{292}{section.Alph0.8}%
